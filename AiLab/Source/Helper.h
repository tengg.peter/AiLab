#pragma once
//own
#include "AiLab/Source/NeuralNetwork/Training/TrainingSample.h"
//std
#include <vector>

namespace ai_lab::impl
{
	void MakeTrainingAndTestData(
		const std::vector<SpTrainingSample>& availableData,
		std::vector<const TrainingSample*>& trainingData,
		std::vector<const TrainingSample*>& testData,
		const double testSamplingFrequency);
}