#pragma once
#include "ObjectiveEnum.h"
//lib
#include "Utils/Utils.h"
//std
#include <vector>

namespace ai_lab
{
	template<template<typename> typename TSolutionCandidate, typename TParam>
	class AObjectiveFunction
	{
	public:
		AObjectiveFunction(const std::vector<Objective>& objectives, const std::vector<utils::String>& objectiveNames)
			: m_objectives(objectives)
			, m_objectiveNames(objectiveNames)
		{}
		virtual ~AObjectiveFunction() = default;

	public:
		virtual void Calculate(TSolutionCandidate<TParam>& solutionCand) = 0;
		virtual const std::vector<Objective>& Objectives() const { return m_objectives; };
		virtual const std::vector<utils::String>& ObjectiveNames() const { return m_objectiveNames; };
		virtual bool ValidateParams(const std::vector<TParam>&) const { return true; };

	protected:
		const std::vector<Objective> m_objectives;
		const std::vector<utils::String> m_objectiveNames;
	};
}