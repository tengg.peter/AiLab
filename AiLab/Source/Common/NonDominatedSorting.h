#pragma once

#include "AObjectiveFunction.h"

namespace ai_lab
{
	namespace
	{
		template<typename TSolutionCandidate>
		bool Dominates(const TSolutionCandidate& a, const TSolutionCandidate& b, const std::vector<Objective>& objectives)
		{
			for (size_t i = 0; i < a.Scores().size(); i++)
			{
				if (Objective::Maximise == objectives[i] &&
					a.Scores()[i] < b.Scores()[i])
				{
					return false;	//worse fitness found, doesn't dominate
				}
				else if (Objective::Minimise == objectives[i] &&
					a.Scores()[i] > b.Scores()[i])
				{
					return false;
				}
			}

			//at this point every fitness is at least as good as for b. Now we need to find one that is better
			for (size_t i = 0; i < a.Scores().size(); i++)
			{
				if (Objective::Maximise == objectives[i] &&
					a.Scores()[i] > b.Scores()[i])
				{
					return true;
				}
				else if (Objective::Minimise == objectives[i] &&
					a.Scores()[i] < b.Scores()[i])
				{
					return true;
				}
			}
			return false;
		}

		template<typename TSolutionCandidate>
		struct Ranking
		{
			double crowdingDistance = 0.0;
			size_t dominatedBy = 0;
			std::vector<const TSolutionCandidate*> dominates;
			size_t rank = 0;
		};

	}	//namespace

	template<typename TSolutionCandidate>
	std::vector<std::vector<const TSolutionCandidate*>> NonDominatedSorting(
		const std::vector<TSolutionCandidate>& population, 
		const std::vector<Objective>& objectives)
	{
		std::vector<std::vector<const TSolutionCandidate*>> fronts;

		std::map<const TSolutionCandidate*, Ranking<TSolutionCandidate>> rankings;

		for (size_t i = 0; i < population.size(); i++)
		{
			const auto& ind1 = population[i];
			for (size_t j = i + 1; j < population.size(); j++)
			{
				const auto& ind2 = population[j];

				if (Dominates(ind1, ind2, objectives))
				{
					rankings[&ind1].dominates.emplace_back(&ind2);
					++rankings[&ind2].dominatedBy;
				}
				else if (Dominates(ind2, ind1, objectives))
				{
					rankings[&ind2].dominates.emplace_back(&ind1);
					++rankings[&ind1].dominatedBy;
				}
			}
			if (0 == rankings[&ind1].dominatedBy)
			{
				if (fronts.empty())
				{
					fronts.emplace_back();
				}
				fronts.back().emplace_back(&ind1);
				rankings[&ind1].rank = 0;
			}
		}

		size_t i = 0;
		while (true)
		{
			fronts.emplace_back();
			for (const TSolutionCandidate* pInd1 : fronts[i])
			{
				for (const TSolutionCandidate* pInd2 : rankings[pInd1].dominates)
				{
					--(rankings[pInd2].dominatedBy);
					if (0 == rankings[pInd2].dominatedBy)
					{
						fronts[i + 1].emplace_back(pInd2);
						rankings[pInd2].rank = i + 1;
					}
				}
			}

			if (!fronts[i + 1].empty())
			{
				++i;
			}
			else
			{
				fronts.pop_back();
				break;
			}
		}

		return fronts;
	}
}