#pragma once

namespace ai_lab
{
	enum class Objective { Maximise, Minimise };
}