#pragma once
#include "../Common/ObjectiveEnum.h"
#include "Nsga2Individual.h"
//lib
#include "Utils/Utils.h"
//std
#include <vector>

namespace ai_lab
{
	template<typename TGene>
	class ANsga2Fitness
	{
	public:
		explicit ANsga2Fitness(const std::vector<Objective>& objectives, const std::vector<utils::String>& objectiveNames)
			: m_objectives(objectives)
			, m_objectiveNames(objectiveNames)
		{}
		virtual ~ANsga2Fitness() = default;
		
	public:
		virtual void Calculate(Nsga2Individual<TGene>& individual) = 0;
		virtual const std::vector<Objective>& Objectives() const { return m_objectives; }
		virtual const std::vector<utils::String>& ObjectiveNames() const { return m_objectiveNames; }
		virtual bool ValidateGenes(const std::vector<TGene>&) const { return true; }

	private:
		const std::vector<Objective> m_objectives;
		const std::vector<utils::String> m_objectiveNames;
	};
}