#pragma once

#include "../AiLab/AiLab/Source/GA/CrossOver/PatternCrossOver.h"
#include "ANsga2Fitness.h"
#include "Nsga2Individual.h"

#include <algorithm>
#include <atomic>
#include <execution>
#include <future>
#include <set>

class Nsga2TestFixture_Ctor_Test;
class Nsga2Test_Ctor_Test;
class Nsga2TestFixture_SortByObjective_Test;
class Nsga2TestFixture_CrowdingDistance_Test;
class Nsga2TestFixture_CreateOffspringPop_Test;
class Nsga2TestFixture_NonDominatedSorting_Test;
class Nsga2TestFixture_Dominates_Test;
class Nsga2TestFixture_SelectParent_Test;
class Nsga2TestFixture_Crossover_Test;
class Nsga2TestFixture_Mutation_Test;
class Nsga2TestFixture_CreateNewPopulation_Test;
class Nsga2Test_WholeProcess_Test;
class Nsga2Test_ParallelFitness_Test;
class Nsga2TestFixture_EvolveNextGeneration_Test;
class Nsga2TestFixture_GetFirstFront_Test;
class Nsga2TestFixture_AverageFitnesses_Test;
class Nsga2TestFixture_BestFitnesses_Test;

namespace ai_lab
{
	template<typename TGene>
	class Nsga2
	{
	public:
		friend class ::Nsga2TestFixture_Ctor_Test;
		friend class ::Nsga2Test_Ctor_Test;
		friend class ::Nsga2TestFixture_SortByObjective_Test;
		friend class ::Nsga2TestFixture_CrowdingDistance_Test;
		friend class ::Nsga2TestFixture_CreateOffspringPop_Test;
		friend class ::Nsga2TestFixture_NonDominatedSorting_Test;
		friend class ::Nsga2TestFixture_Dominates_Test;
		friend class ::Nsga2TestFixture_SelectParent_Test;
		friend class ::Nsga2TestFixture_Crossover_Test;
		friend class ::Nsga2TestFixture_Mutation_Test;
		friend class ::Nsga2TestFixture_CreateNewPopulation_Test;
		friend class ::Nsga2Test_WholeProcess_Test;
		friend class ::Nsga2Test_ParallelFitness_Test;
		friend class ::Nsga2TestFixture_EvolveNextGeneration_Test;
		friend class ::Nsga2TestFixture_GetFirstFront_Test;
		friend class ::Nsga2TestFixture_AverageFitnesses_Test;
		friend class ::Nsga2TestFixture_BestFitnesses_Test;

		explicit Nsga2(
			size_t popSize, 
			size_t chromLength, 
			unsigned int numGenerations, 
			ANsga2Fitness<TGene>& fitness, 
			bool printGenerations, 
			const std::atomic_bool* const pAbort = nullptr);
		
		explicit Nsga2(
			size_t popSize, 
			const std::vector<std::pair<TGene, TGene>>& geneValueLimits,
			unsigned int numGenerations, 
			ANsga2Fitness<TGene>& fitness,
			bool printGenerations,
			const std::atomic_bool* const pAbort = nullptr);

		virtual ~Nsga2() = default;

		// returns Rank 0 after the specified number generations
		std::vector<Nsga2Individual<TGene>> Run();
		void EvolveNextGeneration();
		std::vector<std::vector<Nsga2Individual<TGene>*>> NonDominatedSorting(std::vector<Nsga2Individual<TGene>>& population);
		std::vector<Nsga2Individual<TGene>> GetFirstFront() const;
		const std::vector<Nsga2Individual<TGene>>& Population() const { return m_population; };
		std::vector<double> AverageFitnesses(size_t numFronts) const;
		std::vector<double> BestFitnesses() const;
		void CalculateFitness(std::vector<Nsga2Individual<TGene>>& population);

	private:
		static void SortFrontByObjective(std::vector<Nsga2Individual<TGene>*>& front, size_t i);

	private:
		void CrowdingDistance(std::vector<std::vector<Nsga2Individual<TGene>*>>& fronts);
		std::vector<Nsga2Individual<TGene>> CreateOffspringPop();
		bool Dominates(const Nsga2Individual<TGene>& a, const Nsga2Individual<TGene>& b);
		std::vector<std::vector<Nsga2Individual<TGene>*>> NonDominatedSorting();
		const Nsga2Individual<TGene>& SelectParent(size_t i, size_t j) const;
		void Mutate(Nsga2Individual<TGene>& ind);
		std::vector<Nsga2Individual<TGene>> CreateNewPopulation(std::vector<std::vector<Nsga2Individual<TGene>*>>& fronts);
		void PrintPopulation(size_t generation) const;
		bool IsAborting() const;

	private:
		const size_t m_popSize;
		const unsigned int m_numGenerations;
		const bool m_printGenerations;
		const bool m_printDetails;

		unsigned int m_currentGeneration;
		std::vector<Nsga2Individual<TGene>> m_population;
		std::set<std::vector<TGene>> m_chromSet;	//ensures no duplicate individuals in the population
		ANsga2Fitness<TGene>& m_fitness;
		std::map<std::vector<TGene>, std::vector<double>> m_fitnessCache;
		std::unique_ptr<ACrossOverStrategy<TGene>> m_crossover;
		utils::date_and_time::StopWatch m_totalTimer;
		utils::date_and_time::StopWatch m_lastGenTimer;
		const std::atomic_bool* const m_pAbort;
	};

	// ====================================================================================================================================================================================

	template<typename TGene>
	Nsga2<TGene>::Nsga2(
		size_t popSize,
		const std::vector<std::pair<TGene, TGene>>& geneValueLimits,
		unsigned int numGenerations,
		ANsga2Fitness<TGene>& fitness,
		bool printGenerations,
		const std::atomic_bool* const pAbort)
		: m_popSize(popSize)
		, m_numGenerations(numGenerations)
		, m_currentGeneration(0)
		, m_population()
		, m_chromSet()
		, m_fitness(fitness)
		, m_crossover(std::make_unique<PatternCrossOver<TGene>>(1.0))
		, m_printGenerations(printGenerations)
		, m_printDetails(false)
		, m_pAbort(pAbort)
	{
		while (m_population.size() < popSize)
		{
			Nsga2Individual<TGene> ind(geneValueLimits, &m_fitness);
			if (0 == m_chromSet.count(ind.chromosome))
			{
				m_chromSet.insert(ind.chromosome);
				m_population.emplace_back(std::move(ind));
			}
		}
	}

	template<typename TGene>
	Nsga2<TGene>::Nsga2(
		size_t popSize, 
		size_t chromLength, 
		unsigned int numGenerations, 
		ANsga2Fitness<TGene>& fitness, 
		bool printGenerations,
		const std::atomic_bool* const pAbort)
		: Nsga2(popSize, std::vector<std::pair<TGene, TGene>>(chromLength, { 0.0, 1.0 }), numGenerations, fitness, printGenerations)
	{}

	template<typename TGene>
	std::vector<Nsga2Individual<TGene>> Nsga2<TGene>::Run()
	{
		CalculateFitness(m_population);
		{
			if (IsAborting())
			{
				return {};
			}
			auto fronts = NonDominatedSorting();
			CrowdingDistance(fronts);
		}
		for (m_currentGeneration = 0; m_currentGeneration < m_numGenerations;)
		{
			if (IsAborting())
			{
				return {};
			}
			EvolveNextGeneration();
		}

		return GetFirstFront();
	}

	template<typename TGene>
	void Nsga2<TGene>::EvolveNextGeneration()
	{
		if (0 == m_currentGeneration)
		{
			if (m_printGenerations)
			{
				m_totalTimer.Start();
			}
			CalculateFitness(m_population);
			if (IsAborting())
			{
				return;
			}
			{
				auto fronts = NonDominatedSorting();
				CrowdingDistance(fronts);
			}
		}
		if (m_printGenerations)
		{
			m_lastGenTimer.Start();
		}

		auto offspringPop = CreateOffspringPop();
		CalculateFitness(offspringPop);
		if (IsAborting())
		{
			return;
		}
		m_population.insert(m_population.end(), std::make_move_iterator(offspringPop.begin()), std::make_move_iterator(offspringPop.end()));
		m_chromSet.clear();
		for (const auto& ind : m_population)
		{
			m_chromSet.insert(ind.chromosome);
		}

		auto fronts = NonDominatedSorting();
		CrowdingDistance(fronts);
		auto newPop = CreateNewPopulation(fronts);
		m_population = newPop;
		PrintPopulation(m_currentGeneration);
		++m_currentGeneration;
	}

	template<typename TGene>
	void Nsga2<TGene>::SortFrontByObjective(std::vector<Nsga2Individual<TGene>*>& front, size_t i)
	{
		std::sort(front.begin(), front.end(), [i](const auto a, const auto b)
			{
				return a->fitnessValues[i] < b->fitnessValues[i];
			});
	}

	template<typename TGene>
	void Nsga2<TGene>::CalculateFitness(std::vector<Nsga2Individual<TGene>>& population)
	{
		std::vector<Nsga2Individual<TGene>*> indPtrs;
		for (auto& ind : population)
		{
			if (!m_fitnessCache.count(ind.chromosome))
			{
				indPtrs.emplace_back(&ind);
			}
			else
			{
				ind.fitnessValues = m_fitnessCache.at(ind.chromosome);
			}
		}

		auto func = [this](Nsga2Individual<TGene>* pInd)
		{
			m_fitness.Calculate(*pInd);
		};

		TCOUT << "Individuals to calculate: " << indPtrs.size() << std::endl;
		utils::ParallelForEach(indPtrs, func, 0, m_pAbort);

		for (auto pInd : indPtrs)
		{
			m_fitnessCache[pInd->chromosome] = pInd->fitnessValues;
		}
	}

	template<typename TGene>
	void Nsga2<TGene>::CrowdingDistance(std::vector<std::vector<Nsga2Individual<TGene>*>>& fronts)
	{
		for (auto& front : fronts)
		{
			for (size_t obj = 0; obj < front.front()->fitnessValues.size(); obj++)
			{
				SortFrontByObjective(front, obj);

				const double minVal = front.front()->fitnessValues[obj];
				const double maxVal = front.back()->fitnessValues[obj];

				if (minVal == maxVal)
				{
					continue;
				}

				front.front()->crowdingDistance = std::numeric_limits<double>::infinity();
				front.back()->crowdingDistance = std::numeric_limits<double>::infinity();

				for (size_t i = 1; i < front.size() - 1; i++)
				{
					const double distance = (front[i + 1]->fitnessValues[obj] - front[i - 1]->fitnessValues[obj]) / (maxVal - minVal);
					front[i]->crowdingDistance += distance;
				}
			}
		}
	}

	template<typename TGene>
	std::vector<Nsga2Individual<TGene>> Nsga2<TGene>::CreateOffspringPop()
	{
		auto& random = utils::RandomService::GetInstance();
		std::vector<Nsga2Individual<TGene>> offspringPop;

		while (offspringPop.size() < m_population.size())
		{
			//selects two parents
			auto indexes = random.GetSample(0, m_population.size() - 1, 2);
			Nsga2Individual<TGene> offspring1 = SelectParent(indexes[0], indexes[1]);

			indexes = random.GetSample(0, m_population.size() - 1, 2);
			Nsga2Individual<TGene> offspring2 = SelectParent(indexes[0], indexes[1]);

			//crossover
			std::vector<Individual<TGene>*> offspring{ &offspring1, &offspring2 };
			m_crossover->CrossOver(offspring);

			//mutation
			Mutate(offspring1);
			Mutate(offspring2);

			if (m_fitness.ValidateGenes(offspring1.chromosome) && m_chromSet.insert(offspring1.chromosome).second)
			{
				offspringPop.emplace_back(std::move(offspring1));
			}
			if (m_fitness.ValidateGenes(offspring2.chromosome) && offspringPop.size() < m_population.size() && m_chromSet.insert(offspring2.chromosome).second)
			{
				offspringPop.emplace_back(std::move(offspring2));
			}
		}

		return offspringPop;
	}

	template<typename TGene>
	bool Nsga2<TGene>::Dominates(const Nsga2Individual<TGene>& a, const Nsga2Individual<TGene>& b)
	{
		const auto& objectives = m_fitness.Objectives();
		for (size_t i = 0; i < a.fitnessValues.size(); i++)
		{
			if (Objective::Maximise == objectives[i] &&
				a.fitnessValues[i] < b.fitnessValues[i])
			{
				return false;	//worse fitness found, doesn't dominate
			}
			else if (Objective::Minimise == objectives[i] &&
				a.fitnessValues[i] > b.fitnessValues[i])
			{
				return false;
			}
		}

		//at this point every fitness is at least as good as for b. Now we need to find one that is better
		for (size_t i = 0; i < a.fitnessValues.size(); i++)
		{
			if (Objective::Maximise == objectives[i] &&
				a.fitnessValues[i] > b.fitnessValues[i])
			{
				return true;
			}
			else if (Objective::Minimise == objectives[i] &&
				a.fitnessValues[i] < b.fitnessValues[i])
			{
				return true;
			}
		}
		return false;
	}

	template<typename TGene>
	std::vector<std::vector<Nsga2Individual<TGene>*>> Nsga2<TGene>::NonDominatedSorting()
	{
		return NonDominatedSorting(m_population);
	}

	template<typename TGene>
	std::vector<std::vector<Nsga2Individual<TGene>*>> Nsga2<TGene>::NonDominatedSorting(std::vector<Nsga2Individual<TGene>>& population)
	{
		if (IsAborting())
		{
			return {};
		}

		std::vector<std::vector<Nsga2Individual<TGene>*>> fronts;

		for (auto& ind : population)
		{
			ind.ResetRanking();
		}

		for (size_t i = 0; i < population.size(); i++)
		{
			auto& ind1 = population[i];
			for (size_t j = i + 1; j < population.size(); j++)
			{
				auto& ind2 = population[j];

				if (Dominates(ind1, ind2))
				{
					ind1.dominates.emplace_back(&ind2);
					++ind2.dominatedBy;
				}
				else if (Dominates(ind2, ind1))
				{
					ind2.dominates.emplace_back(&ind1);
					++ind1.dominatedBy;
				}
			}
			if (0 == ind1.dominatedBy)
			{
				if (fronts.empty())
				{
					fronts.emplace_back();
				}
				fronts.back().emplace_back(&ind1);
				ind1.rank = 0;
			}
		}

		size_t i = 0;
		while (true)
		{
			fronts.emplace_back();
			for (const Nsga2Individual<TGene>* pInd1 : fronts[i])
			{
				for (Nsga2Individual<TGene>* pInd2 : pInd1->dominates)
				{
					--(pInd2->dominatedBy);
					if (0 == pInd2->dominatedBy)
					{
						fronts[i + 1].emplace_back(pInd2);
						pInd2->rank = i + 1;
					}
				}
			}

			if (!fronts[i + 1].empty())
			{
				++i;
			}
			else
			{
				fronts.pop_back();
				break;
			}
		}

		return fronts;
	}

	template<typename TGene>
	const Nsga2Individual<TGene>& Nsga2<TGene>::SelectParent(size_t i, size_t j) const
	{
		const auto& ind1 = m_population[i];
		const auto& ind2 = m_population[j];
		if (ind1.rank == ind2.rank)
		{
			return ind1.crowdingDistance >= ind2.crowdingDistance ? ind1 : ind2;
		}
		return ind1.rank < ind2.rank ? ind1 : ind2;
	}

	template<typename TGene>
	void Nsga2<TGene>::Mutate(Nsga2Individual<TGene>& ind)
	{
		auto& random = utils::RandomService::GetInstance();
		const size_t numGenesToChange = random.GetUniformInt(0, ind.chromosome.size());
		const auto genesToChange = random.GetSample(0, ind.chromosome.size() - 1, numGenesToChange);

		for (size_t g : genesToChange)
		{
			//const double mutationFactor = random.GetGaussianDouble(0, 0.2);
			//const double valueRange = ind.GeneValueRanges()[g].second - ind.GeneValueRanges()[g].first;
			//const double geneVal = ind.chromosome[g] + valueRange * mutationFactor;
			//TCOUT << "original ind.chromosome[g]: " << ind.chromosome[g] << ", mutationFactor: " << mutationFactor << ", valueRange: " << valueRange << ", geneVal: " << geneVal << std::endl;
			//ind.SetGeneWithinRange(g, geneVal);

			TGene geneVal = random.GetUniform<TGene>(ind.GeneValueRanges()[g].first, ind.GeneValueRanges()[g].second);
			//TCOUT << "original ind.chromosome[g]: " << ind.chromosome[g] << ", geneVal: " << geneVal << std::endl;
			ind.SetGeneWithinRange(g, geneVal);
		}
	}

	template<typename TGene>
	std::vector<Nsga2Individual<TGene>> Nsga2<TGene>::CreateNewPopulation(std::vector<std::vector<Nsga2Individual<TGene>*>>& fronts)
	{
		std::vector<Nsga2Individual<TGene>> newPop;

		for (auto& front : fronts)
		{
			if (newPop.size() + front.size() <= m_popSize)
			{
				for (const auto pInd : front)
				{
					newPop.emplace_back(std::move(*pInd));
				}
			}
			else
			{
				std::sort(front.begin(), front.end(),
					[](const auto pInd1, const auto pInd2)
					{
						return pInd1->crowdingDistance > pInd2->crowdingDistance;
					});
				const size_t individualsMissing = m_popSize - newPop.size();
				for (size_t i = 0; i < individualsMissing; i++)
				{
					newPop.emplace_back(std::move(*front[i]));
				}
			}
		}

		return newPop;
	}

	template<typename TGene>
	void Nsga2<TGene>::PrintPopulation(size_t generation) const
	{
		if (!m_printGenerations)
		{
			return;
		}

		using std::endl;
		TCOUT << "\n============================================================================================================================================" << endl;
		TCOUT << "Generation " << generation << ": " << endl;
		TCOUT << "Since last generation: " << m_lastGenTimer.ElapsedTime() << endl;
		TCOUT << "Total run time: " << m_totalTimer.ElapsedTime() << endl;
		size_t rank = m_population.front().rank;
		TCOUT << "Rank " << rank << ": " << endl;
		const auto& objectiveNames = m_fitness.ObjectiveNames();
		for (auto& ind : m_population)
		{
			if (rank != ind.rank)
			{
				rank = ind.rank;
				TCOUT << "\nRank " << rank << ": " << endl;
			}
			TCOUT << "(";
			for (size_t i = 0; i < ind.chromosome.size(); i++)
			{
				if (0 < i)
				{
					TCOUT << ", ";
				}
				TCOUT << ind.chromosome[i];
			}
			TCOUT << "): (";

			for (size_t o = 0; o < objectiveNames.size(); o++)
			{
				if (0 < o)
				{
					TCOUT << ", ";
				}
				TCOUT << objectiveNames[o] << ": " << ind.fitnessValues[o];
			}
			TCOUT << ") " << endl;
		}
		TCOUT << endl;
	}

	template<typename TGene>
	inline bool Nsga2<TGene>::IsAborting() const
	{
		return m_pAbort && m_pAbort->load();
	}

	template<typename TGene>
	std::vector<Nsga2Individual<TGene>> Nsga2<TGene>::GetFirstFront() const
	{
		std::vector<Nsga2Individual<TGene>> firstFront;
		std::copy_if(m_population.cbegin(), m_population.cend(), std::back_inserter(firstFront), [](const auto& ind) {return 0 == ind.rank; });
		return firstFront;
	}

	template<typename TGene>
	std::vector<double> Nsga2<TGene>::AverageFitnesses(size_t numFronts) const
	{
		std::vector<double> ret(m_fitness.Objectives().size());

		for (size_t obj = 0; obj < m_fitness.Objectives().size(); obj++)
		{
			size_t numIndividuals = 0;
			double sumFitness = 0.0;

			for (const auto& ind : m_population)
			{
				if (ind.rank >= numFronts)
				{
					break;
				}
				++numIndividuals;
				sumFitness += ind.fitnessValues[obj];
			}
			ret[obj] = sumFitness / numIndividuals;
		}

		return ret;
	}

	template<typename TGene>
	std::vector<double> Nsga2<TGene>::BestFitnesses() const
	{
		std::vector<double> ret;

		for (size_t i = 0; i < m_fitness.Objectives().size(); i++)
		{
			const auto obj = m_fitness.Objectives()[i];
			if (Objective::Maximise == obj)
			{
				ret.emplace_back(std::max_element(m_population.cbegin(), m_population.cend(), [i](const auto& indA, const auto& indB)
					{
						return indA.fitnessValues[i] < indB.fitnessValues[i];
					})->fitnessValues[i]);
			}
			else	//Minimise
			{
				ret.emplace_back(std::min_element(m_population.cbegin(), m_population.cend(), [i](const auto& indA, const auto& indB)
					{
						return indA.fitnessValues[i] < indB.fitnessValues[i];
					})->fitnessValues[i]);
			}
		}

		return ret;
	}
}