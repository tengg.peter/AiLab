#pragma once

#include "../GA/Individual.h"

namespace ai_lab
{
	template<typename TGene>
	class ANsga2Fitness;

	template<typename TGene>
	class Nsga2Individual : public Individual<TGene>
	{
	public:
		explicit Nsga2Individual(const std::vector<std::pair<TGene, TGene>>& geneValueRanges = {}, const ANsga2Fitness<TGene>* pFitness = nullptr);
		explicit Nsga2Individual(size_t chromLength, const ANsga2Fitness<TGene>* pFitness = nullptr);
		virtual ~Nsga2Individual() = default;

	public:
		void ResetRanking()
		{
			crowdingDistance = 0.0;
			dominatedBy = 0;
			dominates.clear();
			rank = 0;
		}

	public:
		double crowdingDistance = 0.0;
		size_t dominatedBy = 0;
		std::vector<Nsga2Individual<TGene>*> dominates;
		size_t rank = 0;
		const ANsga2Fitness<TGene>* pFitness;

	private:
		void ValidateChromosome();
	};

	// =============================================================================================================================================================================

	template<typename TGene>
	Nsga2Individual<TGene>::Nsga2Individual(const std::vector<std::pair<TGene, TGene>>& geneValueRanges, const ANsga2Fitness<TGene>* pFitness)
		: pFitness(pFitness)
		, Individual<TGene>(geneValueRanges)
	{
		ValidateChromosome();
	}

	template<typename TGene>
	Nsga2Individual<TGene>::Nsga2Individual(size_t chromLength, const ANsga2Fitness<TGene>* pFitness)
		: pFitness(pFitness)
		, Individual<TGene>(chromLength)
	{
		ValidateChromosome();
	}

	template<typename TGene>
	void Nsga2Individual<TGene>::ValidateChromosome()
	{
		uint32_t count = 0;
		while (nullptr != pFitness && !pFitness->ValidateGenes(this->chromosome))
		{
			if (1000 <= ++count)
			{
				throw std::logic_error("Impossible constraint?");
			}

			Individual<TGene>::InitChromosome(this->chromosome, this->m_geneValueRanges);
		}
	}
}