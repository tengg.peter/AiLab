#pragma once
//own
#include "AiLab/Source/GA/CrossOver/ACrossOverStrategy.h"
#include "AiLab/Source/GA/CrossOver/PatternCrossOver.h"
#include "AiLab/Source/GA/GaOptions.h"
#include "AiLab/Source/GA/GaParams.h"
#include "AiLab/Source/GA/IFitnessFunction.h"
#include "AiLab/Source/GA/IGaStopCondition.h"
#include "AiLab/Source/GA/Individual.h"
//lib
#include "Utils/Utils.h"

//std
#include <algorithm>
#include <filesystem>
#include <future>
#include <iostream>
#include <limits>
#include <thread>

namespace ai_lab
{
	template<typename TGene>
	class GA
	{
	public:
		static GA FromJsonFile(const utils::String& filePath);

	public:
		explicit GA(const GaParams& gaParams,
			const GaOptions& options,
			const std::shared_ptr<IFitnessFunction<TGene>>& spFitnessFunction,
			const std::shared_ptr<IGaStopCondition>& spStopCondition);
		virtual ~GA() = default;

	public:
		void Run();
		Individual<TGene>& BestIndividual();
		double AverageFitness();
		void SetFitnessFunction(const std::shared_ptr<IFitnessFunction<TGene>>& spFitnessFunction);
		void SetStopCondition(const std::shared_ptr<IGaStopCondition>& spStopCondition);

	private:
		std::vector<Individual<TGene>> GetPopulation() const;
		std::vector<Individual<TGene>> Select(std::piecewise_constant_distribution<double>& rouletteWheel) const;	//cannot be const ref, GCC doesnt like it
		void Mutate(std::vector<Individual<TGene>>& individuals);
		void CalculateFitnesses();
		void CalculateFitnessStatistics();
		std::piecewise_constant_distribution<double> CreateRouletteWheel(const std::vector<Individual<TGene>>& population) const;
		void WriteToJsonFile();
		void DeletePreviousJsonFile() const;
		double Scale(double d, double fromRangeLower, double fromRangeUpper, double toRangeLower, double toRangeUpper) const;

	private:
		GaParams m_gaParams;
		const GaOptions m_gaOptions;
		std::shared_ptr<IGaStopCondition> m_spStopCondition;
		std::shared_ptr<IFitnessFunction<TGene>> m_spFitnessFunction;
		std::shared_ptr<ACrossOverStrategy<TGene>> m_spCrossOver;

		std::vector<Individual<TGene>> m_population1;
		std::vector<Individual<TGene>> m_population2;
		std::vector<Individual<TGene>>* m_pCurrentPopulation = nullptr;
		std::vector<Individual<TGene>>* m_pNextPopulation = nullptr;
		double m_averageFitness = 0.0;
		double m_bestFitness = 0.0;
		double m_worstFitness = 0.0;
		Individual<TGene>* m_pBestIndividual = nullptr;
		unsigned int m_generation = 0;
		bool m_loadedFromFile = false;
		utils::String m_previousJsonFilePath;

		utils::RandomService& m_randomService;
	};

	//==========================================================================================================================================================================================

	namespace
	{
		const std::string AVG_FITNESS_JSON_KEY = "averageFitness";
		const std::string BEST_FITNESS_JSON_KEY = "bestFitness";
		const std::string CHROM_LENGTH_JSON_KEY = "chromLength";
		const std::string CHROMOSOME_JSON_KEY = "chromosome";
		const std::string CROSSOVER_PROB_JSON_KEY = "crossoverProbability";
		const std::string ELITISM_JSON_KEY = "elitism";
		const std::string FITNESS_JSON_KEY = "fitness";
		const std::string GA_OPTIONS_JSON_KEY = "gaOptions";
		const std::string GA_PARAMS_JSON_KEY = "gaParams";
		const std::string GENE_NAMES_JSON_KEY = "geneNames";
		const std::string GENE_VALUE_RANGES_JSON_KEY = "geneValueRanges";
		const std::string GENERATION_JSON_KEY = "generation";
		const std::string MAX_FITNESS_THREADS_JSON_KEY = "maxFitnessThreads";
		const std::string MAX_JSON_KEY = "max";
		const std::string MIN_JSON_KEY = "min";
		const std::string MUTATION_PROB_JSON_KEY = "mutationProbability";
		const std::string OUTPUT_DIR_JSON_KEY = "outputDir";
		const std::string POP_SIZE_JSON_KEY = "popSize";
		const std::string POPULATION_JSON_KEY = "population";
		const std::string PRINT_DETAILS_JSON_KEY = "printDetails";
		const std::string SELECTION_GROUP_SIZE_JSON_KEY = "selectionGroupSize";
	}

	template<typename TGene>
	GA<TGene> GA<TGene>::FromJsonFile(const utils::String& filePath)
	{
		utils::IfStream inputFile(filePath);
		utils::StringStream ss;
		ss << inputFile.rdbuf();

		using utils::string_conversion::UtilsStringToStdString;
		const std::string jsonString{ UtilsStringToStdString(ss.str()) };

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(jsonString, rootJson);

		//gaParams
		GaParams gaParams;
		gaParams.popSize = rootJson[GA_PARAMS_JSON_KEY][POP_SIZE_JSON_KEY].asInt();
		gaParams.chromLength = rootJson[GA_PARAMS_JSON_KEY][CHROM_LENGTH_JSON_KEY].asInt();
		for (size_t i = 0; i < gaParams.chromLength; i++)
		{
			gaParams.geneValueRanges.emplace_back(std::make_pair(
				rootJson[GA_PARAMS_JSON_KEY][GENE_VALUE_RANGES_JSON_KEY][std::to_string(i)][MIN_JSON_KEY].asDouble(),
				rootJson[GA_PARAMS_JSON_KEY][GENE_VALUE_RANGES_JSON_KEY][std::to_string(i)][MAX_JSON_KEY].asDouble())
			);
		}
		gaParams.crossoverProbability = rootJson[GA_PARAMS_JSON_KEY][CROSSOVER_PROB_JSON_KEY].asDouble();
		gaParams.mutationProbability = rootJson[GA_PARAMS_JSON_KEY][MUTATION_PROB_JSON_KEY].asDouble();
		gaParams.elitism = rootJson[GA_PARAMS_JSON_KEY][ELITISM_JSON_KEY].asBool();
		gaParams.selectionGroupSize = rootJson[GA_PARAMS_JSON_KEY][SELECTION_GROUP_SIZE_JSON_KEY].asInt();

		//gaOptions
		GaOptions gaOptions;
		gaOptions.printDetails = rootJson[GA_OPTIONS_JSON_KEY][PRINT_DETAILS_JSON_KEY].asBool();
		using utils::string_conversion::StdStringToUtilsString;
		gaOptions.outputDir = StdStringToUtilsString(rootJson[GA_OPTIONS_JSON_KEY][OUTPUT_DIR_JSON_KEY].asString());
		for (int i = 0; ; i++)
		{
			if (rootJson[GA_OPTIONS_JSON_KEY][GENE_NAMES_JSON_KEY][std::to_string(i)].isNull())
			{
				break;
			}
			gaOptions.geneNames.push_back(
				StdStringToUtilsString(rootJson[GA_OPTIONS_JSON_KEY][GENE_NAMES_JSON_KEY][std::to_string(i)].asString()));
		}
		gaOptions.maxFitnessThreads = rootJson[GA_OPTIONS_JSON_KEY][MAX_FITNESS_THREADS_JSON_KEY].asInt();

		GA ga(gaParams, gaOptions, nullptr /*fitnessFunction*/, nullptr/*stopCondition*/);

		//population
		ga.m_population1.clear();
		for (int i = 0; i < rootJson[POPULATION_JSON_KEY].size(); i++)
		{
			Individual ind;
			ind.fitnessValues.emplace_back(rootJson[POPULATION_JSON_KEY][i][FITNESS_JSON_KEY].asDouble());
			for (int g = 0; ; g++)
			{
				if (rootJson[POPULATION_JSON_KEY][i][CHROMOSOME_JSON_KEY][std::to_string(g)].isNull())
				{
					break;
				}
				ind.chromosome.push_back(rootJson[POPULATION_JSON_KEY][i][CHROMOSOME_JSON_KEY][std::to_string(g)].asDouble());
			}
			ga.m_population1.push_back(ind);
		}
		ga.m_averageFitness = rootJson[AVG_FITNESS_JSON_KEY].asDouble();
		ga.m_bestFitness = rootJson[BEST_FITNESS_JSON_KEY].asDouble();
		ga.m_generation = rootJson[GENERATION_JSON_KEY].asInt();
		ga.m_loadedFromFile = true;

		return ga;
	}

	template<typename TGene>
	GA<TGene>::GA(
		const GaParams& gaParams,
		const GaOptions& options,
		const std::shared_ptr<IFitnessFunction<TGene>>& spFitnessFunction,
		const std::shared_ptr<IGaStopCondition>& spStopCondition)
		: m_gaParams(gaParams)
		, m_gaOptions(options)
		, m_spFitnessFunction(spFitnessFunction)
		, m_spCrossOver(std::make_shared<PatternCrossOver<TGene>>(gaParams.crossoverProbability))
		, m_spStopCondition(spStopCondition)
		, m_randomService(utils::RandomService::GetInstance())
	{
		if (m_gaParams.geneValueRanges.empty())
		{
			//sets default gene values if not set
			for (size_t i = 0; i < m_gaParams.chromLength; i++)
			{
				m_gaParams.geneValueRanges.emplace_back(std::make_pair(0.0, 1.0));
			}
		}

		if (m_gaParams.geneValueRanges.size() != m_gaParams.chromLength)
		{
			throw std::logic_error("The gene value ranges and the chromosome length must be equal.");
		}

		for (int i = 0; i < m_gaParams.popSize; i++)
		{
			m_population1.push_back(Individual(m_gaParams.geneValueRanges));
		}
	}

	template<typename TGene>
	void GA<TGene>::Run()
	{
		TCOUT << "GA starting\n" << m_gaParams << "\n" << m_gaOptions << "\n" << std::endl;

		std::filesystem::create_directory(m_gaOptions.outputDir);
		const utils::String fileName =
			m_gaOptions.outputDir + STR("GA output ") + utils::DateTime::Now().ToDateTimeString() + STR(".csv");
		{
			utils::CsvFileWriter fileWriter(fileName, true /*append*/);
			fileWriter.Write(utils::DateTime::Now(), "Generation", "Best fitness", "Avg. fitness", "");
			for (const utils::String& geneName : m_gaOptions.geneNames)
			{
				fileWriter.Write(geneName);
			}
			fileWriter.WriteLine();
		}

		m_pCurrentPopulation = &m_population1;
		m_pNextPopulation = &m_population2;

		while (true)
		{
			if (!m_loadedFromFile)
			{
				//fitnesses are loaded from the file, there is no need to calculate them
				CalculateFitnesses();
			}
			CalculateFitnessStatistics();
			if (!m_loadedFromFile)
			{
				TCOUT << "generation" << " " << m_generation <<
					": best fitness: " << m_bestFitness <<
					", average fitness: " << m_averageFitness << std::endl;
				{
					utils::CsvFileWriter fileWriter(fileName, true);
					fileWriter.Write(utils::DateTime::Now(), m_generation, m_bestFitness, m_averageFitness, "");
					for (const double gene : m_pBestIndividual->chromosome)
					{
						fileWriter.Write(gene);
					}
					fileWriter.WriteLine();
					WriteToJsonFile();
				}
			}

			if (m_spStopCondition->operator()(m_generation, m_bestFitness, m_averageFitness))
			{
				TCOUT << "Stop condition met. GA exiting." << std::endl;
				break;
			}

			//fill up next generation
			m_pNextPopulation->clear();
			if (m_gaParams.elitism)
			{
				m_pNextPopulation->push_back(*m_pBestIndividual);
			}

			std::piecewise_constant_distribution<double> rouletteWheel = CreateRouletteWheel(*m_pCurrentPopulation);
			while (m_pNextPopulation->size() < m_gaParams.popSize)
			{
				std::vector<Individual<TGene>> selectedOnes = Select(rouletteWheel);
				m_spCrossOver->CrossOver(selectedOnes);
				Mutate(selectedOnes);

				m_pNextPopulation->insert(m_pNextPopulation->cend(), selectedOnes.cbegin(), selectedOnes.cend());
			}
			m_pNextPopulation->resize(m_gaParams.popSize);	//some individuals may be thrown away from the back

			std::swap(m_pCurrentPopulation, m_pNextPopulation);
			++m_generation;
			m_loadedFromFile = false;
		}
	}

	template<typename TGene>
	void GA<TGene>::CalculateFitnesses()
	{
		//TODO: use std::for_each(std::execution::par_unseq. See Nsha2.CalculateFitness()
		if (1 >= m_gaOptions.maxFitnessThreads)
		{
			for (size_t i = 0; i < m_pCurrentPopulation->size(); i++)
			{
				auto& ind = (*m_pCurrentPopulation)[i];
				if (ind.fitnessValues.empty())
				{
					if (m_gaOptions.printDetails)
					{
						TCOUT << "Testing individual " << i + 1 << "/" << m_pCurrentPopulation->size() <<
							" of generation " << m_generation << "..." << std::endl;
					}
					m_spFitnessFunction->Calculate(ind);
				}
				else if (m_gaOptions.printDetails)
				{
					TCOUT << "Known fitness: individual " << i + 1 << "/" << m_pCurrentPopulation->size() <<
						" of generation " << m_generation << std::endl;
				}
				if (m_gaOptions.printDetails)
				{
					TCOUT << "\tFitness: " << ind.fitnessValues.front() << std::endl;
				}
			}
		}
		else
		{
			size_t i = 0;
			size_t completed = 0;
			std::vector<std::future<Individual<TGene>&>> futures;
			do
			{
				while (futures.size() < m_gaOptions.maxFitnessThreads)
				{
					if (m_pCurrentPopulation->size() <= i)
					{
						break;
					}
					auto& ind = (*m_pCurrentPopulation)[i];
					if (ind.fitnessValues.empty())
					{
						if (m_gaOptions.printDetails)
						{
							TCOUT << "Testing individual " << i + 1 << "/" << m_pCurrentPopulation->size() <<
								" of generation " << m_generation << "..." << std::endl;
						}
						futures.push_back(std::async(std::launch::async,
							[this](Individual<TGene>& ind) -> Individual<TGene>&
							{
								this->m_spFitnessFunction->Calculate(ind);
								return ind;
							},
							std::ref(ind)));
					}
					else
					{
						if (m_gaOptions.printDetails)
						{
							TCOUT << "Known fitness: individual " << i + 1 << "/" << m_pCurrentPopulation->size() <<
								" of generation " << m_generation << std::endl;
							//cheat: we have no idea if this individual has a calculated fitness already:
							TCOUT << "\tFitness: " << ind.fitnessValues.front() << std::endl;
						}
						++completed;
					}
					++i;
				}
				using namespace std::chrono_literals;
				std::this_thread::sleep_for(250ms);
				for (auto it = futures.begin(); futures.end() != it; ++it)
				{
					if (std::future_status::ready == it->wait_for(0ms))
					{
						if (m_gaOptions.printDetails)
						{
							//cheat: we have no idea if this individual has a calculated fitness already:
							// TCOUT << "\tFitness: " << (*m_pCurrentPopulation)[completed].fitness << endl;
							TCOUT << "\tFitness: " << it->get().fitnessValues.front() << std::endl;
						}
						it = futures.erase(it);
						++completed;
					}
					if (futures.end() == it)
					{
						break;
					}
				}
			} while (completed < m_pCurrentPopulation->size());
		}
	}

	template<typename TGene>
	std::vector<Individual<TGene>> GA<TGene>::Select(std::piecewise_constant_distribution<double>& rouletteWheel) const
	{
		std::vector<Individual<TGene>> ret;
		while (m_gaParams.selectionGroupSize > ret.size())
		{
			ret.push_back((*m_pCurrentPopulation)[(int)rouletteWheel(m_randomService.GetRandomEngine())]);
		}
		return ret;
	}

	template<typename TGene>
	void GA<TGene>::Mutate(std::vector<Individual<TGene>>& individuals)
	{
		for (auto& ind : individuals)
		{
			for (int i = 0; i < ind.chromosome.size(); i++)
			{
				if (m_randomService.GetUniformDouble(0, 1) < m_gaParams.mutationProbability)
				{
					ind.chromosome[i] = m_randomService.GetUniformDouble(
						m_gaParams.geneValueRanges[i].first,
						m_gaParams.geneValueRanges[i].second);
					ind.fitnessValues.clear();	//gene modified, so resets fitness
				}
			}
		}

	}

	template<typename TGene>
	std::piecewise_constant_distribution<double> GA<TGene>::CreateRouletteWheel(const std::vector<Individual<TGene>>& population) const
	{
		std::vector<size_t> intervals;
		std::vector<double> weights;
		TCOUT << "Roulette wheel probabilities: " << std::endl;
		for (int i = 0; i < population.size(); i++)
		{
			intervals.push_back(i);
			if (m_spFitnessFunction->InverseFitness())
			{
				const double rouletteSection =
					Scale((m_averageFitness / population[i].fitnessValues.front()),
						m_averageFitness / m_worstFitness,
						m_averageFitness / m_bestFitness,
						1.0, 1.0 + 0.2 * m_gaParams.popSize);
				if (0 > rouletteSection)
				{
					TCOUT << rouletteSection << ", ";
				}
				TCOUT << rouletteSection << ", ";
				weights.push_back(rouletteSection);
			}
			else
			{
				weights.push_back(population[i].fitnessValues.front());
			}
		}
		TCOUT << "\n" << std::endl;
		intervals.push_back(population.size());
		std::piecewise_constant_distribution<double> rouletteWheel(intervals.begin(), intervals.end(), weights.begin());
		return rouletteWheel;
	}

	template<typename TGene>
	Individual<TGene>& GA<TGene>::BestIndividual()
	{
		return *m_pBestIndividual;
	}

	template<typename TGene>
	double GA<TGene>::AverageFitness()
	{
		return m_averageFitness;
	}

	template<typename TGene>
	void GA<TGene>::CalculateFitnessStatistics()
	{
		m_bestFitness = m_spFitnessFunction->InverseFitness() ? std::numeric_limits<double>::max() : 0.0;
		m_averageFitness = 0.0;
		m_worstFitness = m_spFitnessFunction->InverseFitness() ? 0.0 : std::numeric_limits<double>::max();
		m_pBestIndividual = nullptr;
		for (int i = 0; i < m_pCurrentPopulation->size(); i++)
		{
			auto& ind = (*m_pCurrentPopulation)[i];
			if (
				!m_spFitnessFunction->InverseFitness() && ind.fitnessValues.front() > m_bestFitness ||
				m_spFitnessFunction->InverseFitness() && ind.fitnessValues.front() < m_bestFitness)
			{
				m_bestFitness = ind.fitnessValues.front();
				m_pBestIndividual = &ind;
			}
			else if (
				!m_spFitnessFunction->InverseFitness() && (*m_pCurrentPopulation)[i].fitnessValues.front() < m_worstFitness ||
				m_spFitnessFunction->InverseFitness() && (*m_pCurrentPopulation)[i].fitnessValues.front() > m_worstFitness)
			{
				m_worstFitness = ind.fitnessValues.front();
			}
			m_averageFitness += ind.fitnessValues.front();
		}
		m_averageFitness /= m_pCurrentPopulation->size();
	}

	template<typename TGene>
	std::vector<Individual<TGene>> GA<TGene>::GetPopulation() const
	{
		return *m_pCurrentPopulation;
	}

	template<typename TGene>
	void GA<TGene>::SetFitnessFunction(const std::shared_ptr<IFitnessFunction<TGene>>& spFitnessFunction)
	{
		m_spFitnessFunction = spFitnessFunction;
	}

	template<typename TGene>
	void GA<TGene>::SetStopCondition(const std::shared_ptr<IGaStopCondition>& spStopCondition)
	{
		m_spStopCondition = spStopCondition;
	}

	template<typename TGene>
	void GA<TGene>::WriteToJsonFile()
	{
		using utils::string_conversion::UtilsStringToStdString;

		Json::Value rootJson;

		//gaParams
		Json::Value gaParams;
		gaParams[POP_SIZE_JSON_KEY] = m_gaParams.popSize;
		gaParams[CHROM_LENGTH_JSON_KEY] = m_gaParams.chromLength;
		for (size_t i = 0; i < m_gaParams.chromLength; i++)
		{
			Json::Value geneValueRange;
			geneValueRange[MIN_JSON_KEY] = m_gaParams.geneValueRanges[i].first;
			geneValueRange[MAX_JSON_KEY] = m_gaParams.geneValueRanges[i].second;
			gaParams[GENE_VALUE_RANGES_JSON_KEY][std::to_string(i)] = geneValueRange;
		}
		gaParams[CROSSOVER_PROB_JSON_KEY] = m_gaParams.crossoverProbability;
		gaParams[MUTATION_PROB_JSON_KEY] = m_gaParams.mutationProbability;
		gaParams[ELITISM_JSON_KEY] = m_gaParams.elitism;
		gaParams[SELECTION_GROUP_SIZE_JSON_KEY] = m_gaParams.selectionGroupSize;
		rootJson[GA_PARAMS_JSON_KEY] = gaParams;

		//gaOptions
		Json::Value gaOptions;
		gaOptions[PRINT_DETAILS_JSON_KEY] = m_gaOptions.printDetails;
		gaOptions[OUTPUT_DIR_JSON_KEY] = UtilsStringToStdString(m_gaOptions.outputDir);
		for (size_t i = 0; i < m_gaOptions.geneNames.size(); i++)
		{
			gaOptions[GENE_NAMES_JSON_KEY][std::to_string(i)] =
				UtilsStringToStdString(m_gaOptions.geneNames[i]);
		}
		gaOptions[MAX_FITNESS_THREADS_JSON_KEY] = m_gaOptions.maxFitnessThreads;
		rootJson[GA_OPTIONS_JSON_KEY] = gaOptions;

		//population
		Json::Value population;
		for (const Individual<TGene>& ind : *m_pCurrentPopulation)
		{
			Json::Value individualJson;
			individualJson[FITNESS_JSON_KEY] = ind.fitnessValues.front();
			for (size_t i = 0; i < ind.chromosome.size(); i++)
			{
				individualJson[CHROMOSOME_JSON_KEY][std::to_string(i)] = ind.chromosome[i];
			}
			population.append(individualJson);
		}
		rootJson[POPULATION_JSON_KEY] = population;

		rootJson[AVG_FITNESS_JSON_KEY] = m_averageFitness;
		rootJson[BEST_FITNESS_JSON_KEY] = m_bestFitness;
		rootJson[GENERATION_JSON_KEY] = m_generation;

		Json::StreamWriterBuilder builder;
		builder["indentation"] = " "; // If you want whitespace-less output
		const utils::String jsonString =
			utils::string_conversion::StdStringToUtilsString(Json::writeString(builder, rootJson));

		std::filesystem::create_directory(m_gaOptions.outputDir);
		const utils::String filePath = m_gaOptions.outputDir + STR("generation ") + utils::ToString(m_generation)
			+ STR(" - ") + utils::DateTime::Now().ToDateTimeString() + STR(".json");

		{
			utils::OfStream file;
			file.open(filePath);
			file << jsonString;
			file.close();
		}
		DeletePreviousJsonFile();
		m_previousJsonFilePath = filePath;
	}

	template<typename TGene>
	void GA<TGene>::DeletePreviousJsonFile() const
	{
		if (m_previousJsonFilePath.empty())
		{
			return;
		}
		std::filesystem::remove(m_previousJsonFilePath);
	}

	template<typename TGene>
	double GA<TGene>::Scale(double d, double fromRangeLower, double fromRangeUpper, double toRangeLower, double toRangeUpper) const
	{
		const double fromRange = fromRangeUpper - fromRangeLower;
		const double toRange = toRangeUpper - toRangeLower;

		return (d - fromRangeLower) / fromRange * toRange + toRangeLower;
	}
}