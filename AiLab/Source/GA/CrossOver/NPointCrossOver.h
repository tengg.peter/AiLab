#pragma once
#include "AiLab/Source/GA/CrossOver/ACrossOverStrategy.h"

namespace ai_lab
{
    template<typename TGene>
    class NPointCrossOver : public ACrossOverStrategy<TGene>
    {
    public:
        explicit NPointCrossOver(double crossOverProbabilty, int numCrossingPoints = -1);
        virtual ~NPointCrossOver() = default;

    public:
        virtual void CrossOver(std::vector<Individual<TGene>>& individuals) const override;
        virtual void CrossOver(std::vector<Individual<TGene>*>& individuals) const override;
    
    protected:
        const int m_numCrossingPoints = -1;
    };

    //=======================================================================================================================================================================================

    template<typename TGene>
    NPointCrossOver<TGene>::NPointCrossOver(double crossOverProbabilty, int numCrossingPoints)
        : ACrossOverStrategy<TGene>(crossOverProbabilty)
        , m_numCrossingPoints(numCrossingPoints)
    {}

    template<typename TGene>
    void NPointCrossOver<TGene>::CrossOver(std::vector<Individual<TGene>>& individuals) const
    {
        const size_t numInd = individuals.size();

        if (0 == m_numCrossingPoints ||
            2 > numInd ||
            this->m_randomService.GetUniformDouble(0, 1) >= this->crossOverProbability ||
            this->AllTheSame(individuals))
        {
            return;
        }

        TCOUT << "Before n point cross over:\n";
        for (const auto& ind : individuals)
        {
            using utils::operator<<;
            TCOUT << ind.chromosome << "\n";
        }
        TCOUT << std::endl;

        size_t numCrossingPoints = -1 == m_numCrossingPoints ? numInd - 1 : m_numCrossingPoints;
        const size_t chromLength = individuals.front().chromosome.size();
        numCrossingPoints = std::min(numCrossingPoints, chromLength - 1);

        //TODO: use std::sample to pick the crossing indexes
        std::vector<bool> crossingPoints(numCrossingPoints, true);
        crossingPoints.resize(chromLength - 1, false);
        this->m_randomService.Shuffle(crossingPoints);

        for (size_t c = 0; c < crossingPoints.size(); c++)
        {
            if (!crossingPoints[c])
            {
                continue;
            }
            const size_t crossingPoint = c + 1;
            for (size_t i = 0; i < numInd - 1; i++)
            {
                auto& ind1 = individuals[i];
                auto& ind2 = individuals[(i + numInd - 1) % numInd];
                std::swap_ranges(ind1.chromosome.begin() + crossingPoint, ind1.chromosome.end(), ind2.chromosome.begin() + crossingPoint);
                ind1.fitnessValues.clear();	//genes modified, so resets fitness
                ind2.fitnessValues.clear();
            }
        }

        TCOUT << "After n point cross over:\n";
        for (const auto& ind : individuals)
        {
            using utils::operator<<;
            TCOUT << ind.chromosome << "\n";
        }
        TCOUT << std::endl;

    }
    
    template<typename TGene>
    void NPointCrossOver<TGene>::CrossOver(std::vector<Individual<TGene>*>& individuals) const
    {}
}