#pragma once
#include "AiLab/Source/GA/CrossOver/ACrossOverStrategy.h"


#include <numeric>

namespace ai_lab
{
    template<typename TGene>
    class PatternCrossOver : public ACrossOverStrategy<TGene>
    {
    public:
        explicit PatternCrossOver(double crossOverProbabilty);
        virtual ~PatternCrossOver() = default;

    public:
        virtual void CrossOver(std::vector<Individual<TGene>>& individuals) const override;
        virtual void CrossOver(std::vector<Individual<TGene>*>& individuals) const override;
    };

    //==========================================================================================================================================================================================

    template<typename TGene>
    PatternCrossOver<TGene>::PatternCrossOver(double crossOverProbabilty)
        : ACrossOverStrategy<TGene>(crossOverProbabilty)
    {}

    template<typename TGene>
    void PatternCrossOver<TGene>::CrossOver(std::vector<Individual<TGene>>& individuals) const
    {
        const size_t numInd = individuals.size();

        if (2 > numInd ||
            this->m_randomService.GetUniformDouble(0, 1) >= this->crossOverProbability ||
            this->AllTheSame(individuals))
        {
            return;
        }

        // TCOUT << "Before pattern cross over:\n";
        // for(const auto& ind : individuals)
        // {
        //     TCOUT << ind.chromosome << "\n";
        // }
        // TCOUT << endl;

        const size_t chromLength = individuals.front().chromosome.size();
        std::vector<size_t> indexes(numInd);
        std::iota(indexes.begin(), indexes.end(), 0);
        std::vector<std::vector<size_t>> pattern(chromLength, indexes);
        std::vector<bool> affected(numInd, false);

        //shuffles until all the individuals are affected by the crossover
        do
        {
            for (auto& v : pattern)
            {
                this->m_randomService.Shuffle(v);
            }
            //checks if the individuals are affected
            for (size_t i = 0; i < pattern.size(); i++)
            {
                for (size_t j = 0; j < pattern[i].size(); j++)
                {
                    affected[j] = affected[j] || (pattern[i][j] != pattern[(i - 1) % chromLength][j]);  //sets affected j to true if the two indexes are not the same
                }
            }
        } while (!std::all_of(affected.cbegin(), affected.cend(), [](bool b) {return b; }));

        //TCOUT << "Crossover pattern: \n";
        //for (size_t i = 0; i < chromLength; i++)
        //{
        //    TCOUT << pattern[i] << "\n";
        //}
        //TCOUT << endl;

       //do the actual crossover based on the pattern
        const std::vector<Individual<TGene>> parents(individuals);

        for (size_t i = 0; i < numInd; i++)
        {
            for (size_t j = 0; j < chromLength; j++)
            {
                individuals[i].chromosome[j] = parents[pattern[j][i]].chromosome[j];
                //TCOUT << "i, j:" << i << ", " << j << endl;
                //TCOUT << "individual" << i << ": " << individuals[i].chromosome << endl;
                //TCOUT << "pattern[j][i]:" << pattern[j][i] << endl;
                //TCOUT << "parent:" << parents[pattern[j][i]].chromosome[j] << endl;
            }
            individuals[i].fitnessValues.clear();
        }

        //TCOUT << "After pattern cross over:\n";
        //for(const auto& ind : individuals)
        //{
        //    TCOUT << ind.chromosome << "\n";
        //}
        //TCOUT << endl;
    }

    template<typename TGene>
    void PatternCrossOver<TGene>::CrossOver(std::vector<Individual<TGene>*>& individuals) const
    {
        const size_t numInd = individuals.size();

        if (2 > numInd ||
            this->m_randomService.GetUniformDouble(0, 1) >= this->crossOverProbability ||
            this->AllTheSame(individuals))
        {
            return;
        }

        // TCOUT << "Before pattern cross over:\n";
        // for(const auto& ind : individuals)
        // {
        //     TCOUT << ind.chromosome << "\n";
        // }
        // TCOUT << endl;

        const size_t chromLength = individuals.front()->chromosome.size();
        std::vector<size_t> indexes(numInd);
        std::iota(indexes.begin(), indexes.end(), 0);
        std::vector<std::vector<size_t>> pattern(chromLength, indexes);
        std::vector<bool> affected(numInd, false);

        //shuffles until all the individuals are affected by the crossover
        do
        {
            for (auto& v : pattern)
            {
                this->m_randomService.Shuffle(v);
            }
            //checks if the individuals are affected
            for (size_t i = 0; i < pattern.size(); i++)
            {
                for (size_t j = 0; j < pattern[i].size(); j++)
                {
                    affected[j] = affected[j] || (pattern[i][j] != pattern[(i - 1) % chromLength][j]);  //sets affected j to true if the two indexes are not the same
                }
            }
        } while (!std::all_of(affected.cbegin(), affected.cend(), [](bool b) {return b; }));

        //TCOUT << "Crossover pattern: \n";
        //for (size_t i = 0; i < chromLength; i++)
        //{
        //    TCOUT << pattern[i] << "\n";
        //}
        //TCOUT << endl;

       //do the actual crossover based on the pattern
        std::vector<std::vector<TGene>> offspringChromosomes;

        for (size_t i = 0; i < numInd; i++)
        {
            offspringChromosomes.emplace_back();
            offspringChromosomes.back().resize(chromLength);
            for (size_t j = 0; j < chromLength; j++)
            {
                offspringChromosomes.back()[j] = individuals[pattern[j][i]]->chromosome[j];
                //TCOUT << "i, j:" << i << ", " << j << endl;
                //TCOUT << "individual" << i << ": " << individuals[i].chromosome << endl;
                //TCOUT << "pattern[j][i]:" << pattern[j][i] << endl;
                //TCOUT << "parent:" << parents[pattern[j][i]].chromosome[j] << endl;
            }
        }

        for (size_t i = 0; i < numInd; i++)
        {
            individuals[i]->chromosome = std::move(offspringChromosomes[i]);
            individuals[i]->fitnessValues.clear();
        }

        //TCOUT << "After pattern cross over:\n";
        //for(const auto& ind : individuals)
        //{
        //    TCOUT << ind.chromosome << "\n";
        //}
        //TCOUT << endl;
    }
}