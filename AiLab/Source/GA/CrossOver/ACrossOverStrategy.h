#pragma once
#include "AiLab/Source/GA/Individual.h"

class CrossoverStrategyTestFixture_AllTheSame_True_Test;
class CrossoverStrategyTestFixture_AllTheSame_False_Test;

namespace ai_lab
{
	template<typename TGene>
	class ACrossOverStrategy
	{
	public:
		explicit ACrossOverStrategy(double crossOverProbability);
		virtual ~ACrossOverStrategy() = default;

		virtual void CrossOver(std::vector<Individual<TGene>>& individuals) const = 0;
		virtual void CrossOver(std::vector<Individual<TGene>*>& individuals) const = 0;

		friend class ::CrossoverStrategyTestFixture_AllTheSame_True_Test;
		friend class ::CrossoverStrategyTestFixture_AllTheSame_False_Test;

	public:
		double crossOverProbability = 0.0;

	protected:
		virtual bool AllTheSame(std::vector<Individual<TGene>>& individuals) const;
		virtual bool AllTheSame(std::vector<Individual<TGene>*>& individuals) const;

	protected:
		utils::RandomService& m_randomService;
	};

	//===================================================================================================================================================================================

	template<typename TGene>
	ACrossOverStrategy<TGene>::ACrossOverStrategy(double crossOverProbability)
		: crossOverProbability(crossOverProbability)
		, m_randomService(utils::RandomService::GetInstance())
	{}

	template<typename TGene>
	bool ACrossOverStrategy<TGene>::AllTheSame(std::vector<Individual<TGene>>& individuals) const
	{
		std::vector<Individual<TGene>*> pVec;
		for (auto& ind : individuals)
		{
			pVec.emplace_back(&ind);
		}

		return AllTheSame(pVec);
	}

	template<typename TGene>
	bool ACrossOverStrategy<TGene>::AllTheSame(std::vector<Individual<TGene>*>& individuals) const
	{
		return std::adjacent_find(individuals.begin(), individuals.end(),
			[](const Individual<TGene>* a, const Individual<TGene>* b) {return a->chromosome != b->chromosome; }) == individuals.end();
	}
}