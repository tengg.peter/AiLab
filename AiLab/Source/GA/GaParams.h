#pragma once
//lib
#include "Utils/Utils.h"
//std
#include <iostream>

namespace ai_lab
{
	struct GaParams
	{
		virtual ~GaParams() = default;

		//default: 0
		int popSize = 0;
		//[including, excluding)
		//default: all genes are between 0.0 and 1.0
		std::vector<std::pair<double, double>> geneValueRanges;
		//default: 0
		int chromLength = 0;
		//default: 0.5
		double crossoverProbability = 0.5;
		//default: 0.01
		double mutationProbability = 0.01;
		//default: true
		bool elitism = true;
		//default: 2
		size_t selectionGroupSize = 2;
	};

	inline utils::OStream& operator<<(utils::OStream& os, const GaParams& params)
	{
		os << "GaParams:" <<
			"\n\tpopSize: " << params.popSize <<
			"\n\tchromLength: " << params.chromLength;
		for(const auto& [min, max] : params.geneValueRanges)
		{
			os << "\n\t\t[" << min << ", " << max << ")";
		}
		os << "\n\tcrossoverProb: " << params.crossoverProbability <<
			"\n\tmutationProb: " << params.mutationProbability <<
			"\n\telitism: " << params.elitism;

		return os;
	}
}

