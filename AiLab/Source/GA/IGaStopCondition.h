#pragma once

namespace ai_lab
{
	class IGaStopCondition
	{
	public:
		virtual ~IGaStopCondition() = default;

	public:
		virtual bool operator()(int numGenerations, double bestFitness, double averageFitness) = 0;
	};
}

