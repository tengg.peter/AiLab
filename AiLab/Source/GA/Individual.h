#pragma once
//lib
#include "Utils/Utils.h"
//std
#include <vector>


namespace ai_lab
{
	template<typename TGene = double>
	class Individual
	{
	public:
		static void InitChromosome(std::vector<TGene>& chromosome, const std::vector<std::pair<TGene, TGene>>& geneValueRanges);
		static std::pair<TGene, TGene> DefaultGeneRange();

	public:
		explicit Individual(const std::vector<std::pair<TGene, TGene>>& geneValueRanges = {});
		explicit Individual(size_t chromLength);
		virtual ~Individual() = default;

		const std::vector<std::pair<TGene, TGene>>& GeneValueRanges() const { return m_geneValueRanges; }
		void SetGeneWithinRange(size_t i, TGene value);

		std::vector<TGene> chromosome;
		std::vector<double> fitnessValues;

	protected:
		std::vector<std::pair<TGene, TGene>> m_geneValueRanges;
	};

	//=====================================================================================================================================================================================

	template<typename TGene>
	void Individual<TGene>::InitChromosome(std::vector<TGene>& chromosome, const std::vector<std::pair<TGene, TGene>>& geneValueRanges)
	{
		for (int i = 0; i < geneValueRanges.size(); i++)
		{
			if (geneValueRanges[i].first > geneValueRanges[i].second)
			{
				throw std::logic_error("min cannot be greater than max");
			}

			chromosome[i] = utils::RandomService::GetInstance().GetUniform(geneValueRanges[i].first, geneValueRanges[i].second);
		}
	}

	template<typename TGene>
	std::pair<TGene, TGene> Individual<TGene>::DefaultGeneRange()
	{
		if constexpr (std::is_same<double, TGene>::value)
		{
			return { 0.0, 1.0 };
		}
		else if constexpr (std::is_same<int, TGene>::value)
		{
			return { 0, 100 };
		}
		else
		{
			[]<bool flag = false>()
            	{ static_assert(flag, "Individual::DefaultGeneRange(): Type not supported."); }();
		}
	}

	template<typename TGene>
	Individual<TGene>::Individual(const std::vector<std::pair<TGene, TGene>>& geneValueRanges)
		: chromosome(geneValueRanges.size())
		, fitnessValues()
		, m_geneValueRanges(geneValueRanges)
	{
		InitChromosome(chromosome, geneValueRanges);
	}

	template<typename TGene>
	Individual<TGene>::Individual(size_t chromLength)
		: Individual(std::vector<std::pair<TGene, TGene>>(chromLength, { 0.0, 1.0 }))
	{}

	template<typename TGene>
	void Individual<TGene>::SetGeneWithinRange(size_t i, TGene value)
	{
		chromosome[i] = std::min(std::max(m_geneValueRanges[i].first, value), m_geneValueRanges[i].second);
	}

	template<typename TGene>
	bool operator==(const Individual<TGene>& a, const Individual<TGene>& b)
	{
		return std::equal(a.chromosome.cbegin(), a.chromosome.cend(), b.chromosome.cbegin(), [](double a, double b)
			{
				return utils::Equals(a, b);
			});
	}

	template<typename TGene>
	bool operator<(const Individual<TGene>& a, const Individual<TGene>& b)
	{
		return a.chromosome < b.chromosome;
	}
}