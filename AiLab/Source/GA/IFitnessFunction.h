#pragma once

#include "Individual.h"
namespace ai_lab
{
	template<typename TGene>
	class IFitnessFunction
	{
	public:
		virtual ~IFitnessFunction() = default;

	public:
		virtual void Calculate(Individual<TGene>& individual) const = 0;
		virtual bool InverseFitness() const = 0;
	};
}