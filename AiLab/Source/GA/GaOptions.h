#pragma once
#include "Utils/Utils.h"
//std
#include <vector>

namespace ai_lab
{
	struct GaOptions
	{
		virtual ~GaOptions() = default;

		//default: false
        bool printDetails = false;
		//default: "Output/GA/"
        utils::String outputDir{STR("Output/GA/")};
        //default: {}
        std::vector<utils::String> geneNames;
        //default: 1
        unsigned int maxFitnessThreads = 1;
	};

    inline utils::OStream& operator<<(utils::OStream& os, const GaOptions& options)
	{
		os << "GaOptions:" <<
            "\n\tprintDetails: " << options.printDetails<<
			"\n\toutputDir: \"" << options.outputDir << "\"" <<
            "\n\tgeneNames: ";
		for(const auto& geneName : options.geneNames)
		{
			os << "\"" << geneName << "\", ";
		}
		os << "\n\tmaxFitnessThreads: " << options.maxFitnessThreads;;

		return os;
	}
}

