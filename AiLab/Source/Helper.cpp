#include "Helper.h"

namespace ai_lab::impl
{
	using std::vector;

	void MakeTrainingAndTestData(
		const vector<SpTrainingSample>& availableData, 
		vector<const TrainingSample*>& trainingData, 
		vector<const TrainingSample*>& testData, 
		const double testSamplingFrequency)
	{
		trainingData.clear();
		testData.clear();

		const int testSamplingPeriod = static_cast<int>(1 / testSamplingFrequency);
		for (int i = 0; i < availableData.size(); i++)
		{
			if (0 < testSamplingFrequency &&
				0 == i % testSamplingPeriod)
			{
				testData.push_back(availableData[i].get());
			}
			else
			{
				trainingData.push_back(availableData[i].get());
			}
		}
	}
}
