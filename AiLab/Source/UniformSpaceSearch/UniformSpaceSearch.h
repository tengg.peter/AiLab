#pragma once
#include "../Common/AObjectiveFunction.h"
#include "../Common/NonDominatedSorting.h"
#include "Counter.h"
#include "SolutionCandidate.h"
//lib
#include "Utils/Utils.h"
//std
#include <set>
#include <utility>	//std::pair

namespace ai_lab
{
	namespace impl
	{
		template<typename TInt>
		uint32_t RoundToInt(double d)
		{
			return static_cast<TInt>(std::lround(d));
		}

		template<typename TParam>
		std::vector<TParam> GetBestParamPerDimension(
			const std::vector<SolutionCandidate<TParam>>& candidates,
			const std::vector<Objective>& objectives)
		{
			std::vector<TParam> bestScores;
			std::vector<TParam> bestParams = candidates.front().params;
			for (size_t dim = 0; dim < objectives.size(); dim++)
			{
				const auto obj = objectives[dim];
				if (Objective::Maximise == obj)
				{
					bestScores.emplace_back(-std::numeric_limits<TParam>::max());
				}
				else	//minimise
				{
					bestScores.emplace_back(std::numeric_limits<TParam>::max());
				}
				for (const auto& cand : candidates)
				{
					if ((Objective::Maximise == obj && cand.scores[dim] > bestScores[dim]) ||
						(Objective::Minimise == obj && cand.scores[dim] < bestScores[dim]))
					{
						bestScores[dim] = cand.scores[dim];
						bestParams[dim] = cand.params[dim];
					}
				}
			}
			return bestParams;
		}

		template<typename TParam>
		std::vector<std::pair<TParam, TParam>> CalculateNewParamRanges(
			const std::vector<TParam>& bestParams,
			const std::vector<double>& stepSizePerDimension,
			const std::vector<std::pair<TParam, TParam>>& oldParamRanges)
		{
			std::vector<std::pair<TParam, TParam>> newParamRanges(oldParamRanges.size());
			for (size_t dim = 0; dim < bestParams.size(); dim++)
			{
				using namespace utils::math;
				const double remainder = std::fmod(bestParams[dim], stepSizePerDimension[dim]);
				const bool wholeStep = utils::Equals(remainder, stepSizePerDimension[dim], 0.001) || remainder < 0.05 * stepSizePerDimension[dim];
				if constexpr (std::is_same<double, TParam>::value)
				{
					if (wholeStep)
					{
						//this parameter falls on a whole step. We take the neighbouring steps as limits
						TParam roundParam = RoundToNearestMultipleOf(bestParams[dim], stepSizePerDimension[dim]);
						newParamRanges[dim].first = std::max(roundParam - stepSizePerDimension[dim], oldParamRanges[dim].first);
						newParamRanges[dim].second = std::min(roundParam + stepSizePerDimension[dim], oldParamRanges[dim].second);
					}
					else	//random sample, far enough from steps. We take the closest steps
					{
						newParamRanges[dim].first = std::max(FloorToNearestMultipleOf(bestParams[dim], stepSizePerDimension[dim]), oldParamRanges[dim].first);
						newParamRanges[dim].second = std::min(CeilToNearestMultipleOf(bestParams[dim], stepSizePerDimension[dim]), oldParamRanges[dim].second);
					}
				}
				else	//int
				{
					if (wholeStep)
					{
						//this parameter falls on a whole step. We take the neighbouring steps as limits
						newParamRanges[dim].first = std::max<int>(RoundToInt<TParam>(bestParams[dim] - stepSizePerDimension[dim]), oldParamRanges[dim].first);
						newParamRanges[dim].second = std::min<int>(RoundToInt<TParam>(bestParams[dim] + stepSizePerDimension[dim]), oldParamRanges[dim].second);
					}
					else	//random sample, far enough from steps. We take the closest steps
					{
						newParamRanges[dim].first = std::max<int>(RoundToInt<TParam>(FloorToNearestMultipleOf<double>(bestParams[dim], stepSizePerDimension[dim])), oldParamRanges[dim].first);
						newParamRanges[dim].second = std::min<int>(RoundToInt<TParam>(CeilToNearestMultipleOf<double>(bestParams[dim], stepSizePerDimension[dim])), oldParamRanges[dim].second);
					}
				}
			}
			return newParamRanges;
		}

		template<typename TParam>
		struct CompareVectors
		{
			bool operator()(const std::vector<TParam>& a, const std::vector<TParam>& b) const
			{
				if constexpr (std::is_same<int, TParam>::value)
				{
					return a < b;
				}
				else	//double
				{
					for (size_t i = 0; i < a.size(); i++)
					{
						if (!utils::Equals(a[i], b[i], 0.01))
						{
							return a[i] < b[i];
						}
					}
					return false;
				}
			}
		};

		template<typename TParam>
		void PrintParamRanges(const std::vector<std::pair<TParam, TParam>>& paramValueRanges)
		{
			TCOUT << "Searching in: ";
			for (size_t i = 0; i < paramValueRanges.size(); i++)
			{
				if (0 < i)
				{
					TCOUT << ", ";
				}
				TCOUT << "(" << paramValueRanges[i].first << ", " << paramValueRanges[i].second << ")";
			}
			TCOUT << std::endl;
		}

		inline bool IsAborting(const std::atomic_bool* const pAborting)
		{
			return pAborting && pAborting->load();
		}

	}	// namespace impl

	template<typename TParam>
	std::vector<SolutionCandidate<TParam>> UniformSpaceSearch(
		uint32_t maxTestRuns,
		uint32_t refinementIterations,
		const std::vector<std::pair<TParam, TParam>>& paramValueRanges,
		AObjectiveFunction<SolutionCandidate, TParam>& objectiveFunction,
		bool printProgress,
		const std::atomic_bool* const pAborting = nullptr
		)
	{
		static constexpr int maxSetInsertAttempts = 100;

		const size_t numDimensions = paramValueRanges.size();
		const uint32_t runsPerIteration = maxTestRuns / refinementIterations;
		const uint32_t stepsPerDimension = impl::RoundToInt<uint32_t>(std::pow(runsPerIteration, 1.0 / numDimensions));
		const uint32_t numRegularSamples = std::min(impl::RoundToInt<uint32_t>(std::pow(stepsPerDimension, numDimensions)), runsPerIteration);
		const uint32_t numRandomSamples = runsPerIteration - numRegularSamples;

		std::set<std::vector<TParam>, impl::CompareVectors<TParam>> usedCombinations;
		std::vector<SolutionCandidate<TParam>> candidates;
		std::vector<SolutionCandidate<TParam>> testedCandidates;
		std::vector<double> stepSizePerDimension(numDimensions);
		auto paramRangesPerDimenson(paramValueRanges);

		utils::Timer totalTimer;
		totalTimer.Start();

		Counter counter(stepsPerDimension, numDimensions);
		for (size_t iter = 0; iter < refinementIterations; iter++)
		{
			if (impl::IsAborting(pAborting))
			{
				return {};
			}

			utils::Timer iterationTimer;
			iterationTimer.Start();

			if (printProgress)
			{
				impl::PrintParamRanges(paramRangesPerDimenson);
			}

			for (size_t dim = 0; dim < numDimensions; dim++)
			{
				stepSizePerDimension[dim] = std::abs(static_cast<double>(paramRangesPerDimenson[dim].second - paramRangesPerDimenson[dim].first)) / (stepsPerDimension - 1);
			}

			bool counterEnded = false;
			{
				int unsuccessfulInserts = 0;
				while (runsPerIteration > candidates.size())
				{
					SolutionCandidate<TParam> cand;
					cand.params.resize(numDimensions);
					for (size_t dim = 0; dim < numDimensions; dim++)
					{
						if constexpr (std::is_same<double, TParam>::value)
						{
							cand.params[dim] = paramRangesPerDimenson[dim].first + stepSizePerDimension[dim] * counter.State()[dim];
						}
						else
						{
							cand.params[dim] = impl::RoundToInt<TParam>(paramRangesPerDimenson[dim].first + stepSizePerDimension[dim] * counter.State()[dim]);
						}
					}
					if (objectiveFunction.ValidateParams(cand.params))
					{
						if (usedCombinations.insert(cand.params).second)
						{
							candidates.emplace_back(std::move(cand));
						}
						else if (counter.SpaceSize() <= ++unsuccessfulInserts)
						{
							break;
						}
					}
					counterEnded = !counter.Step();
					if (counterEnded)
					{
						break;
					}
				}
			}
			if (counterEnded)
			{
				if (paramValueRanges != paramRangesPerDimenson)
				{
					paramRangesPerDimenson = paramValueRanges;	//impossible param ranges reset to the original and tries again
					--iter;
					continue;
				}
			}


			auto& rand = utils::RandomService::GetInstance();
			int unsuccessfulInserts = 0;
			while (runsPerIteration > candidates.size())	//fills up the rest with random points
			{
				SolutionCandidate<TParam> cand;
				cand.params.resize(numDimensions);
				for (size_t dim = 0; dim < numDimensions; dim++)
				{
					cand.params[dim] = rand.GetUniform<TParam>(paramRangesPerDimenson[dim].first, paramRangesPerDimenson[dim].second);
				}
				if (objectiveFunction.ValidateParams(cand.params) && usedCombinations.insert(cand.params).second)
				{
					candidates.emplace_back(std::move(cand));
					unsuccessfulInserts = 0;
				}
				else if (maxSetInsertAttempts <= ++unsuccessfulInserts)
				{
					break;
				}
			}

			if (printProgress)
			{
				TCOUT << "\nIteration " << iter + 1 << " ===========================================================================================================================" << std::endl;
			}

			int count = 0;

			auto lambda = [&objectiveFunction, &count, iter, numCandidates = candidates.size(), maxTestRuns, printProgress](SolutionCandidate<TParam>& candidate)
			{
				objectiveFunction.Calculate(candidate);
				if (printProgress)
				{
					static std::mutex mut;
					mut.lock();
					TCOUT << "Calculated " << ++count << "/" << numCandidates << " (" << maxTestRuns << " in total) candidates of iteration " << iter + 1 << std::endl;
					mut.unlock();
				}
			};

			utils::ParallelForEach(candidates, lambda, 0 /*threadHint*/, pAborting);
			if (impl::IsAborting(pAborting))
			{
				return {};
			}

			const auto bestParams = impl::GetBestParamPerDimension(candidates, objectiveFunction.Objectives());
			testedCandidates.insert(testedCandidates.end(), std::make_move_iterator(candidates.begin()), std::make_move_iterator(candidates.end()));
			candidates.clear();
			counter.Reset();

			if (iter < refinementIterations - 1)
			{
				paramRangesPerDimenson = impl::CalculateNewParamRanges(bestParams, stepSizePerDimension, paramValueRanges);
			}
			if (printProgress)
			{
				TCOUT << "\nIteration time: " << iterationTimer.ElapsedTime() << '\n';
				TCOUT << "Total time: " << totalTimer.ElapsedTime() << '\n';
			}
		}	// for refinementInterations

		const auto fronts = NonDominatedSorting(testedCandidates, objectiveFunction.Objectives());
		std::vector<SolutionCandidate<TParam>> ret;
		std::transform(fronts.front().cbegin(), fronts.front().cend(), std::back_inserter(ret), [](const auto pCand)
			{
				return *pCand;
			});
		return ret;
	}	// UniformSpaceSearch	
}// namespace ai_lab