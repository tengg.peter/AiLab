#pragma once

#include <vector>

namespace ai_lab
{
	template<typename TParam>
	class SolutionCandidate
	{
	public:
		virtual ~SolutionCandidate() = default;

		const std::vector<TParam>& Params() const { return params; }
		std::vector<TParam>& Params() { return params; }

		const std::vector<double>& Scores() const { return scores; }
		std::vector<double>& Scores() { return scores; }

		std::vector<TParam> params;
		std::vector<double> scores;
	};

	template<typename TParam>
	bool operator<(const SolutionCandidate<TParam>& a, const SolutionCandidate<TParam>& b)
	{
		return a.params < b.params;
	}
}