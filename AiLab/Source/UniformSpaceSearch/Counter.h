#pragma once

#if __linux__
	#include <cstddef>	// size_t
	#include <stdint.h>	// uint32_t
#endif
#include <vector>

namespace ai_lab
{
	class Counter
	{
	public:
		explicit Counter(uint32_t base, size_t numDigits);
		virtual ~Counter() = default;

		bool Step();
		void Reset();
		const std::vector<uint32_t>& State() const { return m_state; }
		size_t SpaceSize() const;

	private:
		const uint32_t m_base;
		std::vector<uint32_t> m_state;
	};
}