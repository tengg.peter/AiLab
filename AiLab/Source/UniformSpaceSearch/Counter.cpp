#include "Counter.h"
//lib
//std
#include <cmath>
#include <stdexcept>

namespace ai_lab
{
	Counter::Counter(uint32_t base, size_t numDigits)
		: m_base(base)
		, m_state(numDigits)
	{
		if (1 >= base || 0 == numDigits)
		{
			throw std::invalid_argument("Invalid argument");
		}
	}

	bool Counter::Step()
	{
		for (size_t i = m_state.size() - 1; i >= 0; i--)
		{
			if (m_base - 1 == m_state[i])
			{
				//carry over
				m_state[i] = 0;
				if (0 == i)
				{
					return false;	//back to 0
				}
			}
			else
			{
				++m_state[i];
				break;
			}
		}
		return true;
	}
	void Counter::Reset()
	{
		std::fill(m_state.begin(), m_state.end(), 0);
	}
	size_t Counter::SpaceSize() const
	{
		return std::pow<size_t>(m_base, m_state.size());
	}
}