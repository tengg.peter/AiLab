//own
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/Include.h"
#include "Mlp.h"
#include "Utils/Utils.h"

namespace ai_lab
{
	using std::vector;
	using utils::String;

	Mlp Mlp::FromJson(const String jsonString)
	{
		Mlp mlp;

		Json::Value rootJson;
		Json::Reader reader;
		reader.parse(utils::string_conversion::UtilsStringToStdString(jsonString), rootJson);

		//activation functions
		mlp.m_activationFunctions.resize(rootJson[ACTIVATION_FUNCTIONS_JSON_NAME].size());
		for (int l = 0; l < rootJson[ACTIVATION_FUNCTIONS_JSON_NAME].size(); l++)
		{
			for (int n = 0; n < rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l].size(); n++)
			{
				const String functionTypeStr = utils::string_conversion::StdStringToUtilsString(
					rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l][n][FUNCTION_TYPE_JSON_NAME].asString());

				DcpActivationFunction dcpActivationFunction(AActivationFunction::S_CloneRawPointer);
				if (functionTypeStr == LinearScalingActivationFunction::TYPE_STRING)
				{
					auto pLinearFunc = new LinearScalingActivationFunction();
					pLinearFunc->MinValue(rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l][n][MIN_VALUE_JSON_NAME].asDouble());
					pLinearFunc->MaxValue(rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l][n][MAX_VALUE_JSON_NAME].asDouble());
					dcpActivationFunction.Reset(pLinearFunc);
				}
				else if (functionTypeStr == LeakyReluActivationFunction::TYPE_STRING)
				{
					dcpActivationFunction.Reset(new LeakyReluActivationFunction(
						rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l][n][SCALE_JSON_NAME].asDouble(),
						rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l][n][OFFSET_JSON_NAME].asDouble()
					));
				}
				else if (functionTypeStr == IdentityActivationFunction::TYPE_STRING)
				{
					dcpActivationFunction.Reset(new IdentityActivationFunction(
						rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l][n][SCALE_JSON_NAME].asDouble(),
						rootJson[ACTIVATION_FUNCTIONS_JSON_NAME][l][n][OFFSET_JSON_NAME].asDouble()
					));
				}
				else
				{
					throw std::logic_error("Unknown function type");
				}

				mlp.m_activationFunctions[l].push_back(dcpActivationFunction);
			}
		}	//activation functions

		//weights
		mlp.m_weights.resize(rootJson[WEIGHTS_JSON_NAME].size());
		for (int l = 0; l < rootJson[WEIGHTS_JSON_NAME].size(); l++)
		{
			mlp.m_weights[l].resize(rootJson[WEIGHTS_JSON_NAME][l].size());
			for (int n = 0; n < rootJson[WEIGHTS_JSON_NAME][l].size(); n++)
			{
				mlp.m_weights[l][n].resize(rootJson[WEIGHTS_JSON_NAME][l][n].size());
				for (int w = 0; w < rootJson[WEIGHTS_JSON_NAME][l][n].size(); w++)
				{
					mlp.m_weights[l][n][w] = rootJson[WEIGHTS_JSON_NAME][l][n][w].asDouble();
				}
			}
		}	//weights

		//biases
		mlp.m_biases.resize(rootJson[BIASES_JSON_NAME].size());
		for (int l = 0; l < rootJson[BIASES_JSON_NAME].size(); l++)
		{
			mlp.m_biases[l].resize(rootJson[BIASES_JSON_NAME][l].size());
			for (int n = 0; n < rootJson[BIASES_JSON_NAME][l].size(); n++)
			{
				mlp.m_biases[l][n] = rootJson[BIASES_JSON_NAME][l][n].asDouble();
			}
		}	//biases

		//output function
		if (rootJson.isMember(OUTPUT_FUNCTION_JSON_NAME))
		{
			if (SoftmaxOutputFunction::TYPE_STRING == utils::string_conversion::StdStringToUtilsString(rootJson[OUTPUT_FUNCTION_JSON_NAME].asString()))
			{
				mlp.m_dcpOutputFunction.Reset(new SoftmaxOutputFunction());
			}
		}

		//metadata
		mlp.trainingError = rootJson[METADATA_JSON_NAME][TRAINING_ERROR_JSON_NAME].asDouble();
		mlp.generalisationError = rootJson[METADATA_JSON_NAME][GENERALISATION_ERROR_JSON_NAME].asDouble();
		mlp.trainingLoss = rootJson[METADATA_JSON_NAME][TRAINING_LOSS_JSON_NAME].asDouble();
		mlp.generalisationLoss = rootJson[METADATA_JSON_NAME][GENERALISATION_LOSS_JSON_NAME].asDouble();
		mlp.numEpochs = rootJson[METADATA_JSON_NAME][NUM_EPOCHS_JSON_NAME].asInt();
		mlp.numTrainingSamples = rootJson[METADATA_JSON_NAME][NUM_TRAINING_SAMPLES_JSON_NAME].asInt();
		mlp.numTestSamples = rootJson[METADATA_JSON_NAME][NUM_TEST_SAMPLES_JSON_NAME].asInt();
		mlp.trainingType = rootJson[METADATA_JSON_NAME][TRAINING_TYPE_JSON_NAME].asInt();
		mlp.trainingTimeString = utils::string_conversion::StdStringToUtilsString(rootJson[METADATA_JSON_NAME][TRAINING_TIME_JSON_NAME].asString());
		if (rootJson[METADATA_JSON_NAME].isMember(BACK_PROP_PARAMS_JSON_NAME))
		{
			mlp.backPropParamsOptional.emplace();
			mlp.backPropParamsOptional->learningRate = rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][LEARNING_RATE_JSON_NAME].asDouble();
			mlp.backPropParamsOptional->momentum = rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][MOMENTUM_JSON_NAME].asDouble();
			mlp.backPropParamsOptional->miniBatchSize = rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][MINI_BATCH_SIZE_JSON_NAME].asInt();
			mlp.backPropParamsOptional->weightInitialisation = WeightInitialisation::FromString(
				utils::string_conversion::StdStringToUtilsString(
					rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][WEIGHT_INIT_JSON_NAME].asString()));
		}
		if (rootJson[METADATA_JSON_NAME].isMember(NOTES_JSON_NAME))
		{
			for (const auto& key : rootJson[METADATA_JSON_NAME][NOTES_JSON_NAME].getMemberNames())
			{
				mlp.notes[utils::string_conversion::StdStringToUtilsString(key)] =
					utils::string_conversion::StdStringToUtilsString(
						rootJson[METADATA_JSON_NAME][NOTES_JSON_NAME][key].asString());
			}
		}

		return mlp;
	}

	void Mlp::SetupInputLayerScaling(const vector<SpTrainingSample>& trainingData)
	{
		for (const SpTrainingSample& spTrainingSample : trainingData)
		{
			const vector<double>& input = spTrainingSample->Input();
			for (size_t i = 0; i < input.size(); i++)
			{
				//Throws exception if dynamic cast cannot be done
				LinearScalingActivationFunction& func = dynamic_cast<LinearScalingActivationFunction&>(*m_activationFunctions.front()[i]);
				if (func.MinValue() > input[i])
				{
					func.MinValue(input[i]);
				}
				else if (func.MaxValue() < input[i])
				{
					func.MaxValue(input[i]);
				}
			}
		}
	}

	void Mlp::InitRandomWeights(WeightInitialisation weightInitialisation)
	{
		if (WeightsInitialised())
		{
			throw std::logic_error("The weights have already been initialised.");
		}

		for (size_t l = 0; l < m_weights.size(); l++)
		{
			const size_t numInputUnits = m_weights[l].size();
			const size_t numOutputUnits = m_weights[l].front().size();
			for (size_t n = 0; n < m_weights[l].size(); n++)
			{
				for (size_t w = 0; w < m_weights[l][n].size(); w++)
				{
					m_weights[l][n][w] = utils::RandomService::GetInstance().GetGaussianDouble(0, 1)
						* sqrt(2.0 / (numInputUnits + numOutputUnits));	//Xavier
				}
			}
		}
	}

	bool Mlp::WeightsInitialised()
	{
		return !m_weights.empty() &&
			(0 != m_weights.front().front().front() ||
				0 != m_weights.front().back().back());
	}

	double Mlp::CurrentActivation(size_t layerIdx, size_t neuronIdx) const
	{
		return m_activationFunctions[layerIdx + 1][neuronIdx]->Activation();
	}

	double Mlp::DerivativeOfCurrentActivation(size_t layerIdx, size_t neuronIdx) const
	{
		double d = m_activationFunctions[layerIdx + 1][neuronIdx]->Derivative();
		if (layerIdx == m_activationFunctions.size() - 2 && m_dcpOutputFunction)
		{
			return d * m_dcpOutputFunction->DerivativeOfCurrentOutput(neuronIdx);
		}
		return d;
	}

	double Mlp::Weight(size_t layerIdx, size_t neuronIdx, size_t weightIdx) const
	{
		return m_weights[layerIdx][neuronIdx][weightIdx];
	}

	void Mlp::AdjustWeight(size_t layerIdx, size_t neuronIdx, size_t weightIdx, double delta)
	{
		m_weights[layerIdx][neuronIdx][weightIdx] += delta;
	}

	void Mlp::AdjustBias(size_t layerIdx, size_t neuronIdx, double delta)
	{
		m_biases[layerIdx][neuronIdx] += delta;
	}

	NeuralNetType Mlp::Type() const
	{
		if (m_dcpOutputFunction && m_dcpOutputFunction->TypeString() == SoftmaxOutputFunction::TYPE_STRING)
		{
			return NeuralNetType::Classifier;
		}
		return NeuralNetType::Regressor;
	}

	void Mlp::AddLayer(const utils::String & activationFunctionType, const int numNeurons)
	{
		AddLayer(activationFunctionType, utils::String(), numNeurons);
	}

	void Mlp::AddLayer(
		const utils::String & activationFunctionType,
		const utils::String & outputFunctionType,
		const int numNeurons)
	{
		if (WeightsInitialised())
		{
			throw std::logic_error("The weights have alread been initialised. "
				"The network structure cannot be altered any more.");
		}

		m_activationFunctions.emplace_back();
		for (size_t n = 0; n < numNeurons; n++)
		{
			m_activationFunctions.back().push_back(
				DcpActivationFunction(
					AActivationFunction::CreateNewRawPointer(activationFunctionType),
					AActivationFunction::S_CloneRawPointer));
		}

		if (m_activationFunctions.size() > 1)
		{
			m_weights.emplace_back();
			m_weights.back().resize(numNeurons);
			for (std::vector<double>& wv : m_weights.back())
			{
				wv.resize((m_activationFunctions.crbegin() + 1)->size());
			}

			m_biases.emplace_back();
			m_biases.back().resize(numNeurons);
		}

		if (!outputFunctionType.empty())
		{
			m_dcpOutputFunction.Reset(AOutputFunction::CreateNewRawPointer(outputFunctionType));
		}
	}

	vector<double> Mlp::FeedForward(const vector<double>& input) const
	{
		for (size_t i = 0; i < input.size(); i++)
		{
			m_activationFunctions.front()[i]->CalculateOutput(input[i]);
		}

		vector<double> output(m_weights.back().size());
		for (size_t l = 1; l < m_activationFunctions.size(); l++)
		{
			for (size_t n = 0; n < m_activationFunctions[l].size(); n++)
			{
				double weightedSumOfInputs = 0;
				for (size_t w = 0; w < m_weights[l - 1][n].size(); w++)
				{
					weightedSumOfInputs += m_weights[l - 1][n][w] * m_activationFunctions[l - 1][w]->Activation();
				}
				m_activationFunctions[l][n]->CalculateOutput(weightedSumOfInputs + m_biases[l - 1][n]);
				if (m_activationFunctions.size() - 1 == l)
				{
					output[n] = m_activationFunctions[l][n]->Activation();
				}
			}
		}
		if (m_dcpOutputFunction)
		{
			return m_dcpOutputFunction->CalculateOutput(output);
		}
		return output;
	}

	String Mlp::ToJson() const
	{
		Json::Value rootJson;
		//activation functions
		for (size_t l = 0; l < m_activationFunctions.size(); l++)
		{
			Json::Value layerJson;
			for (size_t n = 0; n < m_activationFunctions[l].size(); n++)
			{
				Json::Value activationFunctionJson;
				activationFunctionJson[FUNCTION_TYPE_JSON_NAME] = utils::string_conversion::UtilsStringToStdString(m_activationFunctions[l][n]->TypeString());
				activationFunctionJson[SCALE_JSON_NAME] = m_activationFunctions[l][n]->Scale();
				activationFunctionJson[OFFSET_JSON_NAME] = m_activationFunctions[l][n]->Offset();
				const auto pFunc = dynamic_cast<const LinearScalingActivationFunction*>(m_activationFunctions[l][n].operator->());
				if (nullptr != pFunc)
				{
					activationFunctionJson[MIN_VALUE_JSON_NAME] = pFunc->MinValue();
					activationFunctionJson[MAX_VALUE_JSON_NAME] = pFunc->MaxValue();
				}
				layerJson.append(activationFunctionJson);
			}
			rootJson[ACTIVATION_FUNCTIONS_JSON_NAME].append(layerJson);
		}

		//weights
		Json::Value weights;
		for (size_t l = 0; l < m_weights.size(); l++)
		{
			Json::Value layerJson;
			for (size_t n = 0; n < m_weights[l].size(); n++)
			{
				Json::Value neuronJson;
				for (size_t w = 0; w < m_weights[l][n].size(); w++)
				{
					neuronJson.append(m_weights[l][n][w]);
				}
				layerJson.append(neuronJson);
			}
			weights.append(layerJson);
		}
		rootJson[WEIGHTS_JSON_NAME] = weights;

		//biases
		Json::Value biasesJson;
		for (size_t l = 0; l < m_biases.size(); l++)
		{
			Json::Value layerJson;
			for (size_t n = 0; n < m_biases[l].size(); n++)
			{
				layerJson.append(m_biases[l][n]);
			}
			biasesJson.append(layerJson);
		}
		rootJson[BIASES_JSON_NAME] = biasesJson;

		//output function
		if (m_dcpOutputFunction)
		{
			rootJson[OUTPUT_FUNCTION_JSON_NAME] = utils::string_conversion::UtilsStringToStdString(m_dcpOutputFunction->TypeString());
		}

		//metadata
		rootJson[METADATA_JSON_NAME][TRAINING_ERROR_JSON_NAME] = trainingError;
		rootJson[METADATA_JSON_NAME][GENERALISATION_ERROR_JSON_NAME] = generalisationError;
		rootJson[METADATA_JSON_NAME][TRAINING_LOSS_JSON_NAME] = trainingLoss;
		rootJson[METADATA_JSON_NAME][GENERALISATION_LOSS_JSON_NAME] = generalisationLoss;
		rootJson[METADATA_JSON_NAME][NUM_EPOCHS_JSON_NAME] = numEpochs;
		rootJson[METADATA_JSON_NAME][NUM_TRAINING_SAMPLES_JSON_NAME] = numTrainingSamples;
		rootJson[METADATA_JSON_NAME][NUM_TEST_SAMPLES_JSON_NAME] = numTestSamples;
		rootJson[METADATA_JSON_NAME][TRAINING_TYPE_JSON_NAME] = trainingType;
		rootJson[METADATA_JSON_NAME][TRAINING_TIME_JSON_NAME] =
			utils::string_conversion::UtilsStringToStdString(trainingTimeString);
		if (backPropParamsOptional.has_value())
		{
			rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][LEARNING_RATE_JSON_NAME] =
				backPropParamsOptional->learningRate;
			rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][LOSS_FUNCTION_JSON_NAME] =
				utils::string_conversion::UtilsStringToStdString(backPropParamsOptional->dcpLossfunction->TypeString());
			rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][MOMENTUM_JSON_NAME] =
				backPropParamsOptional->momentum;
			rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][MINI_BATCH_SIZE_JSON_NAME] =
				backPropParamsOptional->miniBatchSize;
			rootJson[METADATA_JSON_NAME][BACK_PROP_PARAMS_JSON_NAME][WEIGHT_INIT_JSON_NAME] =
				utils::string_conversion::UtilsStringToStdString(backPropParamsOptional->weightInitialisation.ToUtilsString());
		}
		for (const auto&[key, value] : notes)
		{
			rootJson[METADATA_JSON_NAME][NOTES_JSON_NAME][utils::string_conversion::UtilsStringToStdString(key)] =
				utils::string_conversion::UtilsStringToStdString(value);
		}

		for (size_t i = 0; i < m_activationFunctions.size(); i++)
		{
			std::string structureString = std::to_string(m_activationFunctions[i].size());
			structureString += ": ";
			structureString += utils::string_conversion::UtilsStringToStdString(m_activationFunctions[i].front()->TypeString());
			rootJson[METADATA_JSON_NAME][STRUCTURE_JSON_NAME].append(structureString);
		}
		if (m_dcpOutputFunction)
		{
			rootJson[METADATA_JSON_NAME][STRUCTURE_JSON_NAME].append("Output function: " + utils::string_conversion::UtilsStringToStdString(m_dcpOutputFunction->TypeString()));
		}

		Json::StreamWriterBuilder builder;
		builder["indentation"] = " "; // If you want whitespace-less output
		return utils::string_conversion::StdStringToUtilsString(Json::writeString(builder, rootJson));
	}

	bool operator==(const Mlp & a, const Mlp & b)
	{
		if (&a == &b)
		{
			return true;
		}

		return
			a.m_activationFunctions == b.m_activationFunctions &&
			a.m_weights == b.m_weights &&
			a.m_biases == b.m_biases &&
			a.m_dcpOutputFunction == b.m_dcpOutputFunction &&

			//metadata
			a.backPropParamsOptional == b.backPropParamsOptional &&
			a.trainingError == b.trainingError &&
			a.generalisationError == b.generalisationError &&
			a.trainingLoss == b.trainingLoss &&
			a.generalisationLoss == b.generalisationLoss &&
			a.numEpochs == b.numEpochs &&
			a.numTrainingSamples == b.numTrainingSamples &&
			a.numTestSamples == b.numTestSamples &&
			a.trainingType == b.trainingType &&
			a.trainingTimeString == b.trainingTimeString &&
			a.notes == b.notes;
	}

	utils::OStream& operator<<(utils::OStream& os, const Mlp& mlp)
	{
		os << mlp.ToJson();
		return os;
	}
}