#pragma once
//own
#include "AiLab/Source/NeuralNetwork/LossFunctions/ALossFunction.h"
//std:
#include <cmath>

namespace ai_lab
{
	class LogarithmicErrorLossFunction : public ALossFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("LogarithmicErrorLossFunction");

		LogarithmicErrorLossFunction(const double base) : m_base(base) {}
		virtual ~LogarithmicErrorLossFunction() = default;

	public:
		virtual utils::String TypeString() const override { return TYPE_STRING; }
		virtual bool Equals(const ALossFunction& other) const override 
		{ 
			return TypeString() == other.TypeString() && 
				m_base == static_cast<const LogarithmicErrorLossFunction&>(other).m_base;	//this is safe to do, because this wont even run if the TypeStrings dont match
		}

		[[nodiscard]]
		virtual LogarithmicErrorLossFunction* CloneRawPointer() const override { return new LogarithmicErrorLossFunction(*this); }

	protected:
		// Inherited via ILossFunction
		virtual double CalculateSingleLoss(const double actual, const double expected) const override
		{
			throw std::runtime_error("Needs correct implementation and unit testing");
			//the +1 constant shifts the the curve, so it hits the X axis at 0 instead of 1
			return log(expected - actual + 1) / log(m_base);
		}
		virtual double Derivative(const double actual, const double expected) const override
		{
			throw std::runtime_error("Needs correct implementation and unit testing");
			return 1.0 / (log(m_base) * (expected - actual + 1));
		}

	private:
		double m_base;
	};
}