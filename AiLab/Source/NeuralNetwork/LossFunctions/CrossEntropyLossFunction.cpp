//own
#include "CrossEntropyLossFunction.h"
//std
#include <cmath>
#include <numeric>

namespace ai_lab
{
	double CrossEntropyLossFunction::CalculateSingleLoss(const double actual, const double expected) const
	{
		//https://sefiks.com/2017/12/17/a-gentle-introduction-to-cross-entropy-loss-function/
		if (1 == expected)	//this if-else realises the same as p * ... + (1 - p) * ..., which is the common way to express the cross entropy
		{
			return -1 * log(actual);
		}
		else if (0 == expected)
		{
			return -1 * log(1 - actual);
		}
		else
		{
			throw std::logic_error("A class label must be 0 or 1");
		}
	}

	double CrossEntropyLossFunction::Derivative(const double actual, const double expected) const
	{
		//https://sefiks.com/2017/12/17/a-gentle-introduction-to-cross-entropy-loss-function/
		if (1 == expected)	//this if-else realises the same as p * ... + (1 - p) * ..., which is the common way to express the cross entropy
		{
			return -1 * (1.0 / actual);
		}
		else if (0 == expected)
		{
			return 1.0 / (1 - actual);
		}
		else
		{
			throw std::logic_error("A class label must be 0 or 1");
		}
	}

	CrossEntropyLossFunction * CrossEntropyLossFunction::CloneRawPointer() const
	{
		return new CrossEntropyLossFunction(*this);
	}

	double CrossEntropyLossFunction::CalculateLoss(const std::vector<double>& actual, const std::vector<double>& expected) const
	{
		//no assert here: expected and actual dont have to be of the same size. 
		//Expected is a list of labels, most of the times one single label
		std::vector<double> classwiseLosses(actual.size(), 0);
		//losses for the true labels
		for (const double label : expected)
		{
			if (label >= actual.size())
			{
				continue;
			}
			classwiseLosses[label] = CalculateSingleLoss(actual[label], 1);
		}
		//losses for the false labels
		for (size_t i = 0; i < classwiseLosses.size(); i++)
		{
			if (0 != classwiseLosses[i])
			{
				continue;
			}
			classwiseLosses[i] = CalculateSingleLoss(actual[i], 0);
		}

		return std::accumulate(classwiseLosses.cbegin(), classwiseLosses.cend(), 0.0);
	}

	std::vector<double> CrossEntropyLossFunction::Derivatives(const std::vector<double>& actual, const std::vector<double>& expected) const
	{
		//no assert here: expected and actual dont have to be of the same size. 
		//Expected is a list of labels, most of the times one single label
		std::vector<double> derivatives(actual.size(), 0);
		//derivatives for the true labels
		for (const double label : expected)
		{
			if (label >= actual.size())
			{
				continue;
			}
			derivatives[label] = Derivative(actual[label], 1);
		}
		//derivatives for the false labels
		for (size_t i = 0; i < derivatives.size(); i++)
		{
			if (0 != derivatives[i])
			{
				continue;
			}
			derivatives[i] = Derivative(actual[i], 0);
		}

		return derivatives;
	}
}
