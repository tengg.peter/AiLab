#pragma once

#include "Utils/Utils.h"

#include <vector>

namespace ai_lab
{
	class ALossFunction
	{
	public:
		[[nodiscard]]
		static ALossFunction* S_CloneRawPointer(const ALossFunction& instance);

		virtual utils::String TypeString() const = 0;
		[[nodiscard]]
		virtual ALossFunction* CloneRawPointer() const = 0;

		virtual bool Equals(const ALossFunction& other) const { return TypeString() == other.TypeString(); }
		virtual double CalculateLoss(const std::vector<double>& actual, const std::vector<double>& expected) const;
		virtual std::vector<double> Derivatives(const std::vector<double>& actual, const std::vector<double>& expected) const;

		virtual ~ALossFunction() = default;

	protected:
		virtual double CalculateSingleLoss(const double actual, const double expected) const = 0;
		virtual double Derivative(const double actual, const double expected) const = 0;
	};

	bool operator==(const ALossFunction& a, const ALossFunction& b);
}