#include "AiLab/Source/NeuralNetwork/LossFunctions/ALossFunction.h"

#include <stdexcept>

namespace ai_lab
{
	ALossFunction * ALossFunction::S_CloneRawPointer(const ALossFunction & instance)
	{
		return instance.CloneRawPointer();
	}

	double ALossFunction::CalculateLoss(const std::vector<double>& actual, const std::vector<double>& expected) const
	{
		if (expected.size() != actual.size())
		{
			throw std::invalid_argument("actual and expected must have the same dimensions.");
		}

		double sumLoss = 0;
		for (size_t i = 0; i < expected.size(); i++)
		{
			sumLoss += CalculateSingleLoss(actual[i], expected[i]);
		}
		return sumLoss / expected.size();
	}

	std::vector<double> ALossFunction::Derivatives(const std::vector<double>& actual, const std::vector<double>& expected) const
	{
		if (expected.size() != actual.size())
		{
			throw std::invalid_argument("actual and expected must have the same dimensions.");
		}

		std::vector<double> derivatives;
		for (size_t i = 0; i < expected.size(); i++)
		{
			derivatives.push_back(Derivative(actual[i], expected[i]));
		}
		return derivatives;
	}

	bool operator==(const ALossFunction & a, const ALossFunction & b)
	{
		return &a == &b ? true :
			nullptr == &a ? false :
			nullptr == &b ? false :
			a.Equals(b);
	}
}
