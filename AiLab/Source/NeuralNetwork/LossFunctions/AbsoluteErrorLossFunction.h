#pragma once
#include "AiLab/Source/NeuralNetwork/LossFunctions/ALossFunction.h"

namespace ai_lab
{
	class AbsoluteErrorLossFunction : public ALossFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("AbsoluteErrorLossFunction");
		virtual utils::String TypeString() const override { return TYPE_STRING; }
		[[nodiscard]]
		virtual AbsoluteErrorLossFunction* CloneRawPointer() const override { return new AbsoluteErrorLossFunction(*this); }
		virtual ~AbsoluteErrorLossFunction() = default;

	protected:
		// Inherited via ILossFunction
		virtual double CalculateSingleLoss(const double actual, const double expected) const override
		{
			return std::abs(actual - expected);
		}
		virtual double Derivative(const double actual, const double expected) const override
		{
			return 
				actual == expected ? 0 :
				actual - expected > 0 ? 1 : -1;
		}
	};
}