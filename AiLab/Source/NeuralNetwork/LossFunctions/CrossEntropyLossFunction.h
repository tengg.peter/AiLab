#pragma once
#include "AiLab/Source/NeuralNetwork/LossFunctions/ALossFunction.h"

#include <string>
namespace ai_lab
{
	class CrossEntropyLossFunction : public ALossFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("CrossEntropyLossFunction");

		virtual ~CrossEntropyLossFunction() = default;

		// Inherited via ALossFunction
		virtual utils::String TypeString() const override { return TYPE_STRING; }
		[[nodiscard]]
		virtual CrossEntropyLossFunction * CloneRawPointer() const override;

		virtual double CalculateLoss(const std::vector<double>& actual, const std::vector<double>& expected) const override;
		virtual std::vector<double> Derivatives(const std::vector<double>& actual, const std::vector<double>& expected) const override;


	protected:
		virtual double CalculateSingleLoss(const double actual, const double expected) const override;
		virtual double Derivative(const double actual, const double expected) const override;
	};
}
