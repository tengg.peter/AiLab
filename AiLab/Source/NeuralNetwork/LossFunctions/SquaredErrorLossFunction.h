#pragma once
#include "AiLab/Source/NeuralNetwork/LossFunctions/ALossFunction.h"

namespace ai_lab
{
	class SquaredErrorLossFunction : public ALossFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("SquaredErrorLossFunction");
		virtual utils::String TypeString() const override { return TYPE_STRING; }
		[[nodiscard]]
		virtual SquaredErrorLossFunction* CloneRawPointer() const override { return new SquaredErrorLossFunction(*this); }
		virtual ~SquaredErrorLossFunction() = default;

	protected:
		// Inherited via ILossFunction
		virtual double CalculateSingleLoss(const double actual, const double expected) const override
		{
			return 0.5 * pow(actual - expected, 2);
		}
		virtual double Derivative(const double actual, const double expected) const override
		{
			return actual - expected;
		}

	};
}