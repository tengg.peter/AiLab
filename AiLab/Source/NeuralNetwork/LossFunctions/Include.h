#pragma once
#include "AiLab/Source/NeuralNetwork/LossFunctions/AbsoluteErrorLossFunction.h"
#include "AiLab/Source/NeuralNetwork/LossFunctions/ALossFunction.h"
#include "AiLab/Source/NeuralNetwork/LossFunctions/CrossEntropyLossFunction.h"
#include "AiLab/Source/NeuralNetwork/LossFunctions/LogarithmicErrorLossFunction.h"
#include "AiLab/Source/NeuralNetwork/LossFunctions/SquaredErrorLossFunction.h"