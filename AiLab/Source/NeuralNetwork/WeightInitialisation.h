#pragma once
#include "Utils/Utils.h"

#include <iostream>

namespace ai_lab
{
	class WeightInitialisation : public utils::enums::StringEnum
	{
	public:
		virtual ~WeightInitialisation() = default;

	public:
		static const WeightInitialisation Xavier;

		static WeightInitialisation FromString(const utils::String& str) { return WeightInitialisation(str); }

	protected:
		explicit WeightInitialisation(const utils::String& enumValue) : StringEnum(enumValue){}
		// explicit WeightInitialisation(const std::string& enumValue) 
		// 	: StringEnum(utils::string_conversion::StdStringToUtilsString(enumValue)){}
	};
}