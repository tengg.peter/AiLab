#include "ClassificationCounter.h"

namespace ai_lab
{
	void ClassificationCounter::Count(
		const std::vector<double>& expectedResponse,
		const std::vector<double>& response,
		int& numCorrect, 
		int& totalNum)
	{
		bool correctResponse = true;
		for (int i = 0; i < expectedResponse.size() && correctResponse; i++)
		{
			correctResponse = 
				(expectedResponse[i] == 1 && response[i] >= 0.5) || 
				(expectedResponse[i] == 0 && response[i] < 0.5);
		}
		if(correctResponse)
		{
			numCorrect++;
		}
		totalNum++;
	}
}
