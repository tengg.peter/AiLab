#pragma once
#include <vector>

namespace ai_lab
{
	class ICorrectAnswerCounter
	{
	public:
		virtual ~ICorrectAnswerCounter() = default;

	public:
		virtual void Count(
			const std::vector<double>& expectedResponse, 
			const std::vector<double>& response,
			int& numCorrect, 
			int& totalNum) = 0;
	};
}