#pragma once
#include "ICorrectAnswerCounter.h"

namespace ai_lab
{
	class ClassificationCounter : public ICorrectAnswerCounter
	{
	public:
		virtual ~ClassificationCounter() = default;

	public:
		void Count(
			const std::vector<double>& expectedResponse,
			const std::vector<double>& response,
			int& numCorrect, 
			int& totalNum);
	};
}