#pragma once

#include "ATester.h"

namespace ai_lab
{
	class ClassifierTester : public ATester
	{
	public:
		virtual ~ClassifierTester() = default;

	protected:
		void CalculateGoodResponses(
			const std::vector<double>& expectedResponse,
			const std::vector<double>& actualResponse);
	};
}
