#pragma once
//own
#include "AiLab/Source/NeuralNetwork/Training/TrainingSample.h"
//std
#include <cmath>
#include <vector>

namespace ai_lab
{
	class ATester
	{
	public:
		virtual ~ATester() = default;

	public:
		double Ratio() const { return m_ratio; }
		double MeanError() const { return m_meanError; }

		//static double CalculateMeanError(NeuralNetwork& nn, const std::vector<SpTrainingSample>& sample);

		template<typename TNetwork, typename TContainer>
		static double CalculateMeanError(TNetwork& nn, const TContainer& sample)
		{
			if (sample.empty())
			{
				return -1;
			}

			double totalError = 0;
			std::vector<double> response;
			for (const auto& trainingPoint : sample)
			{
				response = nn.FeedForward(trainingPoint->Input());
				for (int i = 0; i < trainingPoint->ExpectedOutput().size(); i++)
				{
					totalError += std::abs(trainingPoint->ExpectedOutput()[i] - response[i]);
				}
			}
			return totalError / sample.front()->ExpectedOutput().size() / sample.size();
		}

	protected:
		int m_numCorrectResponses = 0;
		double m_ratio = 0;
		double m_meanError = 0;
		virtual void CalculateGoodResponses(const std::vector<double>& m_expectedResponse, const std::vector<double>& actualResponse) = 0;
	};
}