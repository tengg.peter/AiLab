//own
#include "AiLab/Source/Helper.h"
#include "Tester.h"
#include "Utils/Utils.h"
//std
#include <iostream>

namespace ai_lab
{
	using std::endl;
	using std::map;
	using std::vector;
	using std::wcout;
	using utils::String;

	void CollectPredictionErrorDistribution(
		const Mlp & mlp, 
		const std::vector<const TrainingSample*>& trainingData,
		const std::vector<const TrainingSample*>& testData,
		const utils::String & outputFilePath)
	{
		map<int, map<int, int>> trainingDistributionByPrediction;
		map<int, map<int, int>> trainingDistributionByActualResult;
		map<int, int> trainingErrorDistribution;

		map<int, map<int, int>> testDistributionByPrediction;
		map<int, map<int, int>> testDistributionByActualResult;
		map<int, int> testErrorDistribution;

		map<int, map<int, int>>* pDistributionByPrediction = &testDistributionByPrediction;
		map<int, map<int, int>>* pDistributionByActualResult = &testDistributionByActualResult;
		map<int, int>* pErrorDistribution = &testErrorDistribution;

		const vector<const TrainingSample*>* pData = &testData;

		utils::OfStream outputFile;
		String fileName =
			utils::string_operations::InsertInfoToOutputFileName(
				outputFilePath,
				utils::DateTime::Now().ToDateTimeString());

		fileName =
			utils::string_operations::InsertInfoToOutputFileName(
				fileName,
				STR("thread ") + utils::ToString(utils::threading::MyThreadNumber()));


		outputFile.open(fileName, std::ios::trunc);

		const String testResultsHeader
		{ STR("\n=================================================== Test results ===================================================\n") };
		const String trainingResultsHeader
		{ STR("\n=================================================== Training results ===================================================\n") };
		const String* pHeader = &testResultsHeader;

		TCOUT << "Writing error distribution to file " << "'" << fileName << "'..." << endl;

		for (size_t i = 0; i < 2; i++)
		{
			outputFile << *pHeader << endl;
			//outputFile << "predicted: actuaSTR(" << endl;

			for (const auto& sample : *pData)
			{
				vector<double> prediction = mlp.FeedForward(sample->Input());
				//outputFile << prediction[0] << ": " << sample->ExpectedOutput()[0] << endl;
				int pred = lround(prediction[0]);
				int act = lround(sample->ExpectedOutput()[0]);

				(*pDistributionByPrediction)[pred][act]++;
				(*pDistributionByActualResult)[act][pred]++;
				(*pErrorDistribution)[pred - act]++;
			}

			outputFile << "\nError => Times:" << endl;
			for (const auto&[error, times] : *pErrorDistribution)
			{
				outputFile << error << ": " << times
					<< " (" << utils::string_conversion::RatioToPercent(static_cast<double>(times) / pData->size()) << ")" << endl;
			}

			outputFile << "\nPredicted => Actual:" << endl;
			for (const auto&[pred, actDist] : *pDistributionByPrediction)
			{
				outputFile << "\n\tPredicted: " << pred << ":" << endl;
				for (const auto&[act, times] : actDist)
				{
					outputFile << "\t\tActual: " << act << ": " << times << endl;
				}
			}

			outputFile << "\nActual => Predicted:" << endl;
			for (const auto&[act, predDist] : *pDistributionByActualResult)
			{
				outputFile << "\n\tActual: " << act << ":" << endl;
				for (const auto&[pred, times] : predDist)
				{
					outputFile << "\t\tPredicted: " << pred << ": " << times << endl;
				}
			}
			outputFile << endl;

			pDistributionByPrediction = &trainingDistributionByPrediction;
			pDistributionByActualResult = &trainingDistributionByActualResult;
			pErrorDistribution = &trainingErrorDistribution;
			pData = &trainingData;
			pHeader = &trainingResultsHeader;
		}

		outputFile.close();
	}

	//temporary implementation for Mlp with one output (card number)
	void CollectPredictionErrorDistribution(
		const Mlp & mlp,
		const std::vector<SpTrainingSample>& availableData,
		const double testSamplingFrequency,
		const String& outputFilePath)
	{
		vector<const TrainingSample*> trainingData;
		vector<const TrainingSample*> testData;
		vector<const TrainingSample*>* pData = &testData;
		impl::MakeTrainingAndTestData(availableData, trainingData, testData, testSamplingFrequency);

		CollectPredictionErrorDistribution(mlp, trainingData, testData, outputFilePath);
	}
}
