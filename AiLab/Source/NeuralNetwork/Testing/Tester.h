#pragma once
//own
#include "AiLab/Source/NeuralNetwork/Mlp.h"
#include "AiLab/Source/NeuralNetwork/Training/TrainingSample.h"
//std
#include <cmath>
#include <map>
#include <vector>


namespace ai_lab
{
	void CollectPredictionErrorDistribution(
		const Mlp& mlp, 
		const std::vector<const TrainingSample*>& trainingData,
		const std::vector<const TrainingSample*>& testData,
		const utils::String& outputFilePath);

	void CollectPredictionErrorDistribution(
		const Mlp& mlp,
		const std::vector<SpTrainingSample>& availableData,
		const double testSamplingFrequency,
		const utils::String& outputFilePath);
}