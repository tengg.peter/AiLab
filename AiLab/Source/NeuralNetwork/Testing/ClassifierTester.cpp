#include "ClassifierTester.h"

namespace ai_lab
{
	void ClassifierTester::CalculateGoodResponses(const std::vector<double>& expectedResponse, const std::vector<double>& actualResponse)
	{
		bool correctResponse = true;
		for (int i = 0; i < expectedResponse.size() && correctResponse; i++)
		{
			correctResponse = 
				(expectedResponse[i] == 1 && actualResponse[i] >= 0.5) || 
				(expectedResponse[i] == 0 && actualResponse[i] < 0.5);
		}

		if(correctResponse)
		{
			m_numCorrectResponses++;
		}
	}
}
