#pragma once
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.h"

namespace ai_lab
{
	class LeakyReluActivationFunction : public AActivationFunction
	{
		static const inline double LEAK_SLOPE = 0.01;

	public:
		static const inline utils::String TYPE_STRING = STR("LeakyReluActivationFunction");

		explicit LeakyReluActivationFunction(double scale = 1, double offset = 0) : AActivationFunction(scale, offset) {}
		virtual ~LeakyReluActivationFunction() = default;

	public:
		// Inherited via AActivationFunction
		virtual double operator()(double x) const override;
		virtual utils::String TypeString() const override { return TYPE_STRING; }
		virtual SpAActivationFunction CloneSp() const override;
		[[nodiscard]]
		virtual LeakyReluActivationFunction* CloneRawPointer() const override;

	protected:
		virtual double Derivative(double x) const override;
	};
}