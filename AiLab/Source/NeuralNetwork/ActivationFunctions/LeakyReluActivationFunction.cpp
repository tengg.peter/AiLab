#include "LeakyReluActivationFunction.h"

namespace ai_lab
{
	double LeakyReluActivationFunction::operator()(double x) const
	{
		m_lastInput = x;
		if (0 <= x)
		{
			return x;
		}
		return LEAK_SLOPE * x;
	}

	double LeakyReluActivationFunction::Derivative(double x) const
	{
		if (0 <= x)
		{
			return 1;
		}
		return LEAK_SLOPE;
	}

	SpAActivationFunction LeakyReluActivationFunction::CloneSp() const
	{
		return SpAActivationFunction(CloneRawPointer());
	}
	LeakyReluActivationFunction* LeakyReluActivationFunction::CloneRawPointer() const
	{
		{ return new LeakyReluActivationFunction(*this); }
	}
}
