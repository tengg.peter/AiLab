#pragma once
//own
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.h"
//std
#include <math.h>

namespace ai_lab
{
	class ArcTanActivationFunction : public AActivationFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("ArcTanActivationFunction");

		explicit ArcTanActivationFunction(double scale = 1, double offset = 0) : AActivationFunction(scale, offset) {}
		virtual ~ArcTanActivationFunction() = default;

	public:
		virtual double operator()(double x) const override
		{
			m_lastInput = x;
			return m_scale * atan(x) + m_offset;
		}
		utils::String TypeString() const override { return TYPE_STRING; }
		// Inherited via AActivationFunction
		virtual SpAActivationFunction CloneSp() const override { return SpAActivationFunction(CloneRawPointer()); }
		[[nodiscard]]
		virtual ArcTanActivationFunction* CloneRawPointer() const override { return new ArcTanActivationFunction(*this); }

	private:
		virtual double Derivative(double x) const override { return m_scale * (1 / (1 + x * x)); }
		ArcTanActivationFunction(const ArcTanActivationFunction& other) : AActivationFunction(other) {}
	};
}
