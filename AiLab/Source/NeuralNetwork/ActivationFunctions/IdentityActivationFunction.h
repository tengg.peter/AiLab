#pragma once

#include "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.h"

namespace ai_lab
{
	class IdentityActivationFunction : public AActivationFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("IdentityActivationFunction");

		explicit IdentityActivationFunction(double scale = 1, double offset = 0) : AActivationFunction(scale, offset) {}
		virtual ~IdentityActivationFunction() = default;
	
	public:
		double operator ()(double x) const 
		{ 
			m_lastInput = x;
			return x; 
		}
		utils::String TypeString() const override { return TYPE_STRING; }
		virtual SpAActivationFunction CloneSp() const override
		{
			return SpAActivationFunction(CloneRawPointer());
		}
		[[nodiscard]]
		virtual IdentityActivationFunction* CloneRawPointer() const override 
		{ 
			return new IdentityActivationFunction(*this); 
		}

		protected:
		double Derivative(double) const { return 1; }
	};
}