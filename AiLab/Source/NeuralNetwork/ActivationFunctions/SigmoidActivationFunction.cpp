#include "SigmoidActivationFunction.h"

namespace ai_lab
{
	double SigmoidActivationFunction::operator()(double x) const
	{
		m_lastInput = x;
		return m_scale * Sigmoid(x) + m_offset;
	}

	SpAActivationFunction SigmoidActivationFunction::CloneSp() const
	{
		return SpAActivationFunction(CloneRawPointer());
	}

	SigmoidActivationFunction * SigmoidActivationFunction::CloneRawPointer() const
	{
		return new SigmoidActivationFunction(*this);
	}

	double SigmoidActivationFunction::Sigmoid(double x) const
	{
		if(x < -45)
		{
			return 0;
		}
		if(x > 45)
		{
			return 1;
		}

		return 1 / (1 + exp(-x));
	}
}
