#pragma once
#include "Utils/Utils.h"

#include <memory>

namespace ai_lab
{
	class AActivationFunction;

	using SpAActivationFunction = std::shared_ptr<AActivationFunction>;

	class AActivationFunction
	{
	public:
		[[nodiscard]]
		static AActivationFunction* S_CloneRawPointer(const AActivationFunction& instance);
		[[nodiscard]]
		static AActivationFunction* CreateNewRawPointer(const utils::String& typeString);

		explicit AActivationFunction(double scale = 1, double offset = 0) : m_scale(scale), m_offset(offset) {}
		explicit AActivationFunction(const AActivationFunction& other);
		virtual ~AActivationFunction() = default;

		virtual double operator()(double x) const = 0;
		virtual utils::String TypeString() const = 0;
		virtual SpAActivationFunction CloneSp() const = 0;
		[[nodiscard]]
		virtual AActivationFunction* CloneRawPointer() const = 0;

		virtual double CalculateOutput(double input) const { return operator()(input); }
		virtual double Derivative() const { return Derivative(m_lastInput); }
		virtual double Activation() const { return operator()(m_lastInput); }
		virtual bool Equals(const AActivationFunction& other) const;

		double Scale() const { return m_scale; }
		virtual void Scale(double value) { m_scale = value; }

		double Offset() const { return m_offset; }
		void Offset(float value) { m_offset = value; }

	protected:
		virtual double Derivative(double x) const = 0;

		double m_scale;
		double m_offset;
		mutable double m_lastInput;
	};

	bool operator==(const AActivationFunction& a, const AActivationFunction& b);
}