#pragma once
//own
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.h"
//std
#include <limits>

namespace ai_lab
{
	class LinearScalingActivationFunction : public AActivationFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("LinearScalingActivationFunction");

		explicit LinearScalingActivationFunction() = default;
		virtual ~LinearScalingActivationFunction() = default;

	public:
		// Inherited via AActivationFunction
		virtual double operator()(double x) const override;
		virtual utils::String TypeString() const override { return TYPE_STRING; }
		virtual SpAActivationFunction CloneSp() const override;
		[[nodiscard]]
		virtual LinearScalingActivationFunction* CloneRawPointer() const override;
		virtual bool Equals(const AActivationFunction& other) const override;

		double MinValue() const { return m_minValue; }
		void MinValue(double minValue);

		double MaxValue() const { return m_maxValue; }
		void MaxValue(double maxValue);
	
	protected:
		virtual double Derivative(double x) const override { return 0; }

	private:
		double m_minValue = (std::numeric_limits<double>::max)();	//extra parenthesis: to prevent min max macro expansion
		double m_maxValue = -(std::numeric_limits<double>::max)();
		double m_range = 0;
		double m_midRange = 0;

		LinearScalingActivationFunction(const LinearScalingActivationFunction& other);

		void UpdateRangeAndMidrange();
	};
}
