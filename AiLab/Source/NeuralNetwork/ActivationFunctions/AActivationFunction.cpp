//own
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/Include.h"
#include "Utils/Utils.h"

namespace ai_lab
{
	AActivationFunction * AActivationFunction::S_CloneRawPointer(const AActivationFunction& instance)
	{
		return instance.CloneRawPointer();
	}

	AActivationFunction * AActivationFunction::CreateNewRawPointer(const utils::String& typeString)
	{
		if (ArcTanActivationFunction::TYPE_STRING == typeString)
		{
			return new ArcTanActivationFunction();
		}
		if (ExponentialActivationFunction::TYPE_STRING == typeString)
		{
			return new ExponentialActivationFunction(1, 0);
		}
		if (IdentityActivationFunction::TYPE_STRING == typeString)
		{
			return new IdentityActivationFunction();
		}
		if (LeakyReluActivationFunction::TYPE_STRING == typeString)
		{
			return new LeakyReluActivationFunction();
		}
		if (LinearScalingActivationFunction::TYPE_STRING == typeString)
		{
			return new LinearScalingActivationFunction();
		}
		if (SigmoidActivationFunction::TYPE_STRING == typeString)
		{
			return new SigmoidActivationFunction();
		}
		if (TanhActivationFunction::TYPE_STRING == typeString)
		{
			return new TanhActivationFunction();
		}
		throw std::logic_error("Unknown type.");
	}

	AActivationFunction::AActivationFunction(const AActivationFunction & other) :
		m_scale(other.m_scale), 
		m_offset(other.m_offset),
		m_lastInput(other.m_lastInput)
	{}

	bool AActivationFunction::Equals(const AActivationFunction & other) const
	{
		return TypeString() == other.TypeString() &&
			m_scale == other.m_scale &&
			m_offset == other.m_offset;
	}

	bool operator==(const AActivationFunction & a, const AActivationFunction & b)
	{
		bool ret = &a == &b ? true :
			nullptr == &a ? false :
			nullptr == &b ? false :
			a.Equals(b);

		if (!ret)
		{
			ret = false;
		}

		return ret;
	}
}
