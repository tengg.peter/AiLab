//own
#include "ExponentialActivationFunction.h"
//std
#include <math.h>

namespace ai_lab
{
	double ExponentialActivationFunction::operator()(double x) const
	{
		m_lastInput = x;
		if(x > 45)
		{
			return pow(2, 45);
		}
		return pow(2, x);
	}

	double ExponentialActivationFunction::Derivative(double x) const
	{
		if(x > 45)
		{
			return pow(2, 45) * log(2);    //natural logarithm
		}
		return pow(2, x) * log(2);
	}

	SpAActivationFunction ExponentialActivationFunction::CloneSp() const
	{
		return SpAActivationFunction(CloneRawPointer());
	}
	ExponentialActivationFunction * ExponentialActivationFunction::CloneRawPointer() const
	{
		return new ExponentialActivationFunction(*this);
	}
}
