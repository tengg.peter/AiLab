#pragma once
//own
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.h"
//std
#include <math.h>

namespace ai_lab
{
	class TanhActivationFunction : public AActivationFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("TanhActivationFunction");

		explicit TanhActivationFunction(double scale = 1, double offset = 0) : AActivationFunction(scale, offset) {}
		virtual ~TanhActivationFunction() = default;

	public:
		double operator()(double x) const 
		{ 
			m_lastInput = x;
			return tanh(x) * m_scale + m_offset; 
		}
		utils::String TypeString() const override { return TYPE_STRING; }
		virtual SpAActivationFunction CloneSp() const override
		{
			return SpAActivationFunction(CloneRawPointer());
		}
		[[nodiscard]]
		virtual TanhActivationFunction* CloneRawPointer() const override 
		{ 
			return new TanhActivationFunction(*this); 
		}

	protected:
		double Derivative(double x) const { return m_scale * (1 - tanh(x) * tanh(x)); }
	};
}
