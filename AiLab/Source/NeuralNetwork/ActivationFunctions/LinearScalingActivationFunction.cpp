#include "AiLab/Source/NeuralNetwork/ActivationFunctions/LinearScalingActivationFunction.h"

namespace ai_lab
{
	LinearScalingActivationFunction::LinearScalingActivationFunction(const LinearScalingActivationFunction & other) :
		AActivationFunction(other),
		m_minValue(other.m_minValue),
		m_maxValue(other.m_maxValue),
		m_range(other.m_range),
		m_midRange(other.m_midRange)
	{	}

	double LinearScalingActivationFunction::operator()(double x) const
	{
		m_lastInput = x;
		if (0 == m_range)
		{
			return 0.0;
		}
		return (x - m_midRange) / (m_range / 2);
	}

	SpAActivationFunction LinearScalingActivationFunction::CloneSp() const
	{
		return SpAActivationFunction(CloneRawPointer());
	}

	LinearScalingActivationFunction * LinearScalingActivationFunction::CloneRawPointer() const
	{
		return new LinearScalingActivationFunction(*this);
	}

	bool LinearScalingActivationFunction::Equals(const AActivationFunction & other) const
	{
		if (AActivationFunction::Equals(other))
		{
			auto otherLinear = dynamic_cast<const LinearScalingActivationFunction&>(other);
			return m_minValue == otherLinear.m_minValue &&
				m_maxValue == otherLinear.m_maxValue;
		}
		return false;
	}

	void LinearScalingActivationFunction::MinValue(double minValue)
	{
		m_minValue = minValue;
		UpdateRangeAndMidrange();
	}

	void LinearScalingActivationFunction::MaxValue(double maxValue)
	{
		m_maxValue = maxValue;
		UpdateRangeAndMidrange();
	}

	void LinearScalingActivationFunction::UpdateRangeAndMidrange()
	{
		m_range = m_maxValue - m_minValue;
		m_midRange = (m_maxValue + m_minValue) / 2;
	}
}