#pragma once
//own
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.h"
//std
#include <math.h>

namespace ai_lab
{
	class SigmoidActivationFunction : public AActivationFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("SigmoidActivationFunction");

		explicit SigmoidActivationFunction(double scale = 1, double offset = 0) : AActivationFunction(scale, offset) {}
		virtual ~SigmoidActivationFunction() = default;
	
	public:
		double operator()(double x) const;
		utils::String TypeString() const override { return TYPE_STRING; }
		virtual SpAActivationFunction CloneSp() const override;
		[[nodiscard]]
		virtual SigmoidActivationFunction* CloneRawPointer() const override;
	
	protected:
		double Derivative(double x) const { return m_scale * Sigmoid(x) * (1 - Sigmoid(x)); }

	private:
		double m_a;   //a is not used yet, because I couldnt figure out the derivative
		double Sigmoid(double x) const;
	};
}
