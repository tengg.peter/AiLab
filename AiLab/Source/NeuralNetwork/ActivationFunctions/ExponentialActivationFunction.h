#pragma once

#include "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.h"

namespace ai_lab
{
	class ExponentialActivationFunction : public AActivationFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("ExponentialActivationFunction");

		//scaling is not needed for this function
		explicit ExponentialActivationFunction(double, double offset = 0) : AActivationFunction(1, offset) {}
		virtual ~ExponentialActivationFunction() = default;

	public:
		double operator()(double x) const;
		utils::String TypeString() const override { return TYPE_STRING; }
		void Scale(double) {}

		// Inherited via AActivationFunction
		virtual SpAActivationFunction CloneSp() const override;
		[[nodiscard]]
		virtual ExponentialActivationFunction* CloneRawPointer() const override;

	protected:
		double Derivative(double x) const;
	};
}