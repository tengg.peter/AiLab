#pragma once
//own
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/Include.h"
#include "AiLab/Source/NeuralNetwork/NeuralNetType.h"
#include "AiLab/Source/NeuralNetwork/OutputFunctions/Include.h"
#include "AiLab/Source/NeuralNetwork/Training/BackPropagation/BackPropParams.h"
#include "AiLab/Source/NeuralNetwork/Training/TrainingSample.h"
#include "AiLab/Source/NeuralNetwork/WeightInitialisation.h"
//std
#include <chrono>
#include <iosfwd>
#include <limits>
#include <optional>
#include <type_traits>
#include <vector>

namespace ai_lab
{
	using NoFunction = struct NoFunctionType {};

	template<typename TActivationFunction, typename TOutputFunction = NoFunction>
	struct LayerDescription
	{
		static_assert(std::is_base_of<AActivationFunction, TActivationFunction>::value, "TActivationFunction must inherit from AActivationFunction");
		static_assert(std::is_same<TOutputFunction, NoFunction>::value || 
			std::is_base_of<AOutputFunction, TOutputFunction>::value, "TOutputFunction must inherit from AOutputFunction");

		using ActivationFunctionType = TActivationFunction;
		using OutputFunctionType = TOutputFunction;
		const size_t numNeurons;

		LayerDescription(size_t numNeurons) : numNeurons(numNeurons) { }
	};

	class Mlp
	{
		using DcpActivationFunction = utils::DeepCopyPointer<AActivationFunction>;
		using DcpOutputFunction = utils::DeepCopyPointer<AOutputFunction>;

	public:
		static Mlp FromJson(const utils::String jsonString);

		//back prop training metadata
		std::optional<BackPropParams> backPropParamsOptional;
		double trainingError = (std::numeric_limits<double>::max)();
		double generalisationError = (std::numeric_limits<double>::max)();
		double trainingLoss = (std::numeric_limits<double>::max)();
		double generalisationLoss = (std::numeric_limits<double>::max)();
		int numEpochs = 0;
		int numTrainingSamples = 0;
		int numTestSamples = 0;
		int trainingType = 0;
		utils::String trainingTimeString;
		std::map<utils::String, utils::String> notes;

		explicit Mlp() = default;
		Mlp(const Mlp&) = default;	//cannot be explicit. It wouldn't copy on returning from functions
		virtual ~Mlp() = default;
		Mlp& operator=(const Mlp&) = default;

		template<typename TFirstLayerDescription, typename... TLayerDescriptions>
		Mlp(TFirstLayerDescription firstLayer, TLayerDescriptions... otherLayers) : Mlp(otherLayers...)
		{
			m_activationFunctions.emplace(m_activationFunctions.cbegin());
			for (size_t n = 0; n < firstLayer.numNeurons; n++)
			{
				m_activationFunctions.front().push_back(
					DcpActivationFunction(
						new typename TFirstLayerDescription::ActivationFunctionType(),
						AActivationFunction::S_CloneRawPointer));
			}

			if (m_activationFunctions.size() > 1)
			{
				m_weights.emplace(m_weights.cbegin());
				m_weights.front().resize((m_activationFunctions.cbegin() + 1)->size());
				for (std::vector<double>& w : m_weights.front())
				{
					w.resize(firstLayer.numNeurons);
				}

				m_biases.emplace(m_biases.cbegin());
				m_biases.front().resize((m_activationFunctions.cbegin() + 1)->size());
			}
		}

		size_t OutputLayerSize() const { return m_activationFunctions.back().size(); }
		void SetupInputLayerScaling(const std::vector<SpTrainingSample>& trainingData);
		void InitRandomWeights(WeightInitialisation weightInitialisation);
		bool WeightsInitialised();
		size_t LayerSize(size_t layerIdx) const { return m_activationFunctions[layerIdx + 1].size(); }	//+1, because the input layer is not a real layer
		//Number of layers excluding the input layer.
		size_t NumLayers() const { return m_weights.size(); }
		size_t NumWeights(size_t layerIdx, size_t neuronIdx) const { return m_weights[layerIdx][neuronIdx].size(); }
		double CurrentActivation(size_t layerIdx, size_t neuronIdx) const;
		double DerivativeOfCurrentActivation(size_t layerIdx, size_t neuronIdx) const;
		double Weight(size_t layerIdx, size_t neuronIdx, size_t weightIdx) const;
		void AdjustWeight(size_t layerIdx, size_t neuronIdx, size_t weightIdx, double delta);
		void AdjustBias(size_t layerIdx, size_t neuronIdx, double delta);
		NeuralNetType Type() const;
		void AddLayer(const utils::String& activationFunctionType, const int numNeurons);
		void AddLayer(const utils::String& activationFunctionType, const utils::String& outputFunctionType, const int numNeurons);

		std::vector<double> FeedForward(const std::vector<double>& input) const;
		utils::String ToJson() const;

		friend bool operator==(const Mlp& a, const Mlp& b);
	private:
		static const inline std::string ACTIVATION_FUNCTIONS_JSON_NAME = "activationFunctions";
		static const inline std::string BACK_PROP_PARAMS_JSON_NAME = "backPropParams";
		static const inline std::string BIASES_JSON_NAME = "biases";
		static const inline std::string FUNCTION_TYPE_JSON_NAME = "functionType";
		static const inline std::string GENERALISATION_ERROR_JSON_NAME = "2_generalisationError";
		static const inline std::string GENERALISATION_LOSS_JSON_NAME = "4_generalisationLoss";
		static const inline std::string LEARNING_RATE_JSON_NAME = "learningRate";
		static const inline std::string LOSS_FUNCTION_JSON_NAME = "lossFunction";
		static const inline std::string MAX_VALUE_JSON_NAME = "maxValue";
		static const inline std::string METADATA_JSON_NAME = "1_metadata";
		static const inline std::string MIN_VALUE_JSON_NAME = "minValue";
		static const inline std::string MINI_BATCH_SIZE_JSON_NAME = "miniBatchSize";
		static const inline std::string MOMENTUM_JSON_NAME = "momentum";
		static const inline std::string NOTES_JSON_NAME = "notes";
		static const inline std::string NUM_EPOCHS_JSON_NAME = "numEpochs";
		static const inline std::string NUM_TEST_SAMPLES_JSON_NAME = "numTestSamples";
		static const inline std::string NUM_TRAINING_SAMPLES_JSON_NAME = "numTrainingSamples";
		static const inline std::string OFFSET_JSON_NAME = "offset";
		static const inline std::string OUTPUT_FUNCTION_JSON_NAME = "outputFunction";
		static const inline std::string SCALE_JSON_NAME = "scale";
		static const inline std::string STRUCTURE_JSON_NAME = "5_structure";
		static const inline std::string TRAINING_ERROR_JSON_NAME = "1_trainingError";
		static const inline std::string TRAINING_LOSS_JSON_NAME = "3_trainingLoss";
		static const inline std::string TRAINING_TIME_JSON_NAME = "trainingTime";
		static const inline std::string TRAINING_TYPE_JSON_NAME = "trainingType";
		static const inline std::string WEIGHT_INIT_JSON_NAME = "weightInit";
		static const inline std::string WEIGHTS_JSON_NAME = "weights";

		//The 3 dimensions are: layers, neurons, weights
		std::vector<std::vector<DcpActivationFunction>> m_activationFunctions;
		std::vector<std::vector<std::vector<double>>> m_weights;
		std::vector<std::vector<double>> m_biases;
		DcpOutputFunction m_dcpOutputFunction{ AOutputFunction::S_CloneRawPointer };

		template<typename TOutputLayerDescription>
		Mlp(TOutputLayerDescription outputLayer)
		{
			m_activationFunctions.emplace(m_activationFunctions.cbegin());
			for (size_t n = 0; n < outputLayer.numNeurons; n++)
			{
				m_activationFunctions.front().push_back(
					DcpActivationFunction(
						new typename TOutputLayerDescription::ActivationFunctionType(),
						AActivationFunction::S_CloneRawPointer));
			}

			if constexpr (false == std::is_same<typename TOutputLayerDescription::OutputFunctionType, NoFunction>::value)
			{
				m_dcpOutputFunction.Reset(new typename TOutputLayerDescription::OutputFunctionType());
			}
		}
	};

	bool operator==(const Mlp& a, const Mlp& b);
	utils::OStream& operator<<(utils::OStream& os, const Mlp& mlp);
}