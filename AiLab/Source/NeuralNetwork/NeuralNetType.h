#pragma once

namespace ai_lab
{
	enum class NeuralNetType
	{
		Regressor,
		Classifier
	};
}