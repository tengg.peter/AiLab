#include "AiLab/Source/NeuralNetwork/OutputFunctions/Include.h"

namespace ai_lab
{
	AOutputFunction* AOutputFunction::CreateNewRawPointer(const utils::String & typeString)
	{
		if (SoftmaxOutputFunction::TYPE_STRING == typeString)
		{
			return new SoftmaxOutputFunction();
		}
		throw std::logic_error("Unknown type.");
	}
}
