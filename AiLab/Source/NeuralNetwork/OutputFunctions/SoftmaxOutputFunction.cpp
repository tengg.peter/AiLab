//own
#include "AiLab/Source/NeuralNetwork/OutputFunctions/SoftmaxOutputFunction.h"
//std
#include <cmath>
#include <numeric>

namespace ai_lab
{
	std::vector<double> SoftmaxOutputFunction::CalculateOutput(const std::vector<double>& activation) const
	{
		m_denominator = 0;
		m_lastInput = activation;

		std::vector<double> output;
		for (const double a : activation)
		{
			double e = exp(a);
			output.push_back(e);
			m_denominator += e;
		}

		for (size_t i = 0; i < output.size(); i++)
		{
			output[i] /= m_denominator;
		}

		return output;
	}

	double SoftmaxOutputFunction::DerivativeOfCurrentOutput(const size_t outputIdx) const
	{
		//source: https://medium.com/@14prakash/back-propagation-is-very-simple-who-made-it-complicated-97b794c97e5c
		//that equation is correct, I double checked it on paper

		double exi = exp(m_lastInput[outputIdx]);
		return (exi * (m_denominator - exi)) / (m_denominator * m_denominator);
	}
}
