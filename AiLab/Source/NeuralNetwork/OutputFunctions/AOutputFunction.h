#pragma once
#include "Utils/Utils.h"

#include <vector>

namespace ai_lab
{
	class AOutputFunction
	{
	public:
		[[nodiscard]]
		static AOutputFunction* S_CloneRawPointer(const AOutputFunction& instance)
		{
			return instance.CloneRawPointer();
		}

		static AOutputFunction* CreateNewRawPointer(const utils::String& typeString);

		virtual ~AOutputFunction() = default;

		virtual std::vector<double> CalculateOutput(const std::vector<double>& activation) const = 0;
		virtual double DerivativeOfCurrentOutput(const size_t outputIdx) const = 0;
		virtual utils::String TypeString() const = 0;
		virtual AOutputFunction* CloneRawPointer() const = 0;

		virtual bool Equals(const AOutputFunction& other) const { return TypeString() == other.TypeString(); }
	};

	inline bool operator==(const AOutputFunction& a, const AOutputFunction& b)
	{
		return &a == &b ? true :
			nullptr == &a ? false :
			nullptr == &b ? false :
			a.Equals(b);
	}
}