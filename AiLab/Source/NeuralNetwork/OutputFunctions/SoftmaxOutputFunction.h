#pragma once
#include "AiLab/Source/NeuralNetwork/OutputFunctions/AOutputFunction.h"

namespace ai_lab
{
	class SoftmaxOutputFunction : public AOutputFunction
	{
	public:
		static const inline utils::String TYPE_STRING = STR("SoftmaxOutputFunction");

		virtual ~SoftmaxOutputFunction() = default;

	public:
		// Inherited via AOutputFunction
		virtual std::vector<double> CalculateOutput(const std::vector<double>& activation) const override;
		virtual double DerivativeOfCurrentOutput(const size_t outputIdx) const override;
		virtual utils::String TypeString() const { return TYPE_STRING; }
		virtual SoftmaxOutputFunction* CloneRawPointer() const override { return new SoftmaxOutputFunction(*this); };

	private:
		mutable std::vector<double> m_lastInput;
		mutable double m_denominator;
	};
}