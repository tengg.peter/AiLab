//own
#include "TrainingUtils.h"
#include "Utils/Utils.h"
//std
#include <cmath>

namespace ai_lab
{
	double ClassificationErrorFunction(double expectedResponse, double actualResponse)
	{
		double error = expectedResponse - actualResponse;
		double sign = static_cast<double>(utils::math::Sgn(error));
		double absError = std::abs(error);

		return absError > 0.5 ? sign : 0;
	}
}