#pragma once
namespace ai_lab
{
	struct BackPropStopCond
	{
		virtual ~BackPropStopCond() = default;

		int minNumIterations = 100;
		int maxNumIterations = 1000;
		double generalisationError = 1;
		/*If not 0, thenstops the training if there has not been any improvement for a while. E.g: when set to 1, stops after
		* the last improvement the same number of epochs as there were before the improvement. E.g: if the last improvement
		* took place after epoch 54, and in the following 54 epochs there is no improvement, the training stops. If this value 
		* was set to 0.1, then it would stop 5.4 (that is 6) epochs after the last improvement.
		*/
		double stopIfNotImprovedEpochRatio = 0;
	};

	inline bool operator==(const BackPropStopCond& a, const BackPropStopCond& b)
	{
		return &a == &b ||
			(a.minNumIterations == b.minNumIterations &&
			a.maxNumIterations == b.maxNumIterations &&
			a.generalisationError == b.generalisationError &&
			a.stopIfNotImprovedEpochRatio == b.stopIfNotImprovedEpochRatio);
	}
}