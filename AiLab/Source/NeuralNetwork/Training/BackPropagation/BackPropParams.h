#pragma once
#include "AiLab/Source/NeuralNetwork/Training/BackPropagation/BackPropStopCond.h"
#include "AiLab/Source/NeuralNetwork/WeightInitialisation.h"
#include "AiLab/Source/NeuralNetwork/LossFunctions/Include.h"
#include "Utils/Utils.h"
//std
#include <functional>

namespace ai_lab
{
	//using DcpLossFunction = utils::DeepCopyPointer<ALossFunction, decltype(&(ALossFunction::S_CloneRawPointer))>;
	using DcpLossFunction = utils::DeepCopyPointer<ALossFunction>;

	struct BackPropParams
	{
		virtual ~BackPropParams() = default;

		double learningRate = 0.00001;
		double momentum = 0.9;
		double testSamplingFrequency = 0.2;
		//Set to 0 to take the whole training sample as a batch
		size_t miniBatchSize = 1;
		bool setUpInputLayerScaling = false;
		WeightInitialisation weightInitialisation = WeightInitialisation::Xavier;
		DcpLossFunction dcpLossfunction{ new SquaredErrorLossFunction(), SquaredErrorLossFunction::S_CloneRawPointer };

		BackPropStopCond stopConditions;
		utils::String neuralNetJsonFilePath = STR("Mlp.json");
		utils::String trainingErrorsFilePath = STR("Training Errors.csv");
		utils::String errorDistributionFilePath = STR("Error Distribution.txt");
	};

	inline bool operator==(const BackPropParams& a, const BackPropParams& b)
	{
		return (&a == &b) ||
				(a.learningRate == b.learningRate &&
				a.momentum == b.momentum &&
				a.testSamplingFrequency == b.testSamplingFrequency &&
				a.miniBatchSize == b.miniBatchSize &&
				a.setUpInputLayerScaling == b.setUpInputLayerScaling &&
				a.weightInitialisation == b.weightInitialisation &&
				a.dcpLossfunction == b.dcpLossfunction &&
				a.stopConditions == b.stopConditions &&
				a.neuralNetJsonFilePath == b.neuralNetJsonFilePath &&
				a.trainingErrorsFilePath == b.trainingErrorsFilePath) &&
				a.errorDistributionFilePath == b.errorDistributionFilePath;
	}

	inline bool operator!=(const BackPropParams& a, const BackPropParams& b)
	{
		return !(a == b);
	}
}