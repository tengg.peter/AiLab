#pragma once
//own
#include "AiLab/Source/NeuralNetwork/Mlp.h"
#include "AiLab/Source/NeuralNetwork/Training/BackPropagation/BackPropParams.h"
#include "AiLab/Source/NeuralNetwork/Training/BackPropagation/TrainingTask.h"
//std
#include <vector>

namespace ai_lab::back_propagation
{
	Mlp Train(
		const Mlp& mlp,
		const std::vector<SpTrainingSample>& availableData,
		const BackPropParams params);

	Mlp Train(TrainingTask& task);

	void TrainParallel(std::vector<TrainingTask>& tasks, const int maxThreads);
}