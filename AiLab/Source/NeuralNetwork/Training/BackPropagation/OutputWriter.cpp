
#include "AiLab/Source/NeuralNetwork/Training/BackPropagation/OutputWriter.h"
#include "AiLab/Source/NeuralNetwork/Mlp.h"

namespace ai_lab::impl
{
	void OutputWriter::WriteOutput(const Mlp & mlp)
	{
		if(m_first)
		{
			m_neuralNetFileName = utils::string_operations::InsertInfoToOutputFileName(
				mlp.backPropParamsOptional->neuralNetJsonFilePath, utils::DateTime::Now().ToDateTimeString());
			m_neuralNetFileName = utils::string_operations::InsertInfoToOutputFileName(
				m_neuralNetFileName, STR("thread ") + utils::ToString(utils::threading::MyThreadNumber()));

			m_epochErrorsFileName = utils::string_operations::InsertInfoToOutputFileName(
				mlp.backPropParamsOptional->trainingErrorsFilePath, utils::DateTime::Now().ToDateTimeString());
			m_epochErrorsFileName = utils::string_operations::InsertInfoToOutputFileName(
				m_epochErrorsFileName, STR("thread ") + utils::ToString(utils::threading::MyThreadNumber()));
		}

		//write neural net
		bool improvedNet = false;
		if (mlp.generalisationLoss != -1 && m_minLoss > mlp.generalisationLoss)
		{
			m_minLoss = mlp.generalisationLoss;
			improvedNet = true;
		}
		else if (mlp.generalisationLoss == -1 && m_minLoss > mlp.trainingLoss)
		{
			m_minLoss = mlp.trainingLoss;
			improvedNet = true;
		}
		if (improvedNet)
		{
			utils::OfStream jsonFile;
			jsonFile.open(m_neuralNetFileName, std::ios::trunc);
			jsonFile << mlp;
			jsonFile.close();
		}

		//write errors
		utils::CsvFileWriter csvFileWriter(m_epochErrorsFileName, true);
		if (m_first)
		{
			csvFileWriter.WriteLine(
				STR("epoch"),
				STR("time stamp"),
				STR("training error"),
				STR("generalisation error"),
				STR("training loss"),
				STR("generalisation loss"));
			m_first = false;
		}
		csvFileWriter.WriteLine(
			mlp.numEpochs,
			utils::DateTime::Now().ToDateTimeString(),
			mlp.trainingError,
			mlp.generalisationError,
			mlp.trainingLoss,
			mlp.generalisationLoss);
	}
}
