#pragma once

#include "AiLab/Source/NeuralNetwork/Mlp.h"

namespace ai_lab::back_propagation
{
	struct TrainingTask
	{
		virtual ~TrainingTask() = default;

		Mlp mlp;
		const std::vector<SpTrainingSample> availableData;
		BackPropParams params;
		int times = 1;
	};
}