//own
#include "AiLab/Source/Helper.h"
#include "AiLab/Source/NeuralNetwork/Testing/ATester.h"
#include "AiLab/Source/NeuralNetwork/Testing/Tester.h"
#include "AiLab/Source/NeuralNetwork/Training/BackPropagation/OutputWriter.h"
#include "AiLab/Source/NeuralNetwork/Training/BackPropagation/BackPropagation.h"
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/IdentityActivationFunction.h"
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/LinearScalingActivationFunction.h"
#include "Utils/Utils.h"
//std
#include <cmath>
#include <future>
#include <limits>
#include <memory>
#include <mutex>
#include <numeric>
#ifdef __linux__
#include <thread>
#endif

namespace ai_lab::back_propagation
{
	using std::vector;
	using utils::String;

	static void SetSizeGradients(vector<vector<double>>& hiddenGradients, const Mlp& mlp)
	{
		hiddenGradients.resize(mlp.NumLayers());
		for (int l = 0; l < mlp.NumLayers(); l++)
		{
			hiddenGradients[l].resize(mlp.LayerSize(l));
		}
	}

	static void SetSizeWeightDeltas(vector<vector<vector<double>>>& weightDeltas, const Mlp& mlp)
	{
		weightDeltas.resize(mlp.NumLayers());
		for (size_t l = 0; l < mlp.NumLayers(); l++)
		{
			weightDeltas[l].resize(mlp.LayerSize(l));
			for (size_t n = 0; n < mlp.LayerSize(l); n++)
			{
				weightDeltas[l][n].resize(mlp.NumWeights(l, n));
			}
		}
	}

	static void SetSizeBiasDeltas(vector<vector<double>>& biasDeltas, const Mlp& mlp)
	{
		biasDeltas.resize(mlp.NumLayers());
		for (int l = 0; l < mlp.NumLayers(); l++)
		{
			biasDeltas[l].resize(mlp.LayerSize(l));
		}
	}

	static void Reset1DVector(vector<double>& v1d)
	{
		std::fill(v1d.begin(), v1d.end(), 0);
	}

	static void Reset2DVector(vector<vector<double>>& v2d)
	{
		for (auto& v1d : v2d)
		{
			Reset1DVector(v1d);
		}
	}

	static void Reset3DVector(vector<vector<vector<double>>>& v3d)
	{
		for (auto& v2d : v3d)
		{
			Reset2DVector(v2d);
		}
	}

	template<typename TContainer, typename TFuncPtr>
	static double CalculateAverageError(const Mlp& mlp, const TContainer& samples, const TFuncPtr& errorFn)
	{
		if (samples.empty())
		{
			return -1;
		}

		double totalLoss = 0;
		for (const auto& sample : samples)
		{
			vector<double> actualOutput = mlp.FeedForward(sample->Input());
			totalLoss += errorFn->CalculateLoss(actualOutput, sample->ExpectedOutput());
		}
		return totalLoss / samples.size();
	}

	Mlp Train(const Mlp& mlp, const std::vector<SpTrainingSample>& availableData, const BackPropParams params)
	{
		Mlp tempMlp(mlp);

		//checks if the samples are correct
		if (availableData.empty())
		{
			throw std::invalid_argument("No data.");
		}
		if (availableData.front()->ExpectedOutput().size() != tempMlp.OutputLayerSize() &&
			NeuralNetType::Classifier != tempMlp.Type())
		{
			throw std::logic_error("Only classifiers can have a different size of output");
		}

		utils::threading::AcquireNewThreadNumber();

		//prepares the training and test sets
		vector<const TrainingSample*> trainingData;
		vector<const TrainingSample*> testData;
		impl::MakeTrainingAndTestData(availableData, trainingData, testData, params.testSamplingFrequency);
		utils::RandomService::GetInstance().Shuffle(trainingData);

		if (params.setUpInputLayerScaling)
		{
			tempMlp.SetupInputLayerScaling(availableData);
		}
		tempMlp.InitRandomWeights(params.weightInitialisation);

		//containers for the gradients
		vector<vector<double>> gradients;
		SetSizeGradients(gradients, tempMlp);

		vector<vector<vector<double>>> weightDeltas1;
		SetSizeWeightDeltas(weightDeltas1, tempMlp);
		vector<vector<vector<double>>> weightDeltas2(weightDeltas1); //creates the same structure by the copy constructor
		vector<vector<vector<double>>>* pWeightDeltas = &weightDeltas1;
		vector<vector<vector<double>>>* pPrevWeightDeltas = &weightDeltas2;

		vector<vector<double>> biasDeltas1;
		SetSizeBiasDeltas(biasDeltas1, tempMlp);
		vector<vector<double>> biasDeltas2(biasDeltas1);
		vector<vector<double>>* pBiasDeltas = &biasDeltas1;
		vector<vector<double>>* pPrevBiasDeltas = &biasDeltas2;

		tempMlp.backPropParamsOptional = params;
		tempMlp.numTrainingSamples = static_cast<int>(trainingData.size());
		tempMlp.numTestSamples = static_cast<int>(testData.size());
		tempMlp.trainingType = 1;
		Mlp bestGeneralisingMlp(tempMlp);

		int numEpochs = 0;
		utils::date_and_time::StopWatch timer;

		auto it = trainingData.cbegin();

		size_t miniBatchSize;
		if (0 == params.miniBatchSize || trainingData.size() < params.miniBatchSize)
		{
			miniBatchSize = trainingData.size();
		}
		else
		{
			miniBatchSize = params.miniBatchSize;
		}

		impl::OutputWriter outputWriter;
		static std::mutex s_coutMutex;

		std::unique_ptr<ALossFunction> errorFunction;
		if (NeuralNetType::Regressor == tempMlp.Type())
		{
			errorFunction.reset(new AbsoluteErrorLossFunction());
		}
		else if (NeuralNetType::Classifier == tempMlp.Type())
		{
			errorFunction.reset(new CrossEntropyLossFunction());
		}

		timer.Start();
		while (true)
		{
			if (trainingData.cbegin() == it)
			{
				double trainingError = CalculateAverageError(tempMlp, trainingData, errorFunction);
				double trainingLoss = CalculateAverageError(tempMlp, trainingData, params.dcpLossfunction);
				double generalisationError = -1;
				double generalisationLoss = -1;
				if (!testData.empty())
				{
					generalisationError = CalculateAverageError(tempMlp, testData, errorFunction);
					generalisationLoss = CalculateAverageError(tempMlp, testData, params.dcpLossfunction);
				}

				tempMlp.trainingError = trainingError;
				tempMlp.generalisationError = generalisationError;
				tempMlp.trainingLoss = trainingLoss;
				tempMlp.generalisationLoss = generalisationLoss;
				tempMlp.numEpochs = numEpochs;
				tempMlp.trainingTimeString = utils::date_and_time::SecsToTime(timer.Elapsed<std::chrono::seconds>());

				if ((!testData.empty() && tempMlp.generalisationLoss < bestGeneralisingMlp.generalisationLoss) ||
					testData.empty() && tempMlp.trainingLoss < bestGeneralisingMlp.trainingLoss)
				{
					bestGeneralisingMlp = tempMlp;
				}

				outputWriter.WriteOutput(tempMlp);

				{
					std::lock_guard coutLock(s_coutMutex);
					TCOUT
						<< "[" << utils::threading::MyThreadNumber() << "] "
						<< numEpochs
						<< ": training error: " << utils::ToString(trainingError)	//to_string is a workaround here. Some double values can crash the cout.
						<< " generalisation error: " << utils::ToString(generalisationError)
						<< " training loss: " << utils::ToString(trainingLoss)
						<< " generalisation loss: " << utils::ToString(generalisationLoss)
						<< std::endl;
				}

				bool stop =
					numEpochs >= params.stopConditions.minNumIterations &&
					(numEpochs >= params.stopConditions.maxNumIterations ||
						bestGeneralisingMlp.numEpochs * (1 + params.stopConditions.stopIfNotImprovedEpochRatio)
						<= numEpochs ||
						(!testData.empty() && generalisationError < params.stopConditions.generalisationError));


				if (stop)
				{
					CollectPredictionErrorDistribution(
						tempMlp,
						trainingData,
						testData,
						params.errorDistributionFilePath);
					return bestGeneralisingMlp;
				}
			}

			Reset3DVector(*pWeightDeltas);
			Reset2DVector(*pBiasDeltas);

			for (size_t sampleCount = 0; sampleCount < miniBatchSize && it != trainingData.cend(); sampleCount++, it++)	//mini batch loop
			{
				const TrainingSample* pTrainingSample = *it;

				vector<double> response = tempMlp.FeedForward(pTrainingSample->Input());
				vector<double> lossGradients = params.dcpLossfunction->Derivatives(response, pTrainingSample->ExpectedOutput());

				//calculates gradients for output neurons
				for (int n = 0; n < lossGradients.size(); n++)
				{
					const size_t outputLayerIdx = tempMlp.NumLayers() - 1;
					gradients[outputLayerIdx][n] = lossGradients[n] * tempMlp.DerivativeOfCurrentActivation(outputLayerIdx, n);
				}

				//calculates gradients for hidden neurons
				for (size_t l = gradients.size() - 2; l < gradients.size(); l--)	//size_t will underflow, therefore the strange l < gradients.size() stop condition
				{
					for (size_t currentLayerNeuron = 0; currentLayerNeuron < gradients[l].size(); currentLayerNeuron++)
					{
						double weightedSumOutputGradients = 0;
						for (size_t nextLayerNeuron = 0; nextLayerNeuron < gradients[l + 1].size(); nextLayerNeuron++)
						{
							weightedSumOutputGradients = gradients[l + 1][nextLayerNeuron] * tempMlp.Weight(l + 1, nextLayerNeuron, currentLayerNeuron);
						}
						gradients[l][currentLayerNeuron] = tempMlp.DerivativeOfCurrentActivation(l, currentLayerNeuron) * weightedSumOutputGradients;
					}
				}

				//gradients have been calculated, now calculate deltas for weights
				//hidden layers:
				//for each layer
				for (size_t l = 0; l < pWeightDeltas->size(); l++)
				{
					//for each neuron
					for (size_t n = 0; n < (*pWeightDeltas)[l].size(); n++)
					{
						//for each input weight
						for (size_t w = 0; w < (*pWeightDeltas)[l][n].size(); w++)
						{
							//this will work, even if l == 0, because layer -1 is the input layer
							(*pWeightDeltas)[l][n][w] -= params.learningRate * gradients[l][n] * tempMlp.CurrentActivation(l - 1, w);
						}
					}
				}

				//calculates deltas for hidden biases
				for (int l = 0; l < pBiasDeltas->size(); l++)
				{
					for (int n = 0; n < (*pBiasDeltas)[l].size(); n++)
					{
						(*pBiasDeltas)[l][n] -= params.learningRate * gradients[l][n];
					}
				}
			}	//mini batch loop

			//updates weights by the deltas. uses previous deltas with momentum to adjust the value.
			for (int l = 0; l < tempMlp.NumLayers(); l++)
			{
				for (int n = 0; n < tempMlp.LayerSize(l); n++)
				{
					for (int w = 0; w < tempMlp.NumWeights(l, n); w++)
					{
						tempMlp.AdjustWeight(l, n, w, (*pWeightDeltas)[l][n][w] + params.momentum * (*pPrevWeightDeltas)[l][n][w]);
					}
				}
			}

			//updates biases by the deltas. uses previous deltas with momentum to adjust the value.
			for (int l = 0; l < tempMlp.NumLayers(); l++)
			{
				for (int n = 0; n < tempMlp.LayerSize(l); n++)
				{
					tempMlp.AdjustBias(l, n, (*pBiasDeltas)[l][n] + params.momentum * (*pPrevBiasDeltas)[l][n]);
				}
			}

			//swaps the previous and current deltas. In the next cycle these deltas will be used by the momentum.
			swap(pWeightDeltas, pPrevWeightDeltas);
			swap(pBiasDeltas, pPrevBiasDeltas);

			if (it == trainingData.cend())
			{
				utils::RandomService::GetInstance().Shuffle(trainingData);
				it = trainingData.cbegin();
				++numEpochs;
			}
		}
	}

	Mlp Train(const TrainingTask& task)
	{
		return Train(task.mlp, task.availableData, task.params);
	}

	void TrainParallel(std::vector<TrainingTask>& tasks, const int maxThreads)
	{
		using namespace std::chrono_literals;

		vector<TrainingTask> expandedTasks;
		for (size_t i = 0; i < tasks.size(); i++)
		{
			for (size_t j = 0; j < tasks[i].times; j++)
			{
				expandedTasks.push_back(tasks[i]);
				expandedTasks.back().times = static_cast<int>(i);	//little hack here: reuses the times variable to store the index of the original task
			}
		}


		int numTasksDone = 0;
		int iNextTask = 0;
		std::map<int, std::future<Mlp>> runningTasks;

		while (numTasksDone < expandedTasks.size())
		{
			//starting new tasks
			while (runningTasks.size() < maxThreads && iNextTask < expandedTasks.size())
			{
				const auto& nextTask = expandedTasks[iNextTask];
				runningTasks.insert(std::make_pair(
					iNextTask,
					std::async(
						std::launch::async,
						static_cast<Mlp(*)(const TrainingTask&)>(Train),
						nextTask)
				));

				++iNextTask;
			}

			std::this_thread::sleep_for(5s);

			//removes finished tasks
			for (auto it = runningTasks.begin(); it != runningTasks.end(); )
			{
				if (std::future_status::ready == it->second.wait_for(0ms))
				{
					const int originalTaskIdx = expandedTasks[it->first].times;
					const Mlp& readyMlp = it->second.get();
					//the hack continues here: m_times stores the index of the original task. It also indexes the return vector.
					if (readyMlp.generalisationLoss < tasks[originalTaskIdx].mlp.generalisationLoss)
					{
						tasks[originalTaskIdx].mlp = readyMlp;
					}
					it = runningTasks.erase(it);
					++numTasksDone;
				}
				else
				{
					++it;
				}
			}
		}
	}
}