#pragma once
//own
#include "Utils/Utils.h"
//std
#include <limits>

namespace ai_lab { class Mlp; }

namespace ai_lab::impl
{
	class OutputWriter
	{
	public:
		virtual ~OutputWriter() = default;

	public:
		void WriteOutput(const Mlp& mlp);

	private:
		bool m_first = true;
		double m_minLoss = std::numeric_limits<double>::max();
		utils::String m_neuralNetFileName;
		utils::String m_epochErrorsFileName;
		bool m_improvedNet = false;
	};
}
