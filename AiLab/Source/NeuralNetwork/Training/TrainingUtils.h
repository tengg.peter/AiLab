#pragma once

#include <vector>

namespace ai_lab
{
	float ClassificationErrorFunction(float expectedResponse, float actualResponse);
}
