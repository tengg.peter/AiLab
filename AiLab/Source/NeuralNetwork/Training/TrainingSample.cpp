#include "TrainingSample.h"

namespace ai_lab
{
	TrainingSample::TrainingSample(const std::vector<double>& input, const std::vector<double>& expectedOutput)
		: m_input(input), m_expectedOutput(expectedOutput)
	{
	}
	TrainingSample::TrainingSample(std::vector<double>&& input, std::vector<double>&& expectedOutput)
		: TrainingSample(input, expectedOutput)
	{
	}
}
