#pragma once
#include <memory>
#include <vector>

namespace ai_lab
{
	class TrainingSample
	{
	public:
		virtual ~TrainingSample() = default;

	public:
		TrainingSample(const std::vector<double>& input, const std::vector<double>& expectedOutput);
		TrainingSample(std::vector<double>&& input, std::vector<double>&& expectedOutput);
		

		virtual const std::vector<double>& Input() const { return m_input; }
		virtual const std::vector<double>& ExpectedOutput() const { return m_expectedOutput; };
	
	protected:
		std::vector<double> m_input;
		std::vector<double> m_expectedOutput;
	};

	using SpTrainingSample = std::shared_ptr<TrainingSample>;
}