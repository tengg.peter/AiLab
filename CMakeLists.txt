# AiLab
cmake_minimum_required(VERSION 3.10)

set(PROJECT_NAME "AiLab")

project (${PROJECT_NAME} C CXX)

if(MSVC)
	set(CMAKE_CXX_STANDARD 23)	#for FMT
else()
	set(CMAKE_CXX_STANDARD 17)
endif()
set(CMAKE_CXX_STANDARD_REQUIRED True)


#find . -name "*.cpp"
set(SRC_FILES
    "AiLab/Source/Helper.cpp"
    "AiLab/Source/UniformSpaceSearch/Counter.cpp"
    "AiLab/Source/NeuralNetwork/WeightInitialisation.cpp"
    "AiLab/Source/NeuralNetwork/LossFunctions/ALossFunction.cpp"
    "AiLab/Source/NeuralNetwork/LossFunctions/CrossEntropyLossFunction.cpp"
    "AiLab/Source/NeuralNetwork/Mlp.cpp"
    "AiLab/Source/NeuralNetwork/OutputFunctions/SoftmaxOutputFunction.cpp"
    "AiLab/Source/NeuralNetwork/OutputFunctions/AOutputFunction.cpp"
    "AiLab/Source/NeuralNetwork/ActivationFunctions/LinearScalingActivationFunction.cpp"
    "AiLab/Source/NeuralNetwork/ActivationFunctions/LeakyReluActivationFunction.cpp"
    "AiLab/Source/NeuralNetwork/ActivationFunctions/AActivationFunction.cpp"
    "AiLab/Source/NeuralNetwork/ActivationFunctions/ExponentialActivationFunction.cpp"
    "AiLab/Source/NeuralNetwork/ActivationFunctions/SigmoidActivationFunction.cpp"
    "AiLab/Source/NeuralNetwork/Training/TrainingUtils.cpp"
    "AiLab/Source/NeuralNetwork/Training/TrainingSample.cpp"
    "AiLab/Source/NeuralNetwork/Training/BackPropagation/OutputWriter.cpp"
    "AiLab/Source/NeuralNetwork/Training/BackPropagation/BackPropagation.cpp"
    "AiLab/Source/NeuralNetwork/Testing/Tester.cpp"
    "AiLab/Source/NeuralNetwork/Testing/AnswerCounters/ClassificationCounter.cpp"
    "AiLab/Source/NeuralNetwork/Testing/ClassifierTester.cpp"
	)

    #find . -type d
include_directories(
    "."
    "AiLab/Source"
    "AiLab/Source/GA"
    "AiLab/Source/NeuralNetwork"
    "AiLab/Source/NeuralNetwork/LossFunctions"
    "AiLab/Source/NeuralNetwork/OutputFunctions"
    "AiLab/Source/NeuralNetwork/ActivationFunctions"
    "AiLab/Source/NeuralNetwork/Training"
    "AiLab/Source/NeuralNetwork/Training/BackPropagation"
    "AiLab/Source/NeuralNetwork/Testing"
    "AiLab/Source/NeuralNetwork/Testing/AnswerCounters"
    ".."
    "../Utils"
	)

add_subdirectory("Tests/")

#message(STATUS "SRC_FILES: ${SRC_FILES}")
if(MSVC)
	# set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")	#this should work, but it doesn't on Win10, cmake 3.16.3
    #message(STATUS "${PROJECT_NAME}: Default CMAKE_CXX_FLAGS_DEBUG: ${CMAKE_CXX_FLAGS_DEBUG}")
    if(NOT ${CMAKE_CXX_FLAGS_DEBUG} STREQUAL "")
        string(REPLACE "/MDd" "/MTd" CMAKE_CXX_FLAGS_DEBUG ${CMAKE_CXX_FLAGS_DEBUG})
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /permissive-")
    endif()
    #message(STATUS "${PROJECT_NAME}: Modified CMAKE_CXX_FLAGS_DEBUG: ${CMAKE_CXX_FLAGS_DEBUG}")
    
    #message(STATUS "${PROJECT_NAME}: Default CMAKE_CXX_FLAGS_RELEASE: ${CMAKE_CXX_FLAGS_RELEASE}")
    if(NOT ${CMAKE_CXX_FLAGS_RELEASE} STREQUAL "")
        string(REPLACE "/MD" "/MT" CMAKE_CXX_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE})
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /permissive-")
    endif()
	#message(STATUS "${PROJECT_NAME}: Modified CMAKE_CXX_FLAGS_RELEASE: ${CMAKE_CXX_FLAGS_RELEASE}")
endif()

if (NOT TARGET AiLab)
    add_library(${PROJECT_NAME} STATIC ${SRC_FILES})
endif()