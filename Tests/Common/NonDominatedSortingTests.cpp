//own
#include "../AiLab/Source/Common/NonDominatedSorting.h"
#include "../AiLab/Source/UniformSpaceSearch/SolutionCandidate.h"

//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std
#include <algorithm>

using namespace ai_lab;

TEST(NonDominatedSortingTests, Ctr)
{
	std::vector<SolutionCandidate<int>> candidates(10);
	candidates[0].scores = { 11, 16 };
	candidates[1].scores = { 8, 15 };
	candidates[2].scores = { 8, 17 };
	candidates[3].scores = { 1, 17 };
	candidates[4].scores = { 5, 17 };
	candidates[5].scores = { 1, 20 };
	candidates[6].scores = { 10, 19 };
	candidates[7].scores = { 10, 18 };
	candidates[8].scores = { 4, 6 };
	candidates[9].scores = { 11, 18 };
	const std::vector<Objective> objectives{ Objective::Maximise, Objective::Minimise };

	const auto ranks = NonDominatedSorting(candidates, objectives);
	EXPECT_EQ(5, ranks.size());
	EXPECT_EQ(3, ranks[0].size());
	EXPECT_EQ(2, ranks[1].size());
	EXPECT_EQ(2, ranks[2].size());
	EXPECT_EQ(2, ranks[3].size());
	EXPECT_EQ(1, ranks[4].size());
	for (const auto& rank : ranks)
	{
		for (const auto& cand : rank)
		{
			TCOUT << "(" << cand->Scores()[0] << ", " << cand->Scores()[1] << ") ";
		}
		TCOUT << std::endl;
	}
}