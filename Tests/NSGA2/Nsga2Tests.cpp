//own
#include "../AiLab/Source/Common/ObjectiveEnum.h"
#include "../AiLab/Source/NSGA2/Nsga2.h"

//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std
#include <numeric>
#include <thread>

using namespace ai_lab;
using std::endl;
class Nsga2TestFixture : public ::testing::Test
{
public:
	class TestFitness : public ANsga2Fitness<double>
	{
	public:
		explicit TestFitness() : ANsga2Fitness({ Objective::Maximise, Objective::Minimise }, { STR("Obj1"), STR("Obj2") })
		{
		}
		virtual ~TestFitness() = default;

		virtual void Calculate(Nsga2Individual<double>& individual) override
		{
			individual.fitnessValues.clear();
			individual.fitnessValues.emplace_back(individual.chromosome[0]);
			individual.fitnessValues.emplace_back(individual.chromosome[1]);
		}

		virtual bool ValidateGenes(const std::vector<double>& chrom) const override
		{
			return chrom[0] > chrom[1];
		}
	};

public:
	Nsga2TestFixture()
		: m_fitness()
		, m_nsga2(10, 2, 100, m_fitness, false /*printGenerations*/)
	{}

protected:
	static void SetUpTestSuite()
	{
		utils::RandomService::GetInstance().Seed(4434);
	}

protected:
	TestFitness m_fitness;
	Nsga2<double> m_nsga2;
};

TEST_F(Nsga2TestFixture, Ctor)
{
	EXPECT_EQ(10, m_nsga2.m_population.size());
	EXPECT_EQ(2, m_nsga2.m_population.front().chromosome.size());
	for (const auto& ind : m_nsga2.m_population)
	{
		EXPECT_EQ(1, m_nsga2.m_chromSet.count(ind.chromosome));	//every chromosome is in the population exactly once
		EXPECT_GT(ind.chromosome[0], ind.chromosome[1]);
	}
}

TEST(Nsga2Test, Ctor)
{
	constexpr size_t popSize = 30;

	Nsga2TestFixture::TestFitness fitness;
	Nsga2 nsga2(popSize, { {1, 1001}, { 1, 1001 } }, 10, fitness, false /*printGenerations*/);

	EXPECT_EQ(popSize, nsga2.m_population.size());
	for (const auto& ind : nsga2.m_population)
	{
		EXPECT_EQ(1, nsga2.m_chromSet.count(ind.chromosome));	//every chromosome is in the population exactly once
		EXPECT_EQ(2, ind.chromosome.size());
		EXPECT_LE(1, ind.chromosome[0]);
		EXPECT_GT(1001, ind.chromosome[0]);
		EXPECT_LE(1, ind.chromosome[1]);
		EXPECT_GT(1001, ind.chromosome[1]);
	}
}

TEST_F(Nsga2TestFixture, SortByObjective)
{
	EXPECT_NO_THROW(m_nsga2.CalculateFitness(m_nsga2.m_population));

	auto fronts = m_nsga2.NonDominatedSorting();
	for (size_t obj = 0; obj < 2; obj++)
	{
		for (auto& front : fronts)
		{
			EXPECT_NO_THROW(m_nsga2.SortFrontByObjective(front, obj));
			for (size_t i = 0; i < front.size() - 1; i++)
			{
				EXPECT_LE(front[i]->fitnessValues[obj], front[i + 1]->fitnessValues[obj]);
			}
		}
	}
}

TEST_F(Nsga2TestFixture, CrowdingDistance)
{
	EXPECT_NO_THROW(m_nsga2.CalculateFitness(m_nsga2.m_population));
	auto fronts = m_nsga2.NonDominatedSorting();
	EXPECT_NO_THROW(m_nsga2.CrowdingDistance(fronts));
	const size_t expectedInfValues = 2 * std::count_if(fronts.cbegin(), fronts.cend(), [](const auto& front) {return 1 < front.size(); });
	size_t infCount = 0;
	for (size_t i = 0; i < m_nsga2.m_population.size(); i++)
	{
		if (std::numeric_limits<double>::infinity() == m_nsga2.m_population[i].crowdingDistance)
		{
			++infCount;
		}
	}
	EXPECT_EQ(expectedInfValues, infCount);
}

template<typename Func>
void IterateFronts(std::vector<std::vector<Nsga2Individual<double>*>> ranks, Func func)
{
	for (const auto& rank : ranks)
	{
		for (const auto p : rank)
		{
			func(p);
		}
	}
}

TEST_F(Nsga2TestFixture, Dominates)
{
	Nsga2Individual<double> ind1;
	Nsga2Individual<double> ind2;

	//maximise, minimise
	ind1.fitnessValues = { 1.0, 0.0 };
	ind2.fitnessValues = { 1.0, 0.4 };
	EXPECT_TRUE(m_nsga2.Dominates(ind1, ind2));
	EXPECT_FALSE(m_nsga2.Dominates(ind2, ind1));

	ind1.fitnessValues = { 1.1, 0.4 };
	ind2.fitnessValues = { 1.0, 0.4 };
	EXPECT_TRUE(m_nsga2.Dominates(ind1, ind2));
	EXPECT_FALSE(m_nsga2.Dominates(ind2, ind1));

	ind1.fitnessValues = { 1.0, 0.4 };
	ind2.fitnessValues = { 0.5, 0.0 };
	EXPECT_FALSE(m_nsga2.Dominates(ind1, ind2));
	EXPECT_FALSE(m_nsga2.Dominates(ind2, ind1));

	ind1.fitnessValues = { 0.5, 0.4 };
	ind2.fitnessValues = { 0.5, 0.4 };
	EXPECT_FALSE(m_nsga2.Dominates(ind1, ind2));
	EXPECT_FALSE(m_nsga2.Dominates(ind2, ind1));
}

void PrintFronts(const std::vector<std::vector<Nsga2Individual<double>*>>& fronts)
{
	for (size_t f = 0; f < fronts.size(); f++)
	{
		TCOUT << "Front " << f << ": ";
		for (const auto pInd : fronts[f])
		{
			TCOUT << "(";
			for (const double fitnessVal : pInd->fitnessValues)
			{
				TCOUT << fitnessVal << ", ";
			}
			TCOUT << "), ";
		}
		TCOUT << endl;
	}
}

TEST_F(Nsga2TestFixture, NonDominatedSorting)
{
	m_nsga2.CalculateFitness(m_nsga2.m_population);
	const std::vector<std::vector<Nsga2Individual<double>*>> fronts = m_nsga2.NonDominatedSorting();
	PrintFronts(fronts);
	size_t numPointers = 0;
	IterateFronts(fronts, [&numPointers](const auto) {++numPointers; });

	EXPECT_EQ(m_nsga2.m_population.size(), numPointers);	//there are as many pointers as popsize
	IterateFronts(fronts, [](const auto p) {EXPECT_NE(nullptr, p); });	//no pointer is NULL
	IterateFronts(fronts, [this](const auto p)	//each pointer points to an individual in the pop
		{
			const auto& pop = m_nsga2.m_population;
			EXPECT_NE(pop.cend(), std::find(pop.cbegin(), pop.cend(), *p));
		});

	for (const auto pInd : fronts.back())
	{
		EXPECT_EQ(0, pInd->dominates.size());	//individuals in the last rank don't dominate anyone
	}

	for (const auto& front : fronts)	//individuals in the same front should not dominate each other
	{
		for (size_t i = 0; i < front.size() - 1; i++)
		{
			EXPECT_FALSE(m_nsga2.Dominates(*front[i], *front[i + 1]));
			EXPECT_FALSE(m_nsga2.Dominates(*front[i + 1], *front[i]));
		}
	}

	for (size_t f = 0; f < fronts.size(); f++)	//ranks equal the front index
	{
		for (const auto pInd : fronts[f])
		{
			EXPECT_EQ(f, pInd->rank);
		}
	}

	for (size_t f = 1; f < fronts.size(); f++)	//every individual in front f must be dominated by at least one individual from front f-1
	{
		for (const auto pWorseInd : fronts[f])
		{
			bool dominantFound = false;
			for (const auto pBetterInd : fronts[f - 1])
			{
				if (m_nsga2.Dominates(*pBetterInd, *pWorseInd))
				{
					dominantFound = true;
					break;
				}
			}
			EXPECT_TRUE(dominantFound);
		}
	}
}

TEST_F(Nsga2TestFixture, SelectParent)
{
	//artificial static fitnesses to test with
	for (size_t i = 0; i < m_nsga2.m_population.size(); i++)
	{
		m_nsga2.m_population[i].fitnessValues.clear();
		m_nsga2.m_population[i].fitnessValues.emplace_back(i);
		m_nsga2.m_population[i].fitnessValues.emplace_back(1.0 / (static_cast<double>(i % 7)));
	}

	auto fronts = m_nsga2.NonDominatedSorting();
	m_nsga2.CrowdingDistance(fronts);

	{
		const Nsga2Individual<double>& parent = m_nsga2.SelectParent(1, 7);	//same rank, crowding distance decides
		EXPECT_EQ(m_nsga2.m_population[7], parent);
	}
	{
		const Nsga2Individual<double>& parent = m_nsga2.SelectParent(7, 1);
		EXPECT_EQ(m_nsga2.m_population[7], parent);
	}
	{
		const Nsga2Individual<double>& parent = m_nsga2.SelectParent(2, 2);
		EXPECT_EQ(m_nsga2.m_population[2], parent);
	}
	{
		const Nsga2Individual<double>& parent = m_nsga2.SelectParent(4, 5);	//different ranks. Rank decides
		EXPECT_EQ(m_nsga2.m_population[5], parent);
	}
	{
		const Nsga2Individual<double>& parent = m_nsga2.SelectParent(5, 4);
		EXPECT_EQ(m_nsga2.m_population[5], parent);
	}
}

TEST_F(Nsga2TestFixture, Crossover)
{
	auto& ind1 = m_nsga2.m_population.front();
	auto& ind2 = m_nsga2.m_population.back();

	const auto origChrom1 = ind1.chromosome;
	const auto origChrom2 = ind2.chromosome;
	std::vector<Individual<double>*> parents{ &ind1, &ind2 };
	m_nsga2.m_crossover->CrossOver(parents);

	EXPECT_NE(origChrom1, ind1.chromosome);
	EXPECT_NE(origChrom2, ind2.chromosome);
	EXPECT_TRUE(origChrom1[0] == ind2.chromosome[0] || origChrom1[1] == ind2.chromosome[1]);	//one gene has been moved to ind2
	EXPECT_TRUE(origChrom2[0] == ind1.chromosome[0] || origChrom2[1] == ind1.chromosome[1]);	//one gene has been moved to ind2
}

TEST_F(Nsga2TestFixture, Mutation)
{
	auto& ind1 = m_nsga2.m_population.front();
	const auto originalChrom = ind1.chromosome;

	for (size_t i = 0; i < 1000; i++)
	{
		m_nsga2.Mutate(ind1);
		for (size_t g = 0; g < ind1.chromosome.size(); g++)
		{
			//TCOUT << ind1.chromosome[0] << ", " << ind1.chromosome[1] << endl;
			EXPECT_LE(ind1.GeneValueRanges()[g].first, ind1.chromosome[g]);
			EXPECT_GE(ind1.GeneValueRanges()[g].second, ind1.chromosome[g]);
		}
	}
	EXPECT_NE(originalChrom, ind1.chromosome);
}

TEST_F(Nsga2TestFixture, CreateOffspringPop)
{
	const auto offspringPop = m_nsga2.CreateOffspringPop();
	EXPECT_EQ(m_nsga2.m_population.size(), offspringPop.size());
	EXPECT_NE(m_nsga2.m_population, offspringPop);

	for (const auto& ind : offspringPop)
	{
		EXPECT_EQ(0, ind.fitnessValues.size());	//offspring should have no calculated fitness
		EXPECT_GT(ind.chromosome[0], ind.chromosome[1]);	//checks the gene constraint
	}

	std::set<Individual<double>> indSet(offspringPop.cbegin(), offspringPop.cend());
	EXPECT_EQ(indSet.size(), offspringPop.size());	//we don't want duplicate individuals
}

TEST_F(Nsga2TestFixture, CreateNewPopulation)
{
	m_nsga2.CalculateFitness(m_nsga2.m_population);
	auto fronts = m_nsga2.NonDominatedSorting();
	EXPECT_TRUE(std::any_of(m_nsga2.m_population.cbegin(), m_nsga2.m_population.cend(), [](const auto& ind) {return ind.rank != 0; }));

	m_nsga2.CrowdingDistance(fronts);
	EXPECT_TRUE(std::any_of(m_nsga2.m_population.cbegin(), m_nsga2.m_population.cend(), [](const auto& ind) {return ind.crowdingDistance != 0; }));
	EXPECT_TRUE(std::any_of(m_nsga2.m_population.cbegin(), m_nsga2.m_population.cend(), [](const auto& ind) {return ind.crowdingDistance == std::numeric_limits<double>::infinity(); }));

	auto offspringPop = m_nsga2.CreateOffspringPop();
	m_nsga2.CalculateFitness(offspringPop);
	EXPECT_TRUE(std::all_of(offspringPop.cbegin(), offspringPop.cend(), [](const auto& ind) {return !ind.fitnessValues.empty(); }));

	m_nsga2.m_population.insert(m_nsga2.m_population.end(), std::make_move_iterator(offspringPop.begin()), std::make_move_iterator(offspringPop.end()));
	fronts = m_nsga2.NonDominatedSorting();
	EXPECT_TRUE(std::all_of(m_nsga2.m_population.cbegin(), m_nsga2.m_population.cend(),
		[](const auto& ind)
		{
			return 0 == ind.dominatedBy && 0 == ind.crowdingDistance;
		}));

	m_nsga2.CrowdingDistance(fronts);
	EXPECT_FALSE(std::all_of(m_nsga2.m_population.cbegin(), m_nsga2.m_population.cend(), [](const auto& ind) { return 0 == ind.crowdingDistance; }));
	EXPECT_TRUE(std::all_of(fronts.back().cbegin(), fronts.back().cend(), [](const auto pInd) { return pInd->dominates.empty(); }));	//individuals in the last rank do not dominate anyone

	std::map<size_t, size_t> rankSizes;
	for (const auto& front : fronts)
	{
		for (const auto pInd : front)
		{
			if (0 == rankSizes.count(pInd->rank))
			{
				rankSizes.insert({ pInd->rank, 1 });
			}
			else
			{
				++rankSizes.at(pInd->rank);
			}
		}
	}

	auto newPop = m_nsga2.CreateNewPopulation(fronts);
	EXPECT_EQ(m_nsga2.m_popSize, newPop.size());

	size_t currentRank = 0;
	size_t rankCount = 0;
	for (const auto& ind : newPop)
	{
		if (ind.rank != currentRank)
		{
			EXPECT_GT(ind.rank, currentRank);
			EXPECT_EQ(rankCount, rankSizes.at(currentRank));	//less or equal, because the last rank may be taken in partly
			currentRank = ind.rank;
			rankCount = 1;
		}
		else
		{
			++rankCount;
		}
	}
	EXPECT_LE(rankCount, rankSizes.at(currentRank));	//less or equal, because the last rank may be taken in partly
}

bool PopAndChromSetInSync(const std::vector<Nsga2Individual<double>>& pop, const std::set<std::vector<double>>& chromSet)
{
	bool pass = true;
	for (const auto& ind : pop)
	{
		EXPECT_EQ(1, chromSet.count(ind.chromosome));	//chromSet should be in sync with the population
		if (1 != chromSet.count(ind.chromosome))
		{
			pass = false;
		}
	}
	return pass;
}

TEST(Nsga2Test, WholeProcess)
{
	/*
		Correct order to calculate:
		- fitness
		- non dominated sorting
		- crowding distance
	*/

	const size_t popSize = 30;
	const int generations = 100;

	Nsga2TestFixture::TestFitness fitness;

	Nsga2 ga(popSize, 2, generations, fitness, false /*printGenerations*/);
	EXPECT_EQ(popSize, ga.m_chromSet.size());
	EXPECT_TRUE(PopAndChromSetInSync(ga.m_population, ga.m_chromSet));
	EXPECT_TRUE(std::all_of(ga.m_population.cbegin(), ga.m_population.cend(), [](const auto& ind) {return ind.fitnessValues.empty(); }));
	ga.CalculateFitness(ga.m_population);
	EXPECT_TRUE(std::all_of(ga.m_population.cbegin(), ga.m_population.cend(), [](const auto& ind) {return 2 == ind.fitnessValues.size(); }));
	auto fronts = ga.NonDominatedSorting();
	ga.CrowdingDistance(fronts);

	for (size_t g = 0; g < generations; g++)
	{
		auto offspringPop = ga.CreateOffspringPop();
		if (0 == g)
		{
			EXPECT_EQ(2 * popSize, ga.m_chromSet.size());
		}
		else
		{
			EXPECT_EQ(3 * popSize, ga.m_chromSet.size());
		}
		EXPECT_TRUE(PopAndChromSetInSync(ga.m_population, ga.m_chromSet));
		ga.CalculateFitness(offspringPop);
		EXPECT_TRUE(std::all_of(offspringPop.cbegin(), offspringPop.cend(), [](const auto& ind) {return 2 == ind.fitnessValues.size(); }));

		//temporarily moves the offspring to the previous population. Now we have double the amount of individuals
		ga.m_chromSet.clear();
		for (size_t i = 0; i < offspringPop.size(); i++)
		{
			ga.m_population.emplace_back(std::move(offspringPop[i]));
		}
		for (const auto& ind : ga.m_population)
		{
			ga.m_chromSet.insert(ind.chromosome);
		}

		EXPECT_EQ(2 * popSize, ga.m_population.size());
		EXPECT_EQ(2 * popSize, ga.m_chromSet.size());
		EXPECT_TRUE(PopAndChromSetInSync(ga.m_population, ga.m_chromSet));

		auto fronts = ga.NonDominatedSorting();
		ga.CrowdingDistance(fronts);

		const auto newPop = ga.CreateNewPopulation(fronts);
		ga.m_population = newPop;
	}
}

TEST(Nsga2Test, Run)
{
	class ChromFitness : public ANsga2Fitness<double>
	{
	public:
		explicit ChromFitness() : ANsga2Fitness({ Objective::Maximise, Objective::Minimise }, { STR("Obj1"), STR("Obj2") })
		{
		}
		virtual ~ChromFitness() = default;

		virtual void Calculate(Nsga2Individual<double>& individual)
		{
			individual.fitnessValues.clear();
			individual.fitnessValues.emplace_back(individual.chromosome[0] + individual.chromosome[1]);
			individual.fitnessValues.emplace_back(1 / (individual.chromosome[0] * individual.chromosome[1]));
		}
	};

	const size_t popSize = 30;
	const int generations = 10;
	constexpr bool printGenerations = false;

	ChromFitness fitness;
	Nsga2 ga(popSize, 2, generations, fitness, printGenerations);
	EXPECT_FALSE(ga.Run().empty());
}

TEST_F(Nsga2TestFixture, EvolveNextGeneration)
{
	m_nsga2.EvolveNextGeneration();
	const auto gen1 = m_nsga2.m_population;
	EXPECT_FALSE(gen1.empty());
	EXPECT_TRUE(std::any_of(m_nsga2.m_population.cbegin(), m_nsga2.m_population.cend(), [](const auto& ind)
		{
			return 0 != ind.rank;
		}));
	EXPECT_TRUE(std::any_of(m_nsga2.m_population.cbegin(), m_nsga2.m_population.cend(), [](const auto& ind)
		{
			return 0 != ind.crowdingDistance;
		}));

	m_nsga2.EvolveNextGeneration();
	const auto gen2 = m_nsga2.m_population;
	EXPECT_NE(gen1, gen2);

	auto addFitness0 = [](double sum, const auto& ind) { return sum + ind.fitnessValues[0]; };
	auto addFitness1 = [](double sum, const auto& ind) { return sum + ind.fitnessValues[1]; };

	const double gen1Fit0 = std::accumulate(gen1.cbegin(), gen1.cend(), 0.0, addFitness0);
	const double gen1Fit1 = std::accumulate(gen1.cbegin(), gen1.cend(), 0.0, addFitness1);
	const double gen2Fit0 = std::accumulate(gen2.cbegin(), gen2.cend(), 0.0, addFitness0);
	const double gen2Fit1 = std::accumulate(gen2.cbegin(), gen2.cend(), 0.0, addFitness1);

	EXPECT_TRUE(gen1Fit0 <= gen2Fit0 || gen1Fit1 >= gen2Fit1);
}

TEST_F(Nsga2TestFixture, GetFirstFront)
{
	m_nsga2.EvolveNextGeneration();
	const auto front = m_nsga2.GetFirstFront();
	EXPECT_TRUE(std::all_of(front.cbegin(), front.cend(), [](const auto& ind) {return 0 == ind.rank; }));
}

TEST_F(Nsga2TestFixture, AverageFitnesses)
{
	m_nsga2.EvolveNextGeneration();
	const auto avgFitnessFirstFront = m_nsga2.AverageFitnesses(1);
	const auto avgFitnessFirstTwoFronts = m_nsga2.AverageFitnesses(2);
	const size_t numObjectives = m_nsga2.m_fitness.Objectives().size();

	ASSERT_EQ(numObjectives, avgFitnessFirstFront.size());
	ASSERT_EQ(numObjectives, avgFitnessFirstTwoFronts.size());

	EXPECT_TRUE((avgFitnessFirstFront[0] >= avgFitnessFirstTwoFronts[0] && avgFitnessFirstFront[1] < avgFitnessFirstTwoFronts[1]) ||
		(avgFitnessFirstFront[0] >= avgFitnessFirstTwoFronts[0] && avgFitnessFirstFront[1] < avgFitnessFirstTwoFronts[1]));
}

TEST_F(Nsga2TestFixture, BestFitnesses)
{
	m_nsga2.EvolveNextGeneration();
	const auto bestFitnesses = m_nsga2.BestFitnesses();
	const size_t numObjectives = m_nsga2.m_fitness.Objectives().size();

	ASSERT_EQ(numObjectives, bestFitnesses.size());
	for (size_t i = 0; i < m_nsga2.m_fitness.Objectives().size(); i++)
	{
		const auto obj = m_nsga2.m_fitness.Objectives()[i];
		for (const auto& ind : m_nsga2.m_population)
		{
			if (Objective::Maximise == obj)
			{
				EXPECT_GE(bestFitnesses[i], ind.fitnessValues[i]);
			}
			else
			{
				EXPECT_LE(bestFitnesses[i], ind.fitnessValues[i]);
			}
		}
	}
}