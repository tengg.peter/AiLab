//own
#include "../AiLab/Source/NSGA2/ANsga2Fitness.h"
#include "../AiLab/Source/NSGA2/Nsga2Individual.h"

//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std

using namespace ai_lab;

TEST(Nsga2IndividualTest, Ctor)
{
	std::vector<Nsga2Individual<double>> pop;
	for (size_t i = 0; i < 30; i++)
	{
		pop.emplace_back(Nsga2Individual<double>({ {1, 1001}, {1, 1001} }));
	}

	for (const auto& ind : pop)
	{
		EXPECT_EQ(2, ind.chromosome.size());
		EXPECT_LE(1, ind.chromosome[0]);
		EXPECT_GT(1001, ind.chromosome[0]);
		EXPECT_LE(1, ind.chromosome[1]);
		EXPECT_GT(1001, ind.chromosome[1]);
	}
}

TEST(Nsga2IndividualTest, ResetRanking)
{
	Nsga2Individual<double> ind;
	ind.crowdingDistance = 0.312;
	ind.dominatedBy = 7;
	ind.dominates = { &ind, nullptr, nullptr, &ind };	//these are all invalid values, but it doesn't metter in this test
	ind.rank = 3;

	ind.ResetRanking();
	EXPECT_EQ(0, ind.crowdingDistance);
	EXPECT_EQ(0, ind.dominatedBy);
	EXPECT_TRUE(ind.dominates.empty());
	EXPECT_EQ(0, ind.rank);
}

TEST(Nsga2IndividualTest, GeneConstraintTest)
{
	class TestFitness : public ANsga2Fitness<double>
	{
	public:
		explicit TestFitness() : ANsga2Fitness({ Objective::Maximise, Objective::Minimise }, { STR("Obj1"), STR("Obj2") })
		{ 
		}
		virtual ~TestFitness() = default;

		virtual void Calculate(Nsga2Individual<double>& individual) override {};

		virtual bool ValidateGenes(const std::vector<double>& chromosome) const override
		{
			return chromosome[0] < 0.5 &&
				chromosome[1] > 0.5 &&
				chromosome[2] >= chromosome[0] + chromosome[1];
		}
	} const fitness;
	
	std::vector<Nsga2Individual<double>> population;
	for (size_t i = 0; i < 10; i++)
	{
		population.emplace_back(3, &fitness);
	}

	for (const auto& ind : population)
	{
		EXPECT_GT(0.5, ind.chromosome[0]);
		EXPECT_LT(0.5, ind.chromosome[1]);
		EXPECT_GE(ind.chromosome[2], ind.chromosome[0] + ind.chromosome[1]);
	}
}

TEST(Nsga2IndividualTest, ImpossibleConstraint)
{
	class TestFitness : public ANsga2Fitness<double>
	{
	public:
		explicit TestFitness() : ANsga2Fitness({ Objective::Maximise, Objective::Minimise }, { STR("Obj1"), STR("Obj2") })
		{ 
		}
		virtual ~TestFitness() = default;

		virtual void Calculate(Nsga2Individual<double>& individual) override {};

		virtual bool ValidateGenes(const std::vector<double>& chromosome) const override
		{
			return 0.5 > chromosome[0] && 0.6 <= chromosome[0];
		}
	} const fitness;

	EXPECT_THROW(Nsga2Individual(1, &fitness), std::logic_error);
}