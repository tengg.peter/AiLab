#include "AiLab/GA.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std

using namespace ai_lab;

class CrossoverStrategyTestFixture : public ::testing::Test
{
public:
    static constexpr size_t NUM_INDIVIDUALS = 5;

public:
    CrossoverStrategyTestFixture()
    {
        m_individuals.resize(NUM_INDIVIDUALS);
        for (auto& ind : m_individuals)
        {
            ind.chromosome = m_chromosome;
        }
    }
    virtual ~CrossoverStrategyTestFixture() = default;

protected:
    const std::vector<double> m_chromosome = { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6 };
    std::vector<Individual<double>> m_individuals;
    NPointCrossOver<double> m_crossover{ 0.23 };
};


TEST_F(CrossoverStrategyTestFixture, AllTheSame_True)
{
    EXPECT_TRUE(m_crossover.AllTheSame(m_individuals));
}

TEST_F(CrossoverStrategyTestFixture, AllTheSame_False)
{
    m_individuals.front().chromosome.front() = 1.0;
    EXPECT_FALSE(m_crossover.AllTheSame(m_individuals));
}
