#include "AiLab/GA.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std
#include <iostream>

using namespace ai_lab;

class NPointCrossOverTestFixture: public ::testing::Test 
{ 
public:
    static constexpr size_t NUM_INDIVIDUALS = 4;

public: 
    explicit NPointCrossOverTestFixture() 
    { 
        m_individuals.resize(NUM_INDIVIDUALS);
        for (size_t i = 0; i < NUM_INDIVIDUALS; i++)
        {
            m_individuals[0].chromosome.resize(10, i);
        }
    } 
    virtual ~NPointCrossOverTestFixture() = default;

protected:
    std::vector<Individual<double>> m_individuals;
};

TEST_F(NPointCrossOverTestFixture, CrossingPointsIs0)
{
    NPointCrossOver<double> crossover(1.0, 0);  //probabilty set to 1
    const auto originalIndividuals = m_individuals;
    ASSERT_NO_THROW(crossover.CrossOver(m_individuals));
    EXPECT_EQ(originalIndividuals, m_individuals);  //should not change anything   
}

TEST_F(NPointCrossOverTestFixture, LessThan2Individuals)
{
    NPointCrossOver<double> crossover(1.0, 0);  //probabilty set to 1
    std::vector<Individual<double>> individuals(1);
    individuals.front().chromosome.resize(10, 0.0);
    
    {
        const auto originalIndividuals = individuals;
        //one individual. Nothing should change
        ASSERT_NO_THROW(crossover.CrossOver(individuals));
        EXPECT_EQ(originalIndividuals, individuals);
    }
    {
        individuals.clear();
        const auto originalIndividuals = individuals;
        //zero individuals. Nothing should change
        ASSERT_NO_THROW(crossover.CrossOver(individuals));
        EXPECT_EQ(originalIndividuals, individuals);
    }
}