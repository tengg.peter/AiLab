#include "AiLab/GA.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std
#include <iostream>

using namespace ai_lab;

TEST(IndividualTests, SetGeneWithinRange)
{
	Individual ind(1);	//default value range is [0.0, 1.0]

	EXPECT_GE(1.0, ind.chromosome[0]);
	EXPECT_LE(0.0, ind.chromosome[0]);

	ind.SetGeneWithinRange(0, 0.64);
	EXPECT_EQ(0.64, ind.chromosome[0]);

	ind.SetGeneWithinRange(0, -0.64);
	EXPECT_EQ(0.0, ind.chromosome[0]);

	ind.SetGeneWithinRange(0, 1.64);
	EXPECT_EQ(1.0, ind.chromosome[0]);
}

TEST(IndividualTests, operatorEquals)
{
	Individual ind1(2);
	Individual ind2(2);

	ind1.chromosome[0] = 3.0 / 2.0;
	ind1.chromosome[1] = 0.0000011;

	ind2.chromosome[0] = (3.0 * std::atan(1)) / (2.0 * std::atan(1));
	ind2.chromosome[1] = 0.0000011;
	EXPECT_EQ(ind1, ind2);

	ind1.chromosome[1] = 0.0000011 - utils::DOUBLE_EPSILON;
	ind2.chromosome[1] = 0.0000011 + utils::DOUBLE_EPSILON;
	EXPECT_FALSE(ind1 == ind2);

}