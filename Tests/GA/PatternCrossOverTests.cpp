#include "AiLab/GA.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std
#include <iostream>

using namespace ai_lab;

class PatternCrossOverTestFixture : public ::testing::Test
{
public:
	static constexpr size_t CHROM_LENGTH = 4;
	static constexpr size_t NUM_INDIVIDUALS = 4;

public:
	explicit PatternCrossOverTestFixture()
	{
		m_individuals.resize(NUM_INDIVIDUALS);
		for (size_t i = 0; i < NUM_INDIVIDUALS; i++)
		{
			m_individuals[i].chromosome.resize(CHROM_LENGTH, i);
			m_individuals[i].fitnessValues.resize(2, i);
		}
	}
	virtual ~PatternCrossOverTestFixture() = default;

protected:
	std::vector<Individual<double>> m_individuals;
	PatternCrossOver<double> m_crossover{ 1.0 };
};

TEST_F(PatternCrossOverTestFixture, FourIndividualsFourGenes)
{
	const auto originalIndividuals = m_individuals;

	for (size_t i = 0; i < 1000; i++)
	{
		m_individuals = originalIndividuals;
		ASSERT_NO_THROW(m_crossover.CrossOver(m_individuals));
		EXPECT_NE(originalIndividuals, m_individuals);  //should always do crossover

		for (size_t g = 0; g < CHROM_LENGTH; g++)
		{
			std::set<double> geneValues;
			for (size_t ind = 0; ind < NUM_INDIVIDUALS; ind++)
			{
				geneValues.insert(m_individuals[ind].chromosome[g]);
			}
			EXPECT_EQ(NUM_INDIVIDUALS, geneValues.size());	//each gene value should be present at the g-th place in one of the individuals.
		}

		for (const auto& ind : m_individuals)
		{
			EXPECT_TRUE(ind.fitnessValues.empty());	//crossover should reset previous fitness values
		}
	}
}