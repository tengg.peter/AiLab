//own
#include "NeuralNet.h"
#include "AiLab/Source/NeuralNetwork/ActivationFunctions/Include.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std
#include <iostream>

using namespace ai_lab;
using utils::String;

TEST(Mlp, ToFromJson)
{
	Mlp mlp(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(3),
		LayerDescription<IdentityActivationFunction, SoftmaxOutputFunction>(3)
	);
	mlp.notes[STR("country")] = STR("England");
	mlp.notes[STR("competition")] = STR("Premier League");

	String json = mlp.ToJson();
	Mlp mlp2 = Mlp::FromJson(json);

	EXPECT_TRUE(mlp == mlp2);
}

TEST(Mlp, ToFromJsonMetadata)
{
	Mlp mlp(
		LayerDescription<LinearScalingActivationFunction>(2),
		LayerDescription<IdentityActivationFunction>(1)
	);

	const double learningRate = 0.0005;
	const double momentum = 0.9;
	const int miniBatchSize = 100;
	const WeightInitialisation weightInit = WeightInitialisation::Xavier;
	const double generalisationError = 1.86;
	const double generalisationLoss = 5.2;
	const int numEpochs = 48;
	const int numTrainingSamples = 19209;
	const int numTestSamples = 6403;
	const double trainingError = 1.854;
	const double trainingLoss = 5.1298;
	const String trainingTime = STR("5:24:53");
	const int trainingType = 1;

	mlp.backPropParamsOptional.emplace();
	mlp.backPropParamsOptional->learningRate = learningRate;
	mlp.backPropParamsOptional->momentum = momentum;
	mlp.backPropParamsOptional->miniBatchSize = miniBatchSize;
	mlp.backPropParamsOptional->weightInitialisation = weightInit;
	mlp.generalisationError = generalisationError;
	mlp.generalisationLoss = generalisationLoss;
	mlp.numEpochs = numEpochs;
	mlp.numTestSamples = numTestSamples;
	mlp.numTrainingSamples = numTrainingSamples;
	mlp.trainingError = trainingError;
	mlp.trainingLoss = trainingLoss;
	mlp.trainingTimeString = trainingTime;
	mlp.trainingType = trainingType;

	String json = mlp.ToJson();
	Mlp mlp2 = Mlp::FromJson(json);

	EXPECT_DOUBLE_EQ(mlp2.backPropParamsOptional->learningRate, learningRate);
	EXPECT_DOUBLE_EQ(mlp2.backPropParamsOptional->momentum, momentum);
	EXPECT_EQ(mlp2.backPropParamsOptional->miniBatchSize, miniBatchSize);
	EXPECT_EQ(mlp2.backPropParamsOptional->weightInitialisation, weightInit);
	EXPECT_DOUBLE_EQ(mlp2.generalisationError, generalisationError);
	EXPECT_DOUBLE_EQ(mlp2.generalisationLoss, generalisationLoss);
	EXPECT_EQ(mlp2.numEpochs, numEpochs);
	EXPECT_EQ(mlp2.numTestSamples, numTestSamples);
	EXPECT_EQ(mlp2.numTrainingSamples, numTrainingSamples);
	EXPECT_DOUBLE_EQ(mlp2.trainingError, trainingError);
	EXPECT_DOUBLE_EQ(mlp2.trainingLoss, trainingLoss);
	EXPECT_EQ(mlp2.trainingTimeString, trainingTime);
	EXPECT_EQ(mlp2.trainingType, trainingType);
}

TEST(Mlp, CopyCtor)
{
	Mlp mlp(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(3),
		LayerDescription<IdentityActivationFunction, SoftmaxOutputFunction>(3)
	);

	Mlp mlp2(mlp);

	EXPECT_TRUE(mlp == mlp2);
}

TEST(Mlp, CopyAssignmentOp)
{
	Mlp mlp(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(3),
		LayerDescription<IdentityActivationFunction, SoftmaxOutputFunction>(3)
	);

	Mlp mlp2 = mlp;

	EXPECT_TRUE(mlp == mlp2);
}

TEST(Mlp, CopyAssignmentOp2)
{
	Mlp mlp(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(3),
		LayerDescription<IdentityActivationFunction>(3)
	);

	Mlp mlp2(
		LayerDescription<LinearScalingActivationFunction>(6),
		LayerDescription<LeakyReluActivationFunction>(5),
		LayerDescription<LeakyReluActivationFunction>(2),
		LayerDescription<IdentityActivationFunction, SoftmaxOutputFunction>(1)
	);
	EXPECT_FALSE(mlp == mlp2);
	mlp2 = mlp;
	EXPECT_TRUE(mlp == mlp2);
}

TEST(Mlp, WeightManipulations)
{
	Mlp mlp(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(3),
		LayerDescription<IdentityActivationFunction>(3)
	);
	EXPECT_EQ(mlp.Weight(0, 1, 2), 0.0);
	EXPECT_EQ(mlp.Weight(1, 0, 2), 0.0);

	mlp.AdjustWeight(0, 1, 2, 0.23);
	mlp.AdjustWeight(1, 0, 2, -0.35);
	EXPECT_EQ(mlp.Weight(0, 1, 2), 0.23);
	EXPECT_EQ(mlp.Weight(1, 0, 2), -0.35);
}

TEST(Mlp, Type)
{
	Mlp regressor(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(3),
		LayerDescription<IdentityActivationFunction>(3)
	);
	EXPECT_EQ(NeuralNetType::Regressor, regressor.Type());

	Mlp classifier(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<IdentityActivationFunction, SoftmaxOutputFunction>(3)
	);
	EXPECT_EQ(NeuralNetType::Classifier, classifier.Type());
}

TEST(Mlp, AddLayer)
{
	Mlp mlp1;
	mlp1.InitRandomWeights(WeightInitialisation::Xavier);
	EXPECT_FALSE(mlp1.WeightsInitialised());	//no layers, so weights couldn have been initialised

	mlp1.AddLayer(LeakyReluActivationFunction::TYPE_STRING, 6);
	mlp1.AddLayer(LeakyReluActivationFunction::TYPE_STRING, 6);
	mlp1.InitRandomWeights(WeightInitialisation::Xavier);
	EXPECT_TRUE(mlp1.WeightsInitialised());
	EXPECT_THROW(mlp1.AddLayer(LeakyReluActivationFunction::TYPE_STRING, 6), std::logic_error);

	Mlp mlp2(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(4),
		LayerDescription<LeakyReluActivationFunction>(3),
		LayerDescription<IdentityActivationFunction, SoftmaxOutputFunction>(3)
	);

	Mlp mlp3;
	mlp3.AddLayer(LinearScalingActivationFunction::TYPE_STRING, 4);
	mlp3.AddLayer(LeakyReluActivationFunction::TYPE_STRING, 4);
	mlp3.AddLayer(LeakyReluActivationFunction::TYPE_STRING, 3);
	mlp3.AddLayer(IdentityActivationFunction::TYPE_STRING, SoftmaxOutputFunction::TYPE_STRING, 3);

	EXPECT_TRUE(mlp2 == mlp3);
}