//own
#include "NeuralNet.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std

using std::vector;

TEST(LossFunctions, Absolute)
{
	ai_lab::AbsoluteErrorLossFunction fn;

	vector<double> expected{ 0.0, -2.0, 1.0 };
	vector<double> actual{ 2.0, 4.0, 2.0 };

	double loss = fn.CalculateLoss(actual, expected);

	EXPECT_DOUBLE_EQ(3, loss);
}

TEST(LossFunctions, AbsoluteDerivative)
{
	ai_lab::AbsoluteErrorLossFunction fn;

	vector<double> expected{ 2.0, -2.0, 2.0 };
	vector<double> actual{ 2.0, 4.0, 1.0 };

	auto derivatives = fn.Derivatives(actual, expected);

	EXPECT_DOUBLE_EQ(0, derivatives[0]);
	EXPECT_DOUBLE_EQ(1, derivatives[1]);
	EXPECT_DOUBLE_EQ(-1, derivatives[2]);
}

TEST(LossFunctions, Squared)
{
	ai_lab::SquaredErrorLossFunction fn;

	vector<double> expected{ 2, 2, 2, 3, 4 };
	vector<double> actual{ 2, 3, 4, 2, 2 };

	double loss = fn.CalculateLoss(actual, expected);
	EXPECT_DOUBLE_EQ(1, loss);
}

TEST(LossFunctions, SquaredDerivative)
{
	ai_lab::SquaredErrorLossFunction fn;

	vector<double> expected{ 2, 2, 2, 3, 4 };
	vector<double> actual{ 2, 3, 4, 2, 2 };

	auto derivatives = fn.Derivatives(actual, expected);

	EXPECT_DOUBLE_EQ(0, derivatives[0]);
	EXPECT_DOUBLE_EQ(1, derivatives[1]);
	EXPECT_DOUBLE_EQ(2, derivatives[2]);
	EXPECT_DOUBLE_EQ(-1, derivatives[3]);
	EXPECT_DOUBLE_EQ(-2, derivatives[4]);

}

TEST(LossFunctions, CrossEntropy)
{
	ai_lab::CrossEntropyLossFunction fn;

	//example numbers taken from https://medium.com/@14prakash/back-propagation-is-very-simple-who-made-it-complicated-97b794c97e5c
	vector<double> expected{ 0 };
	vector<double> actual{ 0.19857651019773825, 0.28559492698949396, 0.5158285628127679 };

	double calculatedError = fn.CalculateLoss(actual, expected);

	//calculated with Windows calculator
	const double expectedError = 2.6782021856010152;

	EXPECT_DOUBLE_EQ(calculatedError, expectedError);
}

TEST(LossFunctions, CrossEntropyDerivative)
{
	ai_lab::CrossEntropyLossFunction fn;

	vector<double> expected{ 1, 2 };
	vector<double> actual{ 0.6, 0.6, 0.4, 0.4 };

	auto derivatives = fn.Derivatives(actual, expected);


	EXPECT_DOUBLE_EQ(2.5, derivatives[0]);
	EXPECT_DOUBLE_EQ(-1.6666666666666667, derivatives[1]);
	EXPECT_DOUBLE_EQ(-2.5, derivatives[2]);
	EXPECT_DOUBLE_EQ(1.6666666666666667, derivatives[3]);

	//example numbers taken from https://medium.com/@14prakash/back-propagation-is-very-simple-who-made-it-complicated-97b794c97e5c
	expected = { 1, 0, 0 };	//these are labels. Label 0 is found twice, but it shouldnt be a problem. Label 1 once, and no label 2
	actual = { 0.2698, 0.3223, 0.4078 };

	derivatives = fn.Derivatives(actual, expected);

	EXPECT_DOUBLE_EQ(-3.7064492216456637, derivatives[0]);
	EXPECT_DOUBLE_EQ(-3.1026993484331369, derivatives[1]);
	EXPECT_DOUBLE_EQ(1.6886187098953054, derivatives[2]);

}