//own
#include "NeuralNet.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>

using namespace ai_lab;
using utils::String;

TEST(Temp, Temp)
{
	Mlp mlp(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<IdentityActivationFunction>(3)
	);

	Mlp mlp2(
		LayerDescription<LinearScalingActivationFunction>(4),
		LayerDescription<IdentityActivationFunction, SoftmaxOutputFunction>(3)
	);

}