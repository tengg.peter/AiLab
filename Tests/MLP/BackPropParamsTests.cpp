//own
#include "NeuralNet.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>

using namespace ai_lab;

TEST(BackPropParams, Copying)
{
	BackPropParams bpp1;
	BackPropParams bpp2;
	EXPECT_EQ(bpp1, bpp2);	//two default constructed instances should be equal
	bpp1.learningRate = 0.1234;
	bpp1.momentum = 0.543;
	EXPECT_NE(bpp1, bpp2);	//two default constructed instances should be equal
	
	bpp1.dcpLossfunction.Reset(new AbsoluteErrorLossFunction());
	EXPECT_NE(bpp1, bpp2);
	
	BackPropParams bpp3 = bpp1;
	EXPECT_EQ(bpp1, bpp3);

	bpp2.dcpLossfunction.Reset(new LogarithmicErrorLossFunction(5.0));
	EXPECT_NE(bpp1, bpp2);

	bpp3 = bpp2;
	EXPECT_EQ(bpp3, bpp2);
}