//own
#include "NeuralNet.h"
//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std

TEST(SoftmaxOutputFunction, Output)
{
	ai_lab::SoftmaxOutputFunction softmax;
	const std::vector<double> input{ 1.8658, 2.2292, 2.8204 };
	const auto output = softmax.CalculateOutput(input);
	std::vector<double> expectedOutput{ 0.19857651019773825, 0.28559492698949396, 0.5158285628127679 };

	EXPECT_EQ(expectedOutput, output);
}

TEST(SoftmaxOutputFunction, Derivatives)
{
	ai_lab::SoftmaxOutputFunction softmax;
	const std::vector<double> input{ 1.8658, 2.2292, 2.8204 };
	softmax.CalculateOutput(input);

	std::vector<double> derivatives;
	for (size_t i = 0; i < input.size(); i++)
	{
		derivatives.push_back(softmax.DerivativeOfCurrentOutput(i));
	}

	//numbers calculated with Windows calculator
	std::vector<double> expectedDerivatives{ 0.1591438797954257925345171734306, 0.20403046466735954291898844231516, 0.24974945659928226155079027631238 };
	for (size_t i = 0; i < expectedDerivatives.size(); i++)
	{
		EXPECT_DOUBLE_EQ(expectedDerivatives[i], derivatives[i]);
	}
}
