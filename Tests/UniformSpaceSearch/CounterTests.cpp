//own
#include "../AiLab/Source/UniformSpaceSearch/Counter.h"

//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std
#include <algorithm>
#if __linux__
	#include <cmath>
#endif

using namespace ai_lab;

TEST(CounterTests, Ctr)
{
	Counter counter(4 /*numberSystem*/, 5 /*numDigits*/);
	EXPECT_EQ(5, counter.State().size());
	EXPECT_TRUE(std::all_of(counter.State().cbegin(), counter.State().cend(), [](const auto digit) {return 0 == digit; }));
}

TEST(CounterTests, CtrInvalidArgumentThrows)
{
	EXPECT_THROW(Counter counter(1 /*numberSystem*/, 5 /*numDigits*/), std::invalid_argument);
	EXPECT_THROW(Counter counter(0 /*numberSystem*/, 5 /*numDigits*/), std::invalid_argument);
	EXPECT_THROW(Counter counter(10 /*numberSystem*/, 0 /*numDigits*/), std::invalid_argument);
}


TEST(CounterTests, Step)
{
	constexpr int base = 4;
	constexpr int digits = 5;

	Counter counter(base, digits);

	auto allZero = [](const auto digit) {return 0 == digit; };

	int numStates = 0;
	while (counter.Step())
	{
		++numStates;
		EXPECT_EQ(5, counter.State().size());
		EXPECT_FALSE(std::all_of(counter.State().cbegin(), counter.State().cend(), allZero));	//should get back to the original state
		EXPECT_TRUE(std::all_of(counter.State().cbegin(), counter.State().cend(), [](const auto digit) {return base >= digit; }));
	}
	++numStates;
	EXPECT_TRUE(std::all_of(counter.State().cbegin(), counter.State().cend(), allZero));	//should get back to the original state
	EXPECT_EQ(std::pow(base, digits), numStates);
}