//own
#include "../AiLab/Source/UniformSpaceSearch/UniformSpaceSearch.h"

//3rd party
#define GTEST_LANG_CXX11 1
#include <gtest/gtest.h>
//std

using namespace ai_lab;

template<typename TParam>
class TestObjFunc : public AObjectiveFunction<SolutionCandidate, TParam>
{
public:
	explicit TestObjFunc() : AObjectiveFunction<SolutionCandidate, TParam>({ Objective::Maximise, Objective::Minimise }, { STR("Obj1"), STR("Obj2") })
	{
	}
	virtual ~TestObjFunc() = default;

	virtual void Calculate(SolutionCandidate<TParam>& solutionCand) override
	{
		solutionCand.scores.emplace_back(-1 * (std::pow(solutionCand.params[0] - 7, 2)));
		solutionCand.scores.emplace_back((std::pow(solutionCand.params[1] - 3, 2)));

		mut.lock();
		//TCOUT << "(" << solutionCand.params[0] << ", " << solutionCand.params[1] << "): (" << solutionCand.scores[0] << ", " << solutionCand.scores[1] << ")\n";
		candidates.emplace_back(solutionCand);
		mut.unlock();
	}

	std::vector<SolutionCandidate<TParam>> candidates;

private:
	inline static std::mutex mut;
};

TEST(UniformSpaceSearchTests, double2d)
{
	TestObjFunc<double> objFunc;

	EXPECT_NO_THROW(UniformSpaceSearch<double>(150, 1, { {0.0, 10.0}, { 0.0, 10.0 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(150, objFunc.candidates.size());
}

TEST(UniformSpaceSearchTests, int2d)
{
	TestObjFunc<int> objFunc;

	EXPECT_NO_THROW(UniformSpaceSearch<int>(250, 1, { {0, 10}, { -10, 10 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(250, objFunc.candidates.size());
}

TEST(UniformSpaceSearchTests, twoRefinementStepsInt)
{
	TestObjFunc<int> objFunc;
	std::vector<SolutionCandidate<int>> bestSolutions;

	EXPECT_NO_THROW(bestSolutions = UniformSpaceSearch<int>(250, 2, { {0, 100}, { -100, 100 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(250, objFunc.candidates.size());
	EXPECT_EQ(1, bestSolutions.size());
	EXPECT_EQ(7, bestSolutions[0].params[0]);
	EXPECT_EQ(4, bestSolutions[0].params[1]);
}

TEST(UniformSpaceSearchTests, twoRefinementStepsDouble)
{
	TestObjFunc<double> objFunc;
	std::vector<SolutionCandidate<double>> bestSolutions;
	EXPECT_NO_THROW(bestSolutions = UniformSpaceSearch<double>(250, 2, { {0, 10}, { -10, 10 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(250, objFunc.candidates.size());
	EXPECT_EQ(1, bestSolutions.size());
	EXPECT_EQ(7, bestSolutions.front().params[0]);
	EXPECT_TRUE(utils::Equals(2.8, bestSolutions.front().params[1], 0.001));
}

template<typename TParam>
class ValidatingObjFunc : public TestObjFunc<TParam>
{
public:
	virtual ~ValidatingObjFunc() = default;

protected:
	virtual bool ValidateParams(const std::vector<TParam>& params) const override 
	{ 
		return params[0] > params[1]; 
	};
};

TEST(UniformSpaceSearchTests, validateParamsInt)
{
	ValidatingObjFunc<int> objFunc;
	std::vector<SolutionCandidate<int>> bestSolutions;
	EXPECT_NO_THROW(bestSolutions = UniformSpaceSearch<int>(250, 2, { {1, 100}, { 1, 100 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(250, objFunc.candidates.size());
	for (const auto& cand : objFunc.candidates)
	{
		EXPECT_GT(cand.params[0], cand.params[1]);
	}

	EXPECT_EQ(1, bestSolutions.size());
	EXPECT_EQ(10, bestSolutions.front().params[0]);
	EXPECT_EQ(3, bestSolutions.front().params[1]);
}

TEST(UniformSpaceSearchTests, validateParamsDouble)
{
	ValidatingObjFunc<double> objFunc;
	std::vector<SolutionCandidate<double>> bestSolutions;
	EXPECT_NO_THROW(bestSolutions = UniformSpaceSearch<double>(250, 2, { {1, 10}, { 1, 10 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(250, objFunc.candidates.size());
	for (const auto& cand : objFunc.candidates)
	{
		EXPECT_GT(cand.params[0], cand.params[1]);
	}

	EXPECT_EQ(3, bestSolutions.size());
	EXPECT_TRUE(utils::Equals(9.567, bestSolutions[0].params[0], 0.001));
	EXPECT_TRUE(utils::Equals(2.999, bestSolutions[0].params[1], 0.001));
}

TEST(UniformSpaceSearchTests, threeRefinementIterationsInt)
{
	ValidatingObjFunc<int> objFunc;
	std::vector<SolutionCandidate<int>> bestSolutions;
	constexpr uint32_t maxRuns = 400;

	EXPECT_NO_THROW(bestSolutions = UniformSpaceSearch<int>(maxRuns, 3, { {1, 1000}, { 1, 1000 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(maxRuns, objFunc.candidates.size());
	for (const auto& cand : objFunc.candidates)
	{
		EXPECT_GT(cand.params[0], cand.params[1]);
	}

	EXPECT_EQ(1, bestSolutions.size());
	EXPECT_EQ(7, bestSolutions.front().params[0]);
	EXPECT_EQ(3, bestSolutions.front().params[1]);
}

TEST(UniformSpaceSearchTests, threeRefinementIterationsDouble)
{
	ValidatingObjFunc<double> objFunc;
	std::vector<SolutionCandidate<double>> bestSolutions;
	constexpr uint32_t maxRuns = 400;

	EXPECT_NO_THROW(bestSolutions = UniformSpaceSearch<double>(maxRuns, 3, { {1, 100}, { 1, 100 } }, objFunc, false /*printProgress*/));
	EXPECT_GE(maxRuns, objFunc.candidates.size());
	for (const auto& cand : objFunc.candidates)
	{
		EXPECT_GT(cand.params[0], cand.params[1]);
	}

	EXPECT_EQ(1, bestSolutions.size());
	EXPECT_TRUE(utils::Equals(8.181, bestSolutions.front().params[0], 0.001));
	EXPECT_TRUE(utils::Equals(2.975, bestSolutions.front().params[1], 0.001));
}
